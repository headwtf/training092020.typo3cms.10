##  5.0 Extend Community Ext and create custom Content Elements


**5.1 Add a new Field zu tx news**

* First we have to extend the database and the table of tx_news. With phpMyAdmin we can see that we have to extend the 'tx_news_domain_model_news' table. 
  

_packages/site/ext_tables.sql_
```
# Table structure for table 'tx_news_domain_model_news '
CREATE TABLE tx_news_domain_model_news (
  tx_news_subheadline VARCHAR(100) DEFAULT '' NOT NULL,
  tx_news_slider VARCHAR(255) DEFAULT '' NOT NULL,
);
```
* Then go to TYPO3 Backend -> **Admin Tools / Maintainance / Analyze database**
* In the next step we have to override the News TCA


**What is the TCA  **(Table Configuration Array) ?

The Table Configuration Array is a global array in TYPO3 which extends the definition of database tables beyond what can be done strictly with SQL. First and foremost defines which tables are editable in the TYPO3 backend. Database tables with no corresponding entry in  are “invisible” to the TYPO3 backend. 

**Further information about the TCA:**
https://docs.typo3.org/m/typo3/reference-tca/master/en-us/Introduction/Index.html
and here you can find all types of Columns Fields:
https://docs.typo3.org/m/typo3/reference-tca/master/en-us/ColumnsConfig/Index.html#columns-types

* Create the file tx_news_domain_model_news.php to extend the TCA of the news. For the name of the file we look in the news folder 'private/typo3conf/ext/news/Configuration/TCA/**tx_news_domain_model_news.php**'


_packages/site/Configuration/TCA/**Overrides/tx_news_domain_model_news.php**_

* Set a new array '$fields = ' and then just add field configs that you will find at 
  **Example:** Value Slider:
  https://docs.typo3.org/m/typo3/reference-tca/master/en-us/ColumnsConfig/Type/inputDefault.html#value-slider

```
<?php
defined('TYPO3_MODE') or die();

$fields = [
    'tx_news_subheadline' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:site/Resources/Private/Language/locallang.xlf:tx_news_domain_model_news.tx_news_subheadline',
        'config' => [
            'type' => 'input',
            'size' => 30,
        ],
    ],
    'tx_news_slider' => [
        'label' => 'LLL:EXT:site/Resources/Private/Language/locallang.xlf:tx_news_domain_model_news.tx_news_slider',
        'config' => [
            'type' => 'input',
            'size' => 10,
            'eval' => 'trim,int',
            'range' => [
                'lower' => -90,
                'upper' => 90,
            ],
            'default' => 0,
            'slider' => [
                'step' => 10,
                'width' => 200,
            ],
        ],
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_news_domain_model_news', $fields);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_news_domain_model_news', 'tx_news_subheadline', '', 'after:title');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_news_domain_model_news', 'tx_news_slider', '', 'after:tx_news_subheadline');

```

* Add the language key to 

_packages/site/Resources/Private/Language/**locallang.xlf**_

```
<trans-unit id="tx_news_domain_model_news.tx_news_subheadline">
    <source>Subheader</source>
</trans-unit>
<trans-unit id="tx_news_domain_model_news.tx_news_slider">
    <source>Slider</source>
</trans-unit>
```

* Add an new Model to extend the class 'NewsDefault'

*packages/site/Classes/Domain/**Model/NewsDefault.php***

```
<?php
namespace Head\Site\Domain\Model;


/***
 *
 * This file will extend the news extension
 * 
 */
class NewsDefault extends \GeorgRinger\News\Domain\Model\News
{

    /**
     * txNewsSubheadline
     * 
     * @var string
     */
    protected $txNewsSubheadline = '';

    /**
     * txNewsSlider
     *
     * @var string
     */
    protected $txNewsSlider = '';

   /**
     * Returns the txNewsSubheadline
     * 
     * @return string $txNewsSubheadline
     */
    public function getTxNewsSubheadline()
    {
        return $this->txNewsSubheadline;
    }

    /**
     * Sets the txNewsSubheadline
     * 
     * @param string $txNewsSubheadline
     * @return void
     */
    public function setTxNewsSubheadline($txNewsSubheadline)
    {
        $this->txNewsSubheadline = $txNewsSubheadline;
    }

    /**
     * Returns the txNewsSlider
     *
     * @return string $txNewsSlider
     */
    public function getTxNewsSlider()
    {
        return $this->txNewsSlider;
    }

    /**
     * Sets the txNewsSlider
     *
     * @param string $txNewsSlider
     * @return void
     */
    public function setTxNewsSlider($txNewsSlider)
    {
        $this->txNewsSlider = $txNewsSlider;
    }
}
```

* Extend the **packages/site/ext_localconf.php**

```
$GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['classes']['Domain/Model/NewsDefault'][] = 'site';
```

* Clear cache and check your news Element and also the `</f:debug>{_all}</f:debug>` in our news partial

**Further Information:**
https://docs.typo3.org/p/georgringer/news/master/en-us/DeveloperManual/ExtendNews/ProxyClassGenerator/Index.html#




## Custom Content Elements


**5.1 Create database structure for my element**

[https://getbootstrap.com/docs/4.0/components/collapse/][https://getbootstrap.com/docs/4.0/components/collapse/]

Let us look at the table tt_content in phpMyAdmin

packages/site/ext_tables.sql

```
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
    tx_site_accordion_item int(11) unsigned DEFAULT '0',
    );

#
# Table structure for table 'tx_site_accordion_item'
#
CREATE TABLE tx_site_accordion_item (

    tt_content int(11) unsigned DEFAULT '0',
    header varchar(255) DEFAULT '' NOT NULL,
    teaser text,
    media int(11) unsigned DEFAULT '0',
    bodytext text,

);
```

* Now just run TYPO3 **Admin-Tool / Maintenance -> Analyze Database Structure** and display the new DB in phpMyAdmin
* We can see that TYPO3 creates not only the fields we have specified, but all those that TYPO3 needs.


**5.2.1 Approve new inline element in backend for output**

If you don't do this for every new inline element, you might see the CE - but you will get an error when saving    **packages/site/ext_tables.php**
    

```
/***************
 * Allow Custom Records on Standard Pages
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_site_accordion_item');
```


**7.2.3 Typoscript Konfigurationen und Icon-Registrierung einfügen**
packages/site/ext_localconf.php

One time additions :

```
/***************
 * Add content rendering configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['FE']['contentRenderingTemplates']['site'] = 'EXT:site/Configuration/TypoScript/ContentElement/';
```

```
/***************
 * PageTS
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site/Configuration/TsConfig/ContentElement/All.tsconfig">');
```


Must be added for individual icons:

```
/***************
 * Register Icons
 */
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon(
    'systeminformation-site',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:site/Resources/Public/Icons/SystemInformation/site.svg']
);
/* Add more icons for other content elementes */
$icons = [
    'accordion',
    'accordion-item'
];
foreach ($icons as $icon) {
    $iconRegistry->registerIcon(
        'content-site-' . $icon,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:site/Resources/Public/Icons/ContentElements/' . $icon . '.svg']
    );
}
```

**7.2.4  Create new folders and new graphics as SVG**
packages**/site/Resources/Public/Icons/**

- ContentElements/accordion.svg
- ContentElements/accordion-item.svg
- SystemInformation/site.svg
  

**5.2.5 Create Typoscript setup and constants and insert them into the site package as dependencies**

**Constants:**
packages/site/Configuration**/TypoScript/constants.typoscript**

```
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site/Configuration/TypoScript/ContentElement/constants.typoscript">
```

**Setup:**
packages/site/Configuration**/TypoScript/setup.typoscript**

```
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site/Configuration/TypoScript/ContentElement/setup.typoscript">
```

* Create setup and constants for for Custom ContentElements:
  packages/site/Configuration/**TypoScript/ContentElement/**

.../ContentElement/**setup.typoscript**

```
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site/Configuration/TypoScript/ContentElement/Element/Accordion.typoscript">
```


.../ContentElement**/constants.typoscript**

```
plugin.site_contentelements {
    view {
        layoutRootPath = EXT:site/Resources/Private/ContentElements/Layouts/
        partialRootPath = EXT:site/Resources/Private/ContentElements/Partials/
        templateRootPath = EXT:site/Resources/Private/ContentElements/Templates/
    }
}
```


Of course I still need the Config of the respective element 
    ...TsConfig/ContentElement**/Element/Accordion.typoscript**

```
############################################
#### CTYPE:accordion ####
############################################
tt_content.accordion >
tt_content.accordion =< lib.contentElement
tt_content.accordion {
    ################
    ### TEMPLATE ###
    ################
    templateName = Accordion

    ##########################
    ### DATA PREPROCESSING ###
    ##########################
    dataProcessing {
        20 = TYPO3\CMS\Frontend\DataProcessing\DatabaseQueryProcessor
        20 {
            table = tx_site_accordion_item
            pidInList.field = pid
            where {
                data = field:uid
                intval = 1
                wrap = tt_content=|
            }
            orderBy = sorting
            dataProcessing {
                10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
                10 {
                    references.fieldName = media
                    as = files
                }
            }
        }
    }
}
```

* To show my new Element in the Backen Element-Wizard i have to create a new config in my TSconfig

packages/site/Configuration/**TsConfig/ContentElement/All.tsconfig**

```
##########################
#### CONTENT ELEMENTS ####
##########################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:site/Configuration/TsConfig/ContentElement/Element" extensions="tsconfig">
```

**All.tsconfig** references the Item folder and considers every ts.config created in the Item folder.

packages/site/Configuration/**TsConfig/ContentElement/Element/Accordion.tsconfig**

In the [ELEMENT NAME].tsconfig I determine under which TAB the element should be displayed. 
FOR EXAMPLE:: "Typical page content":  .wizardItems.**common**

```
####################################
#### NEW CONTENT ELEMENT WIZARD ####
####################################
mod.wizards.newContentElement.wizardItems.common {
    elements {
        accordion {
            iconIdentifier = content-site-accordion
            title = LLL:EXT:site/Resources/Private/Language/Backend.xlf:content_element.accordion
            description = LLL:EXT:site/Resources/Private/Language/Backend.xlf:content_element.accordion.description
            tt_content_defValues {
                CType = accordion
            }
        }
    }
    show := addToList(accordion)
}
```


**Do not forget language file**
packages/site/Resources/Private/Language/Backed.xlf

```
<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<xliff version="1.0">
    <file source-language="en" datatype="plaintext" original="messages" date="2020-05-20UTC17:00:290">
        <header>
            <authorName></authorName>
            <authorEmail></authorEmail>
        </header>
        <body>
            <trans-unit id="content_element.accordion">
                <source>Awesome Accordion</source>
            </trans-unit>
            <trans-unit id="content_element.accordion.description">
                <source>An inline relation record element for fancy accordion magic</source>
            </trans-unit>
            <trans-unit id="accordion_item">
                <source>Accordion items</source>
            </trans-unit>
            <trans-unit id="accordion_item.header">
                <source>Title</source>
            </trans-unit>
            <trans-unit id="accordion_item.teaser">
                <source>Teaser</source>
            </trans-unit>
            <trans-unit id="accordion_item.bodytext">
                <source>Bodytext</source>
            </trans-unit>
            <trans-unit id="accordion_item.media">
                <source>Images</source>
            </trans-unit>
        </body>
    </file>
</xliff>
```


In order for TYPO3 to recognize which fields are needed in my element, I need to create a general TCA (Table Configuration Array) and one for my inline element. The structure can be easily copy & paste from other extensions, or will be found in the official TYPO3 documentation: https://docs.typo3.org/m/typo3/reference-tca/master/en-us/#

packages/site/**Configuration/TCA/Overrides/content_element_accordion.php**
(TYPO3 automatically checks whether new TCAs have been placed in the 'overrides' folder)

```
<?php

/* *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

/**
 * Temporary variables
 */
$extensionKey = 'site';


/***************
 * Add Content Element
 */
if (!is_array($GLOBALS['TCA']['tt_content']['types']['accordion'])) {
    $GLOBALS['TCA']['tt_content']['types']['accordion'] = [];
}

/***************
 * Add content element PageTSConfig
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $extensionKey,
    'Configuration/TsConfig/ContentElement/Element/accordion.tsconfig',
    'Site Package Content Element: Accordion Element'
);

/***************
 * Add content element to selector accordion
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'LLL:EXT:site/Resources/Private/Language/Backend.xlf:content_element.accordion',
        'accordion',
        'content-site-accordion'
    ],
    '--div--',
    'after'
);

/***************
 * Assign Icon
 */
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['accordion'] = 'content-site-accordion';

/***************
 * Configure element type
 */
$GLOBALS['TCA']['tt_content']['types']['accordion'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['types']['accordion'],
    [
        'showitem' => '
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
                tx_site_accordion_item,
            --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                --palette--;;language,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                --palette--;;hidden,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
                rowDescription,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
        '
    ]
);

/***************
 * Register fields
 */
$GLOBALS['TCA']['tt_content']['columns'] = array_replace_recursive(
    $GLOBALS['TCA']['tt_content']['columns'],
    [
        'tx_site_accordion_item' => [
            'label' => 'LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_site_accordion_item',
                'foreign_field' => 'tt_content',
                'appearance' => [
                    'useSortable' => true,
                    'showSynchronizationLink' => true,
                    'showAllLocalizationLink' => true,
                    'showPossibleLocalizationRecords' => true,
                    'showRemovedLocalizationRecords' => false,
                    'expandSingle' => true,
                    'enabledControls' => [
                        'localize' => true,
                    ]
                ],
                'behaviour' => [
                    'mode' => 'select',
                ]
            ]
        ],
    ]
);
```

**
**Create **TCA for the Accordion-Item****
packages/site/**Configuration/TCA/tx_site_accordion_item.php**

```
<?php

/*
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('lang')) {
    $generalLanguageFile = 'EXT:lang/Resources/Private/Language/locallang_general.xlf';
} else {
    $generalLanguageFile = 'EXT:core/Resources/Private/Language/locallang_general.xlf';
}

return [
    'ctrl' => [
        'label' => 'header',
        'sortby' => 'sorting',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'title' => 'LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item',
        'delete' => 'deleted',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'hideTable' => true,
        'hideAtCopy' => true,
        'prependAtCopy' => 'LLL:' . $generalLanguageFile . ':LGL.prependAtCopy',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'languageField' => 'sys_language_uid',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'typeicon_classes' => [
            'default' => 'content-site-accordion-item',
        ]
    ],
    'interface' => [
        'showRecordFieldList' => '
            hidden,
            tt_content,
            header
        ',
    ],
    'types' => [
        '1' => [
            'showitem' => '
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                header,
                teaser,
                bodytext,
                media,              
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.visibility;visibility,
                --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
                --palette--;;hiddenLanguagePalette,
            '
        ],
    ],
    'palettes' => [
        '1' => [
            'showitem' => ''
        ],
        'access' => [
            'showitem' => '
                starttime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel,
                endtime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel
            '
        ],
        'size' => [
            'showitem' => '
                width,
                height
            '
        ],
        'general' => [
            'showitem' => '
                tt_content
            '
        ],
        'visibility' => [
            'showitem' => '
                hidden;LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item
            '
        ],
        // hidden but needs to be included all the time, so sys_language_uid is set correctly
        'hiddenLanguagePalette' => [
            'showitem' => 'sys_language_uid, l10n_parent',
            'isHiddenPalette' => true,
        ],
    ],
    'columns' => [
        'tt_content' => [
            'exclude' => true,
            'label' => 'LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item.tt_content',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tt_content',
                'foreign_table_where' => 'AND tt_content.pid=###CURRENT_PID### AND tt_content.CType="list"',
                'maxitems' => 1,
                'default' => 0,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:' . $generalLanguageFile . ':LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:hidden.I.0'
                    ]
                ]
            ]
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:' . $generalLanguageFile . ':LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
                'default' => 0
            ],
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly'
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:' . $generalLanguageFile . ':LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ]
            ],
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly'
        ],
        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:' . $generalLanguageFile . ':LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => [
                    [
                        'LLL:' . $generalLanguageFile . ':LGL.allLanguages',
                        -1
                    ],
                    [
                        'LLL:' . $generalLanguageFile . ':LGL.default_value',
                        0
                    ]
                ],
                'allowNonIdValues' => true,
            ]
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:' . $generalLanguageFile . ':LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    [
                        '',
                        0
                    ]
                ],
                'foreign_table' => 'tx_site_accordion_item',
                'foreign_table_where' => 'AND tx_site_accordion_item.pid=###CURRENT_PID### AND tx_site_accordion_item.sys_language_uid IN (-1,0)',
                'default' => 0
            ]
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'header' => [
            'exclude' => true,
            'label' => 'LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item.header',
            'config' => [
                'type' => 'input',
                'size' => 50,
                'eval' => 'trim'
            ],
        ],
        'teaser' => [
            'label' => 'LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item.teaser',
            'l10n_mode' => 'prefixLangTitle',
            'l10n_cat' => 'text',
            'config' => [
                'type' => 'text',
                'cols' => '40',
                'rows' => '5',
                'softref' => 'typolink_tag,images,email[subst],url',
                'enableRichtext' => false,
                'richtextConfiguration' => 'default'
            ],
        ],
        'bodytext' => [
            'label' => 'LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item.bodytext',
            'l10n_mode' => 'prefixLangTitle',
            'l10n_cat' => 'text',
            'config' => [
                'type' => 'text',
                'cols' => '80',
                'rows' => '15',
                'softref' => 'typolink_tag,images,email[subst],url',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default'
            ],
        ],
        'media' => [
            'exclude' => true,
            'label' => 'LLL:EXT:site/Resources/Private/Language/Backend.xlf:accordion_item.media',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'media',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_UNKNOWN => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.audioOverlayPalette;audioOverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.videoOverlayPalette;videoOverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ]
                        ]
                    ]
                ],
                $GLOBALS['TYPO3_CONF_VARS']['SYS']['mediafile_ext']
            ),
        ]
    ]
];
```

* **Create a Fluid Template **
  So that all the stuff is also correctly displayed in the frontend, I have to create my Fluid Template

packages/site/Resources/**Private/Templates/ContentElements/Accordion.html**

```
<html xmlns:f="http://typo3.org/ns/TYPO3/CMS/Fluid/ViewHelpers" data-namespace-typo3-fluid="true">

<f:if condition="{records}">
    <div class="accordion my-4" id="myAccordion">
        <f:for each="{records}" as="record" iteration="iteration">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{iteration.index}" aria-expanded="true" aria-controls="collapse{iteration.index}">
                            {record.data.header}<br><small>{record.data.teaser}</small>
                        </button>
                    </h5>
                </div>

                <div id="collapse{iteration.index}" class="collapse" aria-labelledby="heading{iteration.index}" data-parent="#accordion{iteration.index}">
                    <div class="card-body">
                        {record.data.bodytext ->f:format.html()}
                    </div>
                </div>
            </div>
        </f:for>
    </div>
</f:if>
</html>
```

* For the output we still need Bootstrap javascript 
  /assets/**app.js**

```
// Bootstrap Native
var BSN = require("bootstrap.native");
window.BSN = BSN;

var myCollapseInit = new BSN.Collapse('#myAccordion');
```

-  Run `$ yarn encore dev`
-  Now run TYPO3 **Admin-Tool / Maintenance -> Analyze Database Structure**
-  Empty the cache and check if my element is created, ready for use and works without errors. 

[https://getbootstrap.com/docs/4.0/components/collapse/]: https://getbootstrap.com/docs/4.0/components/collapse/