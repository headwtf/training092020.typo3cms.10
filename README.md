# TYPO3 CMS Bootplate

Quickstart

#### Prerequisites

* PHP 7.2.5
* [Composer](https://getcomposer.org/download/)
* [DDEV](https://ddev.readthedocs.io/en/stable/)
* [YARN](https://yarnpkg.com/)
* [Docker](https://www.docker.com/)

## Installation on Development

```bash
$ git clone git clone git@bitbucket.org:headwtf/training092020.typo3cms.10.git
$ cd training092020.typo3cms.10
$ ddev start
$ ddev composer install
$ ddev import-db --src=build/Snapshots/$(ls build/Snapshots/ | sort -r | HEAD -1)
$ yarn install
$ ddev restart
```

#### Backend-Login
* **User: admin**
* **Password: Admin2020!**


----------------


# IF YOU WANT TO START FROM THE SCRATCH:



#### 1 Config DDEV, install and setup TYPO3 and make it more secure


**1.1 Config DDEV**

```
$ ddev config
```

* Project name (**training092020.typo3cms.10**):
* Docroot Location (current directory): **public**
* Project Type [drupal7, drupal8, wordpress, backdrop, drupal6, typo3, magento, magento2, php] (php): **typo3**

**1.2 Install TYPO3**

https://docs.typo3.org/m/typo3/guide-installation/master/en-us/QuickInstall/Composer/Index.html#install-via-composer

```
$ ddev composer create "typo3/cms-base-distribution:^10" --prefer-dist 
```

**--prefer-dist:** Reverse of `--prefer-source`, Composer will install from `dist` if possible. This can speed up installs substantially on build servers and other use cases where you typically do not run updates of the vendors. It is also a way to circumvent problems with git if you do not have a proper setup.

```
$ ddev restart
```

We could work with this installation now, but it is recommended to make TYPO3 even more secure against attacks



**1.3 Let`s make TYPO3 more secure**

For this we will use the composer package "typo3-secure-web“ that creates a web directory for TYPO3, which only contains the entry scripts and links to public assets

This package will then set up the web server document root inside the public folder and TYPO3 inside the private folder. The private folder will look the same way and will contain typo3, typo3conf, fileadmin, typo3temp, uploads folders, while public will only have the entry scripts and links to fileadmin, typo3temp/assets and , Resources/Public of all installed (system) extensions.

**To learn more about security, you can read this blog post later:**
** https://www.nitsan.in/de/blog/how-to-secure-your-typo3-sites-from-hack-attempts/**



**1.3.1 But first thing first, let us prepare our environment.**
		
Create a private folder:

```
$ mkdir -p private/typo3conf
```

Move our AdditionalConfiguration to this new folder

```
$ mv public/typo3conf/AdditionalConfiguration.php private/typo3conf/
```

Clear the public Folder. - We don`t need this structure anymore

```
$ rm -rf public/*
```

In the next step we have to extend our composer .json to give the extension the paths to 'public' and 'private!

```
$ ddev composer config extra.typo3/cms.root-dir private
$ ddev composer config extra.typo3/cms.web-dir public
```

Now, we can install our required extension
[https://packagist.org/packages/helhum/typo3-secure-web][https://packagist.org/packages/helhum/typo3-secure-web]

```
$ ddev composer require helhum/typo3-secure-web
```

Let's take a look at what happened on our page.  For this I let me show the URI again

```
$ ddev restart
```


**1.3.2 To use environment variables in our project we also have to install another extension “dotenv-connector”**

This is a composer plugin, that makes environment variables from a .env file available for any composer based project, without the need to modify code in the project.
[https://packagist.org/packages/helhum/dotenv-connector][https://packagist.org/packages/helhum/dotenv-connector]

Install plugin

```
$ ddev composer require helhum/dotenv-connector
```

Set .env variables to additionalConfiguration

```
'Default' => [
	'dbname' => getenv('TYPO3__DB__database'),
	'host' => getenv('TYPO3__DB__host'),
    'password' => getenv('TYPO3__DB__password'),
    'port' => getenv('TYPO3__DB__port'),
    'user' => getenv('TYPO3__DB__user')
    ],
```


Create an .env File into project root folder

```
TYPO3_CONTEXT='Development'
TYPO3__DB__database='db'
TYPO3__DB__host='db'
TYPO3__DB__password='db'
TYPO3__DB__port='3306'
TYPO3__DB__user='db'
```

have a look if everything still works

```
$ ddev restart 
```



**1.4 Ok, lets setup TYPO3:**

 To start the installation we have to create a **FIRST_INSTAL**L file... due to web-security we have to create it in a private folder - not in the public folder anymore!

```
$ touch private/FIRST_INSTALL
```



**1.4.1 Install**

Open: [https://training092020.typo3cms.10.ddev.site/][https://training092020.typo3cms.10.ddev.site/]

* Step 2/5 & 3/5 are skipped during installation because DDEV has already created an "AdditionalConfiguration.php"
* Step 4/5 creates the first administrative user
  * Username & Password
  * The e-mail address is necessary to use "password forgotten" later in the backend login. This also applies to all other editors or admins
  * Site name (Call it what ever your project name is)**
    **Important: Press Continue and wait! Do not click twice or more. Otherwise you might have problems logging in with your user, because TYPO3 generates a new user with every click, but only the first one is designated as maintainer.**
* Step 5 - Take me straight to the backend



**1.4.2 Login**			

Open: [https://training092020.typo3cms.10.ddev.site/typo3][https://training092020.typo3cms.10.ddev.site/typo3]

* Create Root Page
  * Go to the Backend / Create a new Page (call it Home) / Click with the alternative button on the page and open the dialog box
* Edit Page
  * Behaviour: Use as Root Page (Can be recognized by the globe in the page tree)
  * Access: Page Visible
* Create Root Template
  * Title: Training Main Template
  * Clear Setup field

> Go to Frontpage Error -> “SHOW > home”  or alternative click at ‘home’ and ‘show’ 



**1.5. First Commit and Database Snapshot**

* We have to extend our .gitignore

```
/private/*
!/private/.htaccess
!/private/typo3conf
/private/typo3conf/*
!/private/typo3conf/LocalConfiguration.php
!/private/typo3conf/AdditionalConfiguration.php
/node_modules/
```

* Then we create a new folder for our snapshots and commit

```
$ mkdir -p build/Snapshots/
$ ddev export-db --gzip=false --file=build/Snapshots/$(date +%Y-%m-%d-%H%M).sql
$ git add .
$ git commit -m 'Initial commit'
$ git push -u origin master
```





# 2. Kickstart Site Package

[README_2.0_KickstartSitePackage.md](README_2.0_KickstartSitePackage.md)

# 3. Frontend Enviroment

[README_3.0_FrontendEnviroment.md](README_3.0_FrontendEnviroment.md)

# 4. Configuration

[README_4.0_Configuration.md](README_4.0_Configuration.md)