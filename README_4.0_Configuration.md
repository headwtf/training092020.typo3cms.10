# 4. Configuration



#### 4.1 Backend Layout

We want to use a different template for the landing page than for the subpages. 
Therefore we create a new backend layout.

On the landing-age we find the hint where we can adjust this.
**site/Configuration/TsConfig/Page/Mod/WebLayout/BackendLayouts**/


Copy **default.tsconfig** to **landingpage.tsconfig**

* Change ‚default‘ to ‚landingpage‘ in this file
    * We need another row for a stage section, so set **rowCount** to 2 -> duplikate 1 row and change it to 2.
    * **ColPos = 0** is alway by default „**normal**“ - so just set all other Rows or columns to different numbers
```
#
# BACKENDLAYOUT: Landingpage
#
mod {
    web_layout {
        BackendLayouts {
            landingpage {
                title = LLL:EXT:site/Resources/Private/Language/locallang_be.xlf:backend_layout.landingpage
                config {
                    backend_layout {
                        colCount = 2
                        rowCount = 2
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = LLL:EXT:site/Resources/Private/Language/locallang_be.xlf:backend_layout.column.stage
                                        colPos = 1
                                        colspan = 2
                                    }
                                }
                            }
                            2 {
                                columns {
                                    1 {
                                        name = LLL:EXT:site/Resources/Private/Language/locallang_be.xlf:backend_layout.column.normal
                                        colPos = 0
                                        colspan = 2
                                    }
                                }
                            }
                            3 {
                                columns {
                                    1 {
                                        name = Left
                                        colPos = 3
                                    }
                                    2 {
                                        name = Right
                                        colPos = 4
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:site/Resources/Public/Images/BackendLayouts/landingpage.png
            }
        }
    }
}
```



* We always work with language keys, so we have to add the labels in our localland_be.xlf:

```
<trans-unit id="backend_layout.landingpage">
	<source>Landingpage</source>
</trans-unit>

<trans-unit id="backend_layout.column.stage">
	<source>Stage</source>
</trans-unit>

<trans-unit id="backend_layout.column.left">
	<source>Left</source>
</trans-unit>

<trans-unit id="backend_layout.column.right">
	<source>Right</source>
</trans-unit>
```



To use our new BE Layout we have to add it to our **setup.typoscript**
The value name is also our template name

```
data = pagelayout
required = 1
case = uppercamelcase
split {
  token = pagets__
  cObjNum = 1
  1.current = 1
	}

	#########################
	# add New BE Layouts
	#########################

	landingpage = TEXT
	landingpage.value = Landingpage
```

*  Now we look in the backend and can set a different layout for the start page than for the sub pages. Here we notice that the icon is not displayed correctly. Therefore we copy  
  * site/Resources/Public/Images/BackendLayouts/**default.png** 
    to
  * site/Resources/Public/Images/BackendLayouts/**landingpage.png** 

For projects it is of course advisable to adapt these graphics and draw in the layout structure.



*  On the start page we see our first exception message.
As we can see we are still missing the template for the output. Therefore we simply create 
site/Resources/Private/Templates/Page/**Landingpage.html** .

*  To get an output of the Stage-Section we copy the output-script and add a new section

```
<f:layout name="Landingpage" />

<f:section name="Stage">
    <f:cObject typoscriptObjectPath="lib.dynamicContent" data="{colPos: '1'}" />
</f:section>

<f:section name="Main">
    <f:cObject typoscriptObjectPath="lib.dynamicContent" data="{colPos: '0'}" />
</f:section>

<f:section name="Left">
    <f:cObject typoscriptObjectPath="lib.dynamicContent" data="{colPos: '2'}" />
</f:section>

<f:section name="Right">
    <f:cObject typoscriptObjectPath="lib.dynamicContent" data="{colPos: '3'}" />
</f:section>
```

 Hint: This ViewHelper renders CObjects from the global TypoScript configuration.

* To get Template-Output we also need a Landingpage Template:
  packages/site/Resources/Private/Page/Layouts/**Landingpage.html**

```
<div class="container">
    <header class="blog-header py-3 my-3  border-bottom border-info">
        <div class="row flex-nowrap justify-content-between align-items-center">
            <div class="col-4 pt-1">
                <a class="text-muted" href="#">Subscribe</a>
            </div>
            <div class="col-4 text-center">
                <a class="blog-header-logo text-dark" href="#">LOGO</a>
            </div>
            <div class="col-4 d-flex justify-content-end align-items-center">
                <a class="text-muted" href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mx-3"><circle cx="10.5" cy="10.5" r="7.5"></circle><line x1="21" y1="21" x2="15.8" y2="15.8"></line></svg>
                </a>
                <a class="btn btn-sm btn-outline-secondary" href="#">Sign up</a>
            </div>
        </div>
    </header>

    <div class="nav-scroller pb-3 mb-2">
        <f:render partial="Navi/Main" arguments="{_all}" />
    </div>

    <div class="jumbotron p-3 p-md-5 text-white rounded bg-info">
        <div class="col-md-6 px-0">
        		<!-- Render the new Stage section -->
            <f:render section="Stage" />
        </div>
    </div>

    <div class="row mb-2">
        <div class="col-md-6">
            <div class="card flex-md-row mb-4 box-shadow h-md-250">
                <div class="card-body d-flex flex-column align-items-start">
                    <strong class="d-inline-block mb-2 text-primary">World</strong>
                    <h3 class="mb-0">
                        <a class="text-dark" href="#">Featured post</a>
                    </h3>
                    <div class="mb-1 text-muted">Nov 12</div>
                    <p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
                    <a href="#">Continue reading</a>
                </div>
                <img class="card-img-right flex-auto d-none d-md-block" src="https://via.placeholder.com/200x250" alt="Card image cap">
            </div>
        </div>
        <div class="col-md-6">
            <div class="card flex-md-row mb-4 box-shadow h-md-250">
                <div class="card-body d-flex flex-column align-items-start">
                    <strong class="d-inline-block mb-2 text-success">Design</strong>
                    <h3 class="mb-0">
                        <a class="text-dark" href="#">Post title</a>
                    </h3>
                    <div class="mb-1 text-muted">Nov 11</div>
                    <p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
                    <a href="#">Continue reading</a>
                </div>
                <img class="card-img-right flex-auto d-none d-md-block" src="https://via.placeholder.com/200x250" alt="Card image cap">
            </div>
        </div>
    </div>
</div>

<main role="main" class="container">
    <div class="row my-4">
        <div class="col-md-12 blog-main">
            <f:render section="Main" />
        </div>
    </div>
    <div class="row my-4">
        <div class="col-md-6">
            <f:render section="Left" />
        </div>
        <div class="col-md-6">
            <f:render section="Right" />
        </div>
    </div>
</main>
```



####  4.2 Configurate Core ContentElements

[https://docs.typo3.org/m/typo3/reference-tsconfig/master/en-us/PageTsconfig/TceForm.html][https://docs.typo3.org/m/typo3/reference-tsconfig/master/en-us/PageTsconfig/TceForm.html]

* For example we like to hide the Date-Field and use bootstrap classes for **Alignment**
* So that we can see the field names, we display them by using the Admin Tool:
  **Admin Tools / Settings / Configuration Presets // Debüt Settings -> debug**
* Open a ContentElement in the Backend. We want to hide the field "date" everywhere. 
  

**packages/site/Configuration/TsConfig/Page/TCEFORM.tsconfig**

* Example: 
  **Hide** date, frame_class, linkToTop, sectionIndex and image zoom for all cTypes (including the upcoming custom content elements)
* To use my own Item for a custom „Layout“ and have to specify the type… 
  example: I like to have a 'Call to Action' Layout instead of Layout 1 to 3 just for cTypes „textpics’“

```
TCEFORM {
    pages {

    }
    tt_content {
        date.disabled = 1
        frame_class.disabled = 1
        linkToTop.disabled = 1
        sectionIndex.disabled = 1
        image_zoom.disabled = 1
        # Remove default Layouts and add a new one for cType: textpic
        layout {
            types {
                textpic {
                    removeItems = 1,2,3
                    addItems {
                        4 = Call to Action
                    }
                }
            }
        }	
        # Rename header layout items to what they really stand for
        header_layout {
	    	altLabels {
        		1 = H1
        		2 = H2
        		3 = H3
        		4 = H4
        		5 = H5
    		}
		}
    }
```



####  **4.3 Apply bootstrap class to header output.**

* Let's take a look at the core elements at
   **private/typo3/sysext/fluid_styled_content/Resources/Private/Templates**
* **text or textpic** - reference to partial **header/all** 
  Here we see the classes ce-headline and the variable **{data.header_position}** 
* Als erstes kopieren wir uns die Datei in **unser Site-Package** in den Partials Ordner
   packages/site/Resources/Private/ContentElements/Partials**/Header/All.html**
* Dann lassen wir uns die eingestellten Werte ausgeben: 

```
<f:debug>{_all}</f:debug>
```

* We find our settings in the node "data" .  

* Therefore we can adjust our debug tool more precisely: 

``` 
<f:debug title="Data">{data}</f:debug>
```

* I can now change the classes in an update-safe way, because I only make the changes in my site package. 

```
positionClass: '{f:if(condition: data.header_position, then: \'text-{data.header_position}\')}',
```



####  **4.4 Customize TYPO3 CE Templates**

The nice thing about the Site Package as a Fluid-Styled-Content configuration is that I don't have to care about the configuration. If I want to customize an existing core element. You just have to know where the elements are and where I have to move them.      

Example **Bullet-List [bullets]:** I want to use 2 different representations, which I can customize  with using bootstrap classes.

	- Create CObject from TYPE Bullets List  	
	- So that I can make the template customizations update-safe and site-package specific, I copy **private/typo3/sysext/fluid_styled_content/Resources/Private/Partials/Bullets/ -> packages/site/Resources/Private/ContentElements/Partials/Bullets/**
	- Now I can customize the templates in my site package: 		
	Example: Type-0.html 		
	
```
<ul class="list-group">
    <f:for each="{bullets}" as="bullet">
        <li class="list-group-item">{bullet}</li>
    </f:for>
</ul>
```

* But I want to be able to change the layout. Once with frame, once without frame. 
* Under Appearance/Layout are still the default TYPO3 entries (Layout 1, 2, 3) 	

To change this we have to jump back into TCEFORM
packages/site/Configuration/TsConfig/Page/**TCEFORM.tsconfig** 

```
layout {
    types {
        textpic {
            removeItems = 1,2,3
            addItems {
                4 = Call to Action
            }
        }
        bullets {
            removeItems = 1,2,3
            addItems {
                4 = Without Border
            }
            altLabels {
                0 = Default (Border)
            }
        }
    }
}
```

* Change Content Element to layout 'Without Border'	
* Back to Template and check our variables. Debug and check the Output		

```
<f:debug>{_all}</f:debug>		
```

* As we can see data.layout has become a new value, that we can use in a condition and use a typical Bootstrap class 

  **long version:** 		

```	
<f:if condition="{data.layout} == 4">
     <f:then>
             <ul class="list-group list-group-flush">
                  <f:for each="{bullets}" as="bullet">
                          <li class="list-group-item">{bullet}</li>
                    </f:for>
                </ul>
        </f:then>
        <f:else>
             <ul class="list-group">
                 <f:for each="{bullets}" as="bullet">
                            <li class="list-group-item">{bullet}</li>
                    </f:for>
                </ul>
        </f:else>
</f:if>
```

* keep it short:

```
<ul class="list-group {f:if(condition: '{data.layout} == 4', then: 'list-group-flush')}" >
        <f:for each="{bullets}" as="bullet">
                <li class="list-group-item">{bullet}</li>
        </f:for>
</ul>
```





####  4.5 Customize TYPO3 Community Extensions. For Example NEWS

Install Extension via composer
[https://packagist.org/packages/georgringer/news][https://packagist.org/packages/georgringer/news]
```
$ ddev composer require georgringer/news
```

* Create new Folder and Files for setup and constants:

packages/site/Configuration/TypoScript/Extensions/**news/setup.typoscript** 

``` 	
 <INCLUDE_TYPOSCRIPT: source="FILE:EXT:news/Configuration/TypoScript/setup.txt">
```
**Hint: This has the same effect than include the static file in TYPO3 Backend Template, but this way is clean and not stored at the database**



packages/site/Configuration/TypoScript/Extensions**/news/constants.typoscript**

``` 
# Include static constants of the extension
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:news/Configuration/TypoScript/constants.txt">

# Override the Fluid-Template Path:
plugin.tx_news {
 			view {
  				# cat=plugin.tx_news/file; type=string; label=Path to template root (FE)
  				templateRootPath = EXT:site/Resources/Private/News/Templates/
  			 # cat=plugin.tx_news/file; type=string; label=Path to template partials (FE)
  				partialRootPath = EXT:site/Resources/Private/News/Partials/
  				# cat=plugin.tx_news/file; type=string; label=Path to template layouts (FE)
  			 layoutRootPath = EXT:site/Resources/Private/News/Layouts/
 		 }
 		settings {
  		 # cat=plugin.tx_news/file; type=string; label=Path to CSS file
 		 cssFile =
 	 }
}
```

**Hint**: You can also find these script specifications under **private/typo3conf/ext/news/Configuration/TypoScript/constants.txt**



**Now Copy the Template / Partial or Layout from the News-Extension to your Site-Package:**

* from > private/typo3conf/ext/news/Resources/Private/**Partials/News/List/Item.htm**l  
* to > packages/site/Resources/Private/**News/Partials/List/Item.html**

 To create a first list view, we need a storage folder 

 * TYPO3 > Create Folder [News] > Behavior Contains Plugin NEWS > Access Visiblity
 * Activat Extension -> TYPO3 -> Admin Tools > Extensions or „Analyze Database Structure“
 * Create 2 News in Container „News“
 * Go to Page News -> Place Plugin News in Column "Normal"
    * Startingpoint > News Folder
    * Check Output!

######  This is the same way for all extensions. Install / Update database if necessary and then adjust the constant and the path to the templates in the sitepackage. 



4.6 Routing

```
routeEnhancers:
  News:
    type: Extbase
    limitToPages:
      # News list view - needed for pagination:
      - 2
      # News detail views - needed for general routing:
      - 9
      # News category view:
      - 9
    extension: News
    plugin: Pi1
    routes:
      - routePath: '/page/{page}'
        _controller: 'News::list'
        _arguments:
          page: '@widget_0/currentPage'
      - routePath: '/{news_title}'
        _controller: 'News::detail'
        _arguments:
          news_title: news
      - routePath: '/{category_name}'
        _controller: 'News::list'
        _arguments:
          category_name: overwriteDemand/categories
    defaultController: 'News::list'
    defaults:
      page: '0'
    requirements:
      news_title: '^[a-zA-Z0-9].*$'
      page: \d+
    aspects:
      news_title:
        type: PersistedAliasMapper
        tableName: tx_news_domain_model_news
        routeFieldName: path_segment
      page:
        type: StaticRangeMapper
        start: '1'
        end: '100'
      category_name:
        type: PersistedAliasMapper
        tableName: sys_category
        routeFieldName: title
```






####  4.7 Multi-Language Page

TYPO3 Multi-Language Page 	
* TYPO3 > List > ROOT (New TYPO3 Site) > (+) Website Language 
* TYPO3 > Sites > Languages > add English for FR  	
* TYPO3 > Page > Select to „Languages“   
* Open Page [https://training092020.typo3cms.10.ddev.site/en/][https://training092020.typo3cms.10.ddev.site/en/]

Just to see this once - Backend-Language for Editors:
* TYPO3 > Admin Tools > Maintenance > Manage Language Packs > 'install german'
* TYPO3 > User settings > Personal Data > Language 


[https://training092020.typo3cms.10.ddev.site/en/]: https://training092020.typo3cms.10.ddev.site/en/


# ---- NEW --- DAY 2

#### Localized content

There are two strategies for handling the translation of content on pages:

- **Connected Mode**
  If you wish to translate a page 1:1 you might like to using “content binding” which secures exactly that. The page’s content is then always defined by the default language records and any translation is solely depending on whether a localized record for the default language record exists. **This is the least flexible method but leaves less room for errors.**
- **Free Mode**
  If you wish to localize a page you can build up a separate set of content elements for the page in their own order. You can still maintain references between original and translation if you wish. **Most flexible, but could be too much freedom.**




####  4.8 Setup Form Framework

* Create a '**ext_typoscript_setup.typoscript**' into the site package root

```
plugin.tx_form.settings.yamlConfigurations {
    999 = EXT:site/Configuration/Yaml/CustomFormSetup.yaml
}

module.tx_form.settings.yamlConfigurations {
    999 = EXT:site/Configuration/Yaml/CustomFormSetup.yaml
}
```

* Then we have to create this yaml File into site/Configuration**/Yaml/CustomFormSetup.yaml**

```
TYPO3:
  CMS:
    Form:
      persistenceManager:
        allowedExtensionPaths:
          999: EXT:site/Resources/Private/Forms/Yaml/
			  ### allowedFileMounts overrides the core path and disalowed to storage in fileadmin
        allowedFileMounts:
          10: EXT:site/Resources/Private/Forms/Yaml/
        allowSaveToExtensionPaths: true
        allowDeleteFromExtensionPaths: true

      prototypes:
        ### PROTOTYPE: STANDARD
        standard:
          formElementsDefinition:
            Form:
              renderingOptions:
                translation:
                  translationFiles:
                    # index '10' is reserved for the default translation file.
                    20: 'EXT:site/Resources/Private/Forms/Language/locallang_forms_general.xlf'
                templateRootPaths:
                  100: 'EXT:site/Resources/Private/Forms/Templates/'
                partialRootPaths:
                  100: 'EXT:site/Resources/Private/Forms/Partials/'
                layoutRootPaths:
                  100: 'EXT:site/Resources/Private/Forms/Layouts/'
```

* Create Folder and copy all templates and partials from sysextfolder:
  **private/typo3/sysext/form/Resources/Private/Frontend**
  * site/Resources/Private/Forms/Language/locallang_forms_general.xlf
  * **site/Resources/Private/Forms/Templates/**
  * **site/Resources/Private/Forms/Partials/**
  * site/Resources/Private/Forms/Layouts/
  * site/Resources/Private/Forms/Yaml/
    
    

Lets create a new form with the wizard and see that the storage folder is just in our site package due overwrite 'allowedFileMounts'

* Fields: Select (Country) , Text (Name),Email ( Email), Testfield (Message)
* Place the Plugin at some Page and check the result

*Error: No Template was found!* 
If you run into this message you have to add the static Typoscript setup and include it in 'packages/site/Configuration/TypoScript/Extensions/setup.typoscript':

_packages/site/Configuration/TypoScript/**Extensions/form/setup.typoscript**_

```
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:form/Configuration/TypoScript/setup.typoscript">
```



#### Now let us make the selectfield more dynamic

* For this we have to install a new extension 'static-info-tables'. This is needed as data source for the countries of the world.
  *(Check phpMyAdmin after install. Maybe you have to refresh the content within the extension tool)*

```
$ ddev composer req sjbr/static-info-tables
```

* PHP CLASS

For a new form element in EXT:form you need a php class, which mainly takes care of filling the dropdown options with data. In our example this class is located in **EXT:site/Classes/FormElements/CountryOptions.php**

```
<?php
namespace Head\Site\FormElements;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\FrontendRestrictionContainer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Domain\Model\FormElements\GenericFormElement;
use TYPO3\CMS\Frontend\Category\Collection\CategoryCollection;

class CountryOptions extends GenericFormElement
{

    protected $valueField = '';

    protected $labelField = '';

    public function setProperty(string $key, $value)
    {
        // see form element config for the static country column to be used for the select option value
        if ($key === 'valueField') {
            $this->valueField = $value;
        }
        // see form element config for the static country column to be used for the select option label
        if ($key === 'labelField') {
            $this->labelField = $value;
            $this->setProperty('options', $this->getOptions($this->valueField, $this->labelField));
            return;
        }
        parent::setProperty($key, $value);
    }

    protected function getOptions(string $valueField, string $labelField = 'cn_short_en') : array
    {
        $options = [];
        foreach ($this->getCountries($uid) as $country) {
            $label = $country[$labelField];
            // dynamic label field generation based on current language isocode if marker {currentLanguage} is present
            if (strstr($labelField, '{currentLanguage}') !== false) {
                $siteLanguage = $GLOBALS['TYPO3_REQUEST']->getAttribute('language')->getTwoLetterIsoCode();
                if (isset($country[str_replace('{currentLanguage}', $siteLanguage, $labelField)]) && $country[str_replace('{currentLanguage}', $siteLanguage, $labelField)]) {
                    $label = $country[str_replace('{currentLanguage}', $siteLanguage, $labelField)];
                }
            }
            $options[$country[$valueField]] = $label;
        }
        asort($options);
        return $options;
    }

    protected function getCountries() : array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class) ->getQueryBuilderForTable('static_countries');
        $queryBuilder->setRestrictions(GeneralUtility::makeInstance(FrontendRestrictionContainer::class));
        return $queryBuilder->select('*')->from('static_countries')->execute()->fetchAll();
    }

}
```

* Then the element must be registered in the Form Framework using the following YAML code.

```
TYPO3:
  CMS:
    Form:
      persistenceManager:
        allowedExtensionPaths:
          999: EXT:site/Resources/Private/Forms/Yaml/
        allowedFileMounts:
          10: EXT:site/Resources/Private/Forms/Yaml/
        allowSaveToExtensionPaths: true
        allowDeleteFromExtensionPaths: true

      prototypes:
        ### PROTOTYPE: STANDARD
        standard:
          formElementsDefinition:
            Form:
              renderingOptions:
                translation:
                  translationFiles:
                    # index '10' is reserved for the default translation file.
                    20: 'EXT:site/Resources/Private/Forms/Language/locallang_forms_general.xlf'
                templateRootPaths:
                  100: 'EXT:site/Resources/Private/Forms/Templates/Frontend/'
                partialRootPaths:
                  100: 'EXT:site/Resources/Private/Forms/Partials/Frontend/'
                layoutRootPaths:
                  100: 'EXT:site/Resources/Private/Forms/Layouts/Frontend/'
            SingleSelectWithCountries:
              __inheritances:
                10: 'TYPO3.CMS.Form.prototypes.standard.formElementsDefinition.SingleSelect'
              implementationClassName: 'Head\Site\FormElements\CountryOptions'
              renderingOptions:
                templateName: 'SingleSelect'
```

* Now the select field in my YAML file of the form has to be adjusted:
  packages/site/Resources/Private/**Forms/Yaml/contact.form.yaml**

```
renderables:
  -
    properties:
      fluidAdditionalAttributes:
        required: required
      valueField: cn_short_en
      labelField: cn_short_en
      prependOptionLabel: 'please choose'
    defaultValue: ''
    type: SingleSelectWithCountries
    identifier: country
    label: Country
```


* To customize your HTML and Plaintext E-Mail Template **for every Finisher**, you have to add this snippet to your finisher yaml.

  ```
  finishers:
    -
      options:
  			templateRootPaths:
    			20: 'EXT:site/Resources/Private/Forms/Templates/Frontend/Finishers/Email/'
  ```



#### Related tutorials

Do you need to get one step explained in detail? You can find a series of tutorials on the TYPO3 Form Framework here:

1. [Assign custom templates to the new EXT:form framework](https://www.sebkln.de/en/tutorials/assign-custom-templates-to-the-new-ext-form-framework/)
2. [Different e-mail templates for EmailToSender and EmailToReceiver finishers in EXT:form](https://www.sebkln.de/en/tutorials/different-e-mail-templates-for-emailtosender-and-emailtoreceiver-finishers-in-ext-form/)
3. [Translating forms in the TYPO3 Form Framework](https://www.sebkln.de/en/tutorials/translating-forms-in-the-typo3-form-framework/)

