<?php
defined('TYPO3_MODE') || die();

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['fluid_components']['namespaces']['Sota\\Components\\Components'] =
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('components', 'Resources/Private/Components');
