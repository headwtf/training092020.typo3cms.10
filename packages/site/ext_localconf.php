<?php
defined('TYPO3_MODE') || die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['site'] = 'EXT:site/Configuration/RTE/Default.yaml';
$GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['classes']['Domain/Model/NewsDefault'][] = 'site';

/***************
 * PageTS
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site/Configuration/TsConfig/Page/All.tsconfig">');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site/Configuration/TsConfig/ContentElement/All.tsconfig">');


/***************
 * Add content rendering configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['FE']['contentRenderingTemplates']['site'] = 'EXT:site/Configuration/TypoScript/ContentElement/';


/***************
 * Register Icons
 */
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon(
    'systeminformation-site',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:site/Resources/Public/Icons/SystemInformation/site.svg']
);

/* Add more icons for other content elementes */
$icons = [
    'accordion',
    'accordion-item',
];

foreach ($icons as $icon) {
    $iconRegistry->registerIcon(
        'content-site-' . $icon,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:site/Resources/Public/Icons/ContentElements/' . $icon . '.svg']
    );
}