<?php
namespace head\Site\ViewHelpers;

/**
 * @year 2020
 */


use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3\CMS\Core\Database\Query\Restriction\BackendWorkspaceRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;

/**
 * Render
 */
class ElementsViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    /**
     * As this ViewHelper renders array.
     *
     * @var bool
     */
    protected $escapeOutput = false;

    /**
     * Initializes the arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('tt_content', 'int', 'uid of the content elements', true, 0);
        $this->registerArgument('ref_table', 'string', 'name of the reference table', true, 0);
    }

    /**
     * Resolve .
     *
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return string
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        if ($arguments['tt_content'] > 0) {

            $ref_table = $arguments['ref_table'];

            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($ref_table);
            $queryBuilder->getRestrictions()->removeAll();
            $queryBuilder->getRestrictions()->add(GeneralUtility::makeInstance(DeletedRestriction::class));
            $queryBuilder->getRestrictions()->add(GeneralUtility::makeInstance(BackendWorkspaceRestriction::class));
            $queryBuilder->getRestrictions()->add(GeneralUtility::makeInstance(HiddenRestriction::class));

            $queryBuilder
                ->select('*')
                ->from($ref_table)
                ->where(
                    $queryBuilder->expr()->eq(
                        'tt_content',
                        $queryBuilder->createNamedParameter($arguments['tt_content'], \PDO::PARAM_INT)
                    )
                )
                ->andWhere(
                    $queryBuilder->expr()->eq(
                        't3ver_wsid',
                        $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)
                    )
                )
                ->orderBy('sorting');

            if ($GLOBALS['BE_USER']->workspace) {
                $elements = $queryBuilder
                    ->execute()
                    ->fetchAll();
                foreach ($elements as $i => $e) {
                    BackendUtility::workspaceOL($ref_table, $elements[$i]);
                    $order = $elements[$i]['sorting'];
                }
                array_multisort($elements, SORT_ASC, $order);
            } else {
                $elements = $queryBuilder
                    ->execute()
                    ->fetchAll();
            }
        } else {
            $elements = false;
        }
        return $elements;
    }
}