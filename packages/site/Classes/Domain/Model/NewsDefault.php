<?php
namespace Head\Site\Domain\Model;

/***
 *
 * This file will extend the news extension
 *
 */
class NewsDefault extends \GeorgRinger\News\Domain\Model\News
{

    /**
     * txNewsSubheadline
     *
     * @var string
     */
    protected $txNewsSubheadline = '';

    /**
     * txNewsSlider
     *
     * @var string
     */
    protected $txNewsSlider = '';

    /**
     * Returns the txNewsSubheadline
     *
     * @return string $txNewsSubheadline
     */
    public function getTxNewsSubheadline()
    {
        return $this->txNewsSubheadline;
    }

    /**
     * Sets the txNewsSubheadline
     *
     * @param string $txNewsSubheadline
     * @return void
     */
    public function setTxNewsSubheadline($txNewsSubheadline)
    {
        $this->txNewsSubheadline = $txNewsSubheadline;
    }

    /**
     * Returns the txNewsSlider
     *
     * @return string $txNewsSlider
     */
    public function getTxNewsSlider()
    {
        return $this->txNewsSlider;
    }

    /**
     * Sets the txNewsSlider
     *
     * @param string $txNewsSlider
     * @return void
     */
    public function setTxNewsSlider($txNewsSlider)
    {
        $this->txNewsSlider = $txNewsSlider;
    }
}