<?php
namespace Head\Site\FormElements;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\FrontendRestrictionContainer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Domain\Model\FormElements\GenericFormElement;
use TYPO3\CMS\Frontend\Category\Collection\CategoryCollection;

class CountryOptions extends GenericFormElement
{

    protected $valueField = '';
    protected $labelField = '';

    public function setProperty(string $key, $value)
    {
        // see form element config for the static country column to be used for the select option value
        if ($key === 'valueField') {
            $this->valueField = $value;
        }
        // see form element config for the static country column to be used for the select option label
        if ($key === 'labelField') {
            $this->labelField = $value;
            $this->setProperty('options', $this->getOptions($this->valueField, $this->labelField));
            return;
        }
        parent::setProperty($key, $value);
    }

    protected function getOptions(string $valueField, string $labelField = 'cn_short_en') : array
    {
        $options = [];
        foreach ($this->getCountries($uid) as $country) {
            $label = $country[$labelField];
            // dynamic label field generation based on current language isocode if marker {currentLanguage} is present
            if (strstr($labelField, '{currentLanguage}') !== false) {
                $siteLanguage = $GLOBALS['TYPO3_REQUEST']->getAttribute('language')->getTwoLetterIsoCode();
                if (isset($country[str_replace('{currentLanguage}', $siteLanguage, $labelField)]) && $country[str_replace('{currentLanguage}', $siteLanguage, $labelField)]) {
                    $label = $country[str_replace('{currentLanguage}', $siteLanguage, $labelField)];
                }
            }
            $options[$country[$valueField]] = $label;
        }
        asort($options);
        return $options;
    }

    protected function getCountries() : array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class) ->getQueryBuilderForTable('static_countries');
        $queryBuilder->setRestrictions(GeneralUtility::makeInstance(FrontendRestrictionContainer::class));
        return $queryBuilder->select('*')->from('static_countries')->execute()->fetchAll();
    }

}