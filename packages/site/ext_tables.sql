#
# Add SQL definition of database tables
#

# Table structure for table 'tx_news_domain_model_news '
CREATE TABLE tx_news_domain_model_news (
  tx_news_subheadline VARCHAR(100) DEFAULT '' NOT NULL,
  tx_news_slider VARCHAR(255) DEFAULT '' NOT NULL,
);

# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
    tx_site_accordion_item int(11) unsigned DEFAULT '0',
    );


#
# Table structure for table 'tx_site_accordion_item'
#
CREATE TABLE tx_site_accordion_item (
    tt_content int(11) unsigned DEFAULT '0',
    header varchar(255) DEFAULT '' NOT NULL,
    teaser text,
    media int(11) unsigned DEFAULT '0',
    bodytext text,
);