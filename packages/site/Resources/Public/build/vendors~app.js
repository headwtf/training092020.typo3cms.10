(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["vendors~app"],{

/***/ "./node_modules/bootstrap.native/dist/bootstrap-native.esm.js":
/*!********************************************************************!*\
  !*** ./node_modules/bootstrap.native/dist/bootstrap-native.esm.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*!
  * Native JavaScript for Bootstrap v3.0.10 (https://thednp.github.io/bootstrap.native/)
  * Copyright 2015-2020 © dnp_theme
  * Licensed under MIT (https://github.com/thednp/bootstrap.native/blob/master/LICENSE)
  */
var transitionEndEvent = 'webkitTransition' in document.head.style ? 'webkitTransitionEnd' : 'transitionend';

var supportTransition = 'webkitTransition' in document.head.style || 'transition' in document.head.style;

var transitionDuration = 'webkitTransition' in document.head.style ? 'webkitTransitionDuration' : 'transitionDuration';

function getElementTransitionDuration(element) {
  var duration = supportTransition ? parseFloat(getComputedStyle(element)[transitionDuration]) : 0;
  duration = typeof duration === 'number' && !isNaN(duration) ? duration * 1000 : 0;
  return duration;
}

function emulateTransitionEnd(element,handler){
  var called = 0, duration = getElementTransitionDuration(element);
  duration ? element.addEventListener( transitionEndEvent, function transitionEndWrapper(e){
              !called && handler(e), called = 1;
              element.removeEventListener( transitionEndEvent, transitionEndWrapper);
            })
           : setTimeout(function() { !called && handler(), called = 1; }, 17);
}

function queryElement(selector, parent) {
  var lookUp = parent && parent instanceof Element ? parent : document;
  return selector instanceof Element ? selector : lookUp.querySelector(selector);
}

function bootstrapCustomEvent(eventName, componentName, related) {
  var OriginalCustomEvent = new CustomEvent( eventName + '.bs.' + componentName, {cancelable: true});
  OriginalCustomEvent.relatedTarget = related;
  return OriginalCustomEvent;
}

function dispatchCustomEvent(customEvent){
  this && this.dispatchEvent(customEvent);
}

function Alert(element) {
  var self = this,
    alert,
    closeCustomEvent = bootstrapCustomEvent('close','alert'),
    closedCustomEvent = bootstrapCustomEvent('closed','alert');
  function triggerHandler() {
    alert.classList.contains('fade') ? emulateTransitionEnd(alert,transitionEndHandler) : transitionEndHandler();
  }
  function toggleEvents(action){
    action = action ? 'addEventListener' : 'removeEventListener';
    element[action]('click',clickHandler,false);
  }
  function clickHandler(e) {
    alert = e && e.target.closest(".alert");
    element = queryElement('[data-dismiss="alert"]',alert);
    element && alert && (element === e.target || element.contains(e.target)) && self.close();
  }
  function transitionEndHandler() {
    toggleEvents();
    alert.parentNode.removeChild(alert);
    dispatchCustomEvent.call(alert,closedCustomEvent);
  }
  self.close = function () {
    if ( alert && element && alert.classList.contains('show') ) {
      dispatchCustomEvent.call(alert,closeCustomEvent);
      if ( closeCustomEvent.defaultPrevented ) { return; }
      self.dispose();
      alert.classList.remove('show');
      triggerHandler();
    }
  };
  self.dispose = function () {
    toggleEvents();
    delete element.Alert;
  };
  element = queryElement(element);
  alert = element.closest('.alert');
  element.Alert && element.Alert.dispose();
  if ( !element.Alert ) {
    toggleEvents(1);
  }
  self.element = element;
  element.Alert = self;
}

function Button(element) {
  var self = this, labels,
      changeCustomEvent = bootstrapCustomEvent('change', 'button');
  function toggle(e) {
    var input,
        label = e.target.tagName === 'LABEL' ? e.target
              : e.target.closest('LABEL') ? e.target.closest('LABEL') : null;
    input = label && label.getElementsByTagName('INPUT')[0];
    if ( !input ) { return; }
    dispatchCustomEvent.call(input, changeCustomEvent);
    dispatchCustomEvent.call(element, changeCustomEvent);
    if ( input.type === 'checkbox' ) {
      if ( changeCustomEvent.defaultPrevented ) { return; }
      if ( !input.checked ) {
        label.classList.add('active');
        input.getAttribute('checked');
        input.setAttribute('checked','checked');
        input.checked = true;
      } else {
        label.classList.remove('active');
        input.getAttribute('checked');
        input.removeAttribute('checked');
        input.checked = false;
      }
      if (!element.toggled) {
        element.toggled = true;
      }
    }
    if ( input.type === 'radio' && !element.toggled ) {
      if ( changeCustomEvent.defaultPrevented ) { return; }
      if ( !input.checked || (e.screenX === 0 && e.screenY == 0) ) {
        label.classList.add('active');
        label.classList.add('focus');
        input.setAttribute('checked','checked');
        input.checked = true;
        element.toggled = true;
        Array.from(labels).map(function (otherLabel){
          var otherInput = otherLabel.getElementsByTagName('INPUT')[0];
          if ( otherLabel !== label && otherLabel.classList.contains('active') )  {
            dispatchCustomEvent.call(otherInput, changeCustomEvent);
            otherLabel.classList.remove('active');
            otherInput.removeAttribute('checked');
            otherInput.checked = false;
          }
        });
      }
    }
    setTimeout( function () { element.toggled = false; }, 50 );
  }
  function keyHandler(e) {
    var key = e.which || e.keyCode;
    key === 32 && e.target === document.activeElement && toggle(e);
  }
  function preventScroll(e) {
    var key = e.which || e.keyCode;
    key === 32 && e.preventDefault();
  }
  function focusToggle(e) {
    if (e.target.tagName === 'INPUT' ) {
      var action = e.type === 'focusin' ? 'add' : 'remove';
      e.target.closest('.btn').classList[action]('focus');
    }
  }
  function toggleEvents(action) {
    action = action ? 'addEventListener' : 'removeEventListener';
    element[action]('click',toggle,false );
    element[action]('keyup',keyHandler,false), element[action]('keydown',preventScroll,false);
    element[action]('focusin',focusToggle,false), element[action]('focusout',focusToggle,false);
  }
  self.dispose = function () {
    toggleEvents();
    delete element.Button;
  };
  element = queryElement(element);
  element.Button && element.Button.dispose();
  labels = element.getElementsByClassName('btn');
  if (!labels.length) { return; }
  if ( !element.Button ) {
    toggleEvents(1);
  }
  element.toggled = false;
  element.Button = self;
  Array.from(labels).map(function (btn){
    !btn.classList.contains('active')
      && queryElement('input:checked',btn)
      && btn.classList.add('active');
    btn.classList.contains('active')
      && !queryElement('input:checked',btn)
      && btn.classList.remove('active');
  });
}

var mouseHoverEvents = ('onmouseleave' in document) ? [ 'mouseenter', 'mouseleave'] : [ 'mouseover', 'mouseout' ];

var supportPassive = (function () {
  var result = false;
  try {
    var opts = Object.defineProperty({}, 'passive', {
      get: function() {
        result = true;
      }
    });
    document.addEventListener('DOMContentLoaded', function wrap(){
      document.removeEventListener('DOMContentLoaded', wrap, opts);
    }, opts);
  } catch (e) {}
  return result;
})();

var passiveHandler = supportPassive ? { passive: true } : false;

function isElementInScrollRange(element) {
  var bcr = element.getBoundingClientRect(),
      viewportHeight = window.innerHeight || document.documentElement.clientHeight;
  return bcr.top <= viewportHeight && bcr.bottom >= 0;
}

function Carousel (element,options) {
  options = options || {};
  var self = this,
    vars, ops,
    slideCustomEvent, slidCustomEvent,
    slides, leftArrow, rightArrow, indicator, indicators;
  function pauseHandler() {
    if ( ops.interval !==false && !element.classList.contains('paused') ) {
      element.classList.add('paused');
      !vars.isSliding && ( clearInterval(vars.timer), vars.timer = null );
    }
  }
  function resumeHandler() {
    if ( ops.interval !== false && element.classList.contains('paused') ) {
      element.classList.remove('paused');
      !vars.isSliding && ( clearInterval(vars.timer), vars.timer = null );
      !vars.isSliding && self.cycle();
    }
  }
  function indicatorHandler(e) {
    e.preventDefault();
    if (vars.isSliding) { return; }
    var eventTarget = e.target;
    if ( eventTarget && !eventTarget.classList.contains('active') && eventTarget.getAttribute('data-slide-to') ) {
      vars.index = parseInt( eventTarget.getAttribute('data-slide-to'));
    } else { return false; }
    self.slideTo( vars.index );
  }
  function controlsHandler(e) {
    e.preventDefault();
    if (vars.isSliding) { return; }
    var eventTarget = e.currentTarget || e.srcElement;
    if ( eventTarget === rightArrow ) {
      vars.index++;
    } else if ( eventTarget === leftArrow ) {
      vars.index--;
    }
    self.slideTo( vars.index );
  }
  function keyHandler(ref) {
    var which = ref.which;
    if (vars.isSliding) { return; }
    switch (which) {
      case 39:
        vars.index++;
        break;
      case 37:
        vars.index--;
        break;
      default: return;
    }
    self.slideTo( vars.index );
  }
  function toggleEvents(action) {
    action = action ? 'addEventListener' : 'removeEventListener';
    if ( ops.pause && ops.interval ) {
      element[action]( mouseHoverEvents[0], pauseHandler, false );
      element[action]( mouseHoverEvents[1], resumeHandler, false );
      element[action]( 'touchstart', pauseHandler, passiveHandler );
      element[action]( 'touchend', resumeHandler, passiveHandler );
    }
    ops.touch && slides.length > 1 && element[action]( 'touchstart', touchDownHandler, passiveHandler );
    rightArrow && rightArrow[action]( 'click', controlsHandler,false );
    leftArrow && leftArrow[action]( 'click', controlsHandler,false );
    indicator && indicator[action]( 'click', indicatorHandler,false );
    ops.keyboard && window[action]( 'keydown', keyHandler,false );
  }
  function toggleTouchEvents(action) {
    action = action ? 'addEventListener' : 'removeEventListener';
    element[action]( 'touchmove', touchMoveHandler, passiveHandler );
    element[action]( 'touchend', touchEndHandler, passiveHandler );
  }
  function touchDownHandler(e) {
    if ( vars.isTouch ) { return; }
    vars.touchPosition.startX = e.changedTouches[0].pageX;
    if ( element.contains(e.target) ) {
      vars.isTouch = true;
      toggleTouchEvents(1);
    }
  }
  function touchMoveHandler(e) {
    if ( !vars.isTouch ) { e.preventDefault(); return; }
    vars.touchPosition.currentX = e.changedTouches[0].pageX;
    if ( e.type === 'touchmove' && e.changedTouches.length > 1 ) {
      e.preventDefault();
      return false;
    }
  }
  function touchEndHandler (e) {
    if ( !vars.isTouch || vars.isSliding ) { return }
    vars.touchPosition.endX = vars.touchPosition.currentX || e.changedTouches[0].pageX;
    if ( vars.isTouch ) {
      if ( (!element.contains(e.target) || !element.contains(e.relatedTarget) )
          && Math.abs(vars.touchPosition.startX - vars.touchPosition.endX) < 75 ) {
        return false;
      } else {
        if ( vars.touchPosition.currentX < vars.touchPosition.startX ) {
          vars.index++;
        } else if ( vars.touchPosition.currentX > vars.touchPosition.startX ) {
          vars.index--;
        }
        vars.isTouch = false;
        self.slideTo(vars.index);
      }
      toggleTouchEvents();
    }
  }
  function setActivePage(pageIndex) {
    Array.from(indicators).map(function (x){x.classList.remove('active');});
    indicators[pageIndex] && indicators[pageIndex].classList.add('active');
  }
  function transitionEndHandler(e){
    if (vars.touchPosition){
      var next = vars.index,
          timeout = e && e.target !== slides[next] ? e.elapsedTime*1000+100 : 20,
          activeItem = self.getActiveIndex(),
          orientation = vars.direction === 'left' ? 'next' : 'prev';
      vars.isSliding && setTimeout(function () {
        if (vars.touchPosition){
          vars.isSliding = false;
          slides[next].classList.add('active');
          slides[activeItem].classList.remove('active');
          slides[next].classList.remove(("carousel-item-" + orientation));
          slides[next].classList.remove(("carousel-item-" + (vars.direction)));
          slides[activeItem].classList.remove(("carousel-item-" + (vars.direction)));
          dispatchCustomEvent.call(element, slidCustomEvent);
          if ( !document.hidden && ops.interval && !element.classList.contains('paused') ) {
            self.cycle();
          }
        }
      }, timeout);
    }
  }
  self.cycle = function () {
    if (vars.timer) {
      clearInterval(vars.timer);
      vars.timer = null;
    }
    vars.timer = setInterval(function () {
      var idx = vars.index || self.getActiveIndex();
      isElementInScrollRange(element) && (idx++, self.slideTo( idx ) );
    }, ops.interval);
  };
  self.slideTo = function (next) {
    if (vars.isSliding) { return; }
    var activeItem = self.getActiveIndex(), orientation;
    if ( activeItem === next ) {
      return;
    } else if  ( (activeItem < next ) || (activeItem === 0 && next === slides.length -1 ) ) {
      vars.direction = 'left';
    } else if  ( (activeItem > next) || (activeItem === slides.length - 1 && next === 0 ) ) {
      vars.direction = 'right';
    }
    if ( next < 0 ) { next = slides.length - 1; }
    else if ( next >= slides.length ){ next = 0; }
    orientation = vars.direction === 'left' ? 'next' : 'prev';
    slideCustomEvent = bootstrapCustomEvent('slide', 'carousel', slides[next]);
    slidCustomEvent = bootstrapCustomEvent('slid', 'carousel', slides[next]);
    dispatchCustomEvent.call(element, slideCustomEvent);
    if (slideCustomEvent.defaultPrevented) { return; }
    vars.index = next;
    vars.isSliding = true;
    clearInterval(vars.timer);
    vars.timer = null;
    setActivePage( next );
    if ( getElementTransitionDuration(slides[next]) && element.classList.contains('slide') ) {
      slides[next].classList.add(("carousel-item-" + orientation));
      slides[next].offsetWidth;
      slides[next].classList.add(("carousel-item-" + (vars.direction)));
      slides[activeItem].classList.add(("carousel-item-" + (vars.direction)));
      emulateTransitionEnd(slides[next], transitionEndHandler);
    } else {
      slides[next].classList.add('active');
      slides[next].offsetWidth;
      slides[activeItem].classList.remove('active');
      setTimeout(function () {
        vars.isSliding = false;
        if ( ops.interval && element && !element.classList.contains('paused') ) {
          self.cycle();
        }
        dispatchCustomEvent.call(element, slidCustomEvent);
      }, 100 );
    }
  };
  self.getActiveIndex = function () { return Array.from(slides).indexOf(element.getElementsByClassName('carousel-item active')[0]) || 0; };
  self.dispose = function () {
    var itemClasses = ['left','right','prev','next'];
    Array.from(slides).map(function (slide,idx) {
      slide.classList.contains('active') && setActivePage( idx );
      itemClasses.map(function (cls) { return slide.classList.remove(("carousel-item-" + cls)); });
    });
    clearInterval(vars.timer);
    toggleEvents();
    vars = {};
    ops = {};
    delete element.Carousel;
  };
  element = queryElement( element );
  element.Carousel && element.Carousel.dispose();
  slides = element.getElementsByClassName('carousel-item');
  leftArrow = element.getElementsByClassName('carousel-control-prev')[0];
  rightArrow = element.getElementsByClassName('carousel-control-next')[0];
  indicator = element.getElementsByClassName('carousel-indicators')[0];
  indicators = indicator && indicator.getElementsByTagName( "LI" ) || [];
  if (slides.length < 2) { return }
  var
    intervalAttribute = element.getAttribute('data-interval'),
    intervalData = intervalAttribute === 'false' ? 0 : parseInt(intervalAttribute),
    touchData = element.getAttribute('data-touch') === 'false' ? 0 : 1,
    pauseData = element.getAttribute('data-pause') === 'hover' || false,
    keyboardData = element.getAttribute('data-keyboard') === 'true' || false,
    intervalOption = options.interval,
    touchOption = options.touch;
  ops = {};
  ops.keyboard = options.keyboard === true || keyboardData;
  ops.pause = (options.pause === 'hover' || pauseData) ? 'hover' : false;
  ops.touch = touchOption || touchData;
  ops.interval = typeof intervalOption === 'number' ? intervalOption
              : intervalOption === false || intervalData === 0 || intervalData === false ? 0
              : isNaN(intervalData) ? 5000
              : intervalData;
  if (self.getActiveIndex()<0) {
    slides.length && slides[0].classList.add('active');
    indicators.length && setActivePage(0);
  }
  vars = {};
  vars.direction = 'left';
  vars.index = 0;
  vars.timer = null;
  vars.isSliding = false;
  vars.isTouch = false;
  vars.touchPosition = {
    startX : 0,
    currentX : 0,
    endX : 0
  };
  toggleEvents(1);
  if ( ops.interval ){ self.cycle(); }
  element.Carousel = self;
}

function Collapse(element,options) {
  options = options || {};
  var self = this;
  var accordion = null,
      collapse = null,
      activeCollapse,
      activeElement,
      showCustomEvent,
      shownCustomEvent,
      hideCustomEvent,
      hiddenCustomEvent;
  function openAction(collapseElement, toggle) {
    dispatchCustomEvent.call(collapseElement, showCustomEvent);
    if ( showCustomEvent.defaultPrevented ) { return; }
    collapseElement.isAnimating = true;
    collapseElement.classList.add('collapsing');
    collapseElement.classList.remove('collapse');
    collapseElement.style.height = (collapseElement.scrollHeight) + "px";
    emulateTransitionEnd(collapseElement, function () {
      collapseElement.isAnimating = false;
      collapseElement.setAttribute('aria-expanded','true');
      toggle.setAttribute('aria-expanded','true');
      collapseElement.classList.remove('collapsing');
      collapseElement.classList.add('collapse');
      collapseElement.classList.add('show');
      collapseElement.style.height = '';
      dispatchCustomEvent.call(collapseElement, shownCustomEvent);
    });
  }
  function closeAction(collapseElement, toggle) {
    dispatchCustomEvent.call(collapseElement, hideCustomEvent);
    if ( hideCustomEvent.defaultPrevented ) { return; }
    collapseElement.isAnimating = true;
    collapseElement.style.height = (collapseElement.scrollHeight) + "px";
    collapseElement.classList.remove('collapse');
    collapseElement.classList.remove('show');
    collapseElement.classList.add('collapsing');
    collapseElement.offsetWidth;
    collapseElement.style.height = '0px';
    emulateTransitionEnd(collapseElement, function () {
      collapseElement.isAnimating = false;
      collapseElement.setAttribute('aria-expanded','false');
      toggle.setAttribute('aria-expanded','false');
      collapseElement.classList.remove('collapsing');
      collapseElement.classList.add('collapse');
      collapseElement.style.height = '';
      dispatchCustomEvent.call(collapseElement, hiddenCustomEvent);
    });
  }
  self.toggle = function (e) {
    if (e && e.target.tagName === 'A' || element.tagName === 'A') {e.preventDefault();}
    if (element.contains(e.target) || e.target === element) {
      if (!collapse.classList.contains('show')) { self.show(); }
      else { self.hide(); }
    }
  };
  self.hide = function () {
    if ( collapse.isAnimating ) { return; }
    closeAction(collapse,element);
    element.classList.add('collapsed');
  };
  self.show = function () {
    if ( accordion ) {
      activeCollapse = accordion.getElementsByClassName("collapse show")[0];
      activeElement = activeCollapse && (queryElement(("[data-target=\"#" + (activeCollapse.id) + "\"]"),accordion)
                    || queryElement(("[href=\"#" + (activeCollapse.id) + "\"]"),accordion) );
    }
    if ( !collapse.isAnimating ) {
      if ( activeElement && activeCollapse !== collapse ) {
        closeAction(activeCollapse,activeElement);
        activeElement.classList.add('collapsed');
      }
      openAction(collapse,element);
      element.classList.remove('collapsed');
    }
  };
  self.dispose = function () {
    element.removeEventListener('click',self.toggle,false);
    delete element.Collapse;
  };
    element = queryElement(element);
    element.Collapse && element.Collapse.dispose();
    var accordionData = element.getAttribute('data-parent');
    showCustomEvent = bootstrapCustomEvent('show', 'collapse');
    shownCustomEvent = bootstrapCustomEvent('shown', 'collapse');
    hideCustomEvent = bootstrapCustomEvent('hide', 'collapse');
    hiddenCustomEvent = bootstrapCustomEvent('hidden', 'collapse');
    collapse = queryElement(options.target || element.getAttribute('data-target') || element.getAttribute('href'));
    collapse.isAnimating = false;
    accordion = element.closest(options.parent || accordionData);
    if ( !element.Collapse ) {
      element.addEventListener('click',self.toggle,false);
    }
    element.Collapse = self;
}

function setFocus (element){
  element.focus ? element.focus() : element.setActive();
}

function Dropdown(element,option) {
  var self = this,
      showCustomEvent,
      shownCustomEvent,
      hideCustomEvent,
      hiddenCustomEvent,
      relatedTarget = null,
      parent, menu, menuItems = [],
      persist;
  function preventEmptyAnchor(anchor) {
    (anchor.href && anchor.href.slice(-1) === '#' || anchor.parentNode && anchor.parentNode.href
      && anchor.parentNode.href.slice(-1) === '#') && this.preventDefault();
  }
  function toggleDismiss() {
    var action = element.open ? 'addEventListener' : 'removeEventListener';
    document[action]('click',dismissHandler,false);
    document[action]('keydown',preventScroll,false);
    document[action]('keyup',keyHandler,false);
    document[action]('focus',dismissHandler,false);
  }
  function dismissHandler(e) {
    var eventTarget = e.target,
          hasData = eventTarget && (eventTarget.getAttribute('data-toggle')
                                || eventTarget.parentNode && eventTarget.parentNode.getAttribute
                                && eventTarget.parentNode.getAttribute('data-toggle'));
    if ( e.type === 'focus' && (eventTarget === element || eventTarget === menu || menu.contains(eventTarget) ) ) {
      return;
    }
    if ( (eventTarget === menu || menu.contains(eventTarget)) && (persist || hasData) ) { return; }
    else {
      relatedTarget = eventTarget === element || element.contains(eventTarget) ? element : null;
      self.hide();
    }
    preventEmptyAnchor.call(e,eventTarget);
  }
  function clickHandler(e) {
    relatedTarget = element;
    self.show();
    preventEmptyAnchor.call(e,e.target);
  }
  function preventScroll(e) {
    var key = e.which || e.keyCode;
    if( key === 38 || key === 40 ) { e.preventDefault(); }
  }
  function keyHandler(e) {
    var key = e.which || e.keyCode,
        activeItem = document.activeElement,
        isSameElement = activeItem === element,
        isInsideMenu = menu.contains(activeItem),
        isMenuItem = activeItem.parentNode === menu || activeItem.parentNode.parentNode === menu,
        idx = menuItems.indexOf(activeItem);
    if ( isMenuItem ) {
      idx = isSameElement ? 0
                          : key === 38 ? (idx>1?idx-1:0)
                          : key === 40 ? (idx<menuItems.length-1?idx+1:idx) : idx;
      menuItems[idx] && setFocus(menuItems[idx]);
    }
    if ( (menuItems.length && isMenuItem
          || !menuItems.length && (isInsideMenu || isSameElement)
          || !isInsideMenu )
          && element.open && key === 27
    ) {
      self.toggle();
      relatedTarget = null;
    }
  }
  self.show = function () {
    showCustomEvent = bootstrapCustomEvent('show', 'dropdown', relatedTarget);
    dispatchCustomEvent.call(parent, showCustomEvent);
    if ( showCustomEvent.defaultPrevented ) { return; }
    menu.classList.add('show');
    parent.classList.add('show');
    element.setAttribute('aria-expanded',true);
    element.open = true;
    element.removeEventListener('click',clickHandler,false);
    setTimeout(function () {
      setFocus( menu.getElementsByTagName('INPUT')[0] || element );
      toggleDismiss();
      shownCustomEvent = bootstrapCustomEvent( 'shown', 'dropdown', relatedTarget);
      dispatchCustomEvent.call(parent, shownCustomEvent);
    },1);
  };
  self.hide = function () {
    hideCustomEvent = bootstrapCustomEvent('hide', 'dropdown', relatedTarget);
    dispatchCustomEvent.call(parent, hideCustomEvent);
    if ( hideCustomEvent.defaultPrevented ) { return; }
    menu.classList.remove('show');
    parent.classList.remove('show');
    element.setAttribute('aria-expanded',false);
    element.open = false;
    toggleDismiss();
    setFocus(element);
    setTimeout(function () {
      element.Dropdown && element.addEventListener('click',clickHandler,false);
    },1);
    hiddenCustomEvent = bootstrapCustomEvent('hidden', 'dropdown', relatedTarget);
    dispatchCustomEvent.call(parent, hiddenCustomEvent);
  };
  self.toggle = function () {
    if (parent.classList.contains('show') && element.open) { self.hide(); }
    else { self.show(); }
  };
  self.dispose = function () {
    if (parent.classList.contains('show') && element.open) { self.hide(); }
    element.removeEventListener('click',clickHandler,false);
    delete element.Dropdown;
  };
  element = queryElement(element);
  element.Dropdown && element.Dropdown.dispose();
  parent = element.parentNode;
  menu = queryElement('.dropdown-menu', parent);
  Array.from(menu.children).map(function (child){
    child.children.length && (child.children[0].tagName === 'A' && menuItems.push(child.children[0]));
    child.tagName === 'A' && menuItems.push(child);
  });
  if ( !element.Dropdown ) {
    !('tabindex' in menu) && menu.setAttribute('tabindex', '0');
    element.addEventListener('click',clickHandler,false);
  }
  persist = option === true || element.getAttribute('data-persist') === 'true' || false;
  element.open = false;
  element.Dropdown = self;
}

function Modal(element,options) {
  options = options || {};
  var self = this, modal,
    showCustomEvent,
    shownCustomEvent,
    hideCustomEvent,
    hiddenCustomEvent,
    relatedTarget = null,
    scrollBarWidth,
    overlay,
    overlayDelay,
    fixedItems,
    ops = {};
  function setScrollbar() {
    var openModal = document.body.classList.contains('modal-open'),
        bodyPad = parseInt(getComputedStyle(document.body).paddingRight),
        bodyOverflow = document.documentElement.clientHeight !== document.documentElement.scrollHeight
                    || document.body.clientHeight !== document.body.scrollHeight,
        modalOverflow = modal.clientHeight !== modal.scrollHeight;
    scrollBarWidth = measureScrollbar();
    modal.style.paddingRight = !modalOverflow && scrollBarWidth ? (scrollBarWidth + "px") : '';
    document.body.style.paddingRight = modalOverflow || bodyOverflow ? ((bodyPad + (openModal ? 0:scrollBarWidth)) + "px") : '';
    fixedItems.length && fixedItems.map(function (fixed){
      var itemPad = getComputedStyle(fixed).paddingRight;
      fixed.style.paddingRight = modalOverflow || bodyOverflow ? ((parseInt(itemPad) + (openModal?0:scrollBarWidth)) + "px") : ((parseInt(itemPad)) + "px");
    });
  }
  function resetScrollbar() {
    document.body.style.paddingRight = '';
    modal.style.paddingRight = '';
    fixedItems.length && fixedItems.map(function (fixed){
      fixed.style.paddingRight = '';
    });
  }
  function measureScrollbar() {
    var scrollDiv = document.createElement('div'), widthValue;
    scrollDiv.className = 'modal-scrollbar-measure';
    document.body.appendChild(scrollDiv);
    widthValue = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    document.body.removeChild(scrollDiv);
    return widthValue;
  }
  function createOverlay() {
    var newOverlay = document.createElement('div');
    overlay = queryElement('.modal-backdrop');
    if ( overlay === null ) {
      newOverlay.setAttribute('class', 'modal-backdrop' + (ops.animation ? ' fade' : ''));
      overlay = newOverlay;
      document.body.appendChild(overlay);
    }
    return overlay;
  }
  function removeOverlay () {
    overlay = queryElement('.modal-backdrop');
    if ( overlay && !document.getElementsByClassName('modal show')[0] ) {
      document.body.removeChild(overlay); overlay = null;
    }
    overlay === null && (document.body.classList.remove('modal-open'), resetScrollbar());
  }
  function toggleEvents(action) {
    action = action ? 'addEventListener' : 'removeEventListener';
    window[action]( 'resize', self.update, passiveHandler);
    modal[action]( 'click',dismissHandler,false);
    document[action]( 'keydown',keyHandler,false);
  }
  function beforeShow() {
    modal.style.display = 'block';
    setScrollbar();
    !document.getElementsByClassName('modal show')[0] && document.body.classList.add('modal-open');
    modal.classList.add('show');
    modal.setAttribute('aria-hidden', false);
    modal.classList.contains('fade') ? emulateTransitionEnd(modal, triggerShow) : triggerShow();
  }
  function triggerShow() {
    setFocus(modal);
    modal.isAnimating = false;
    toggleEvents(1);
    shownCustomEvent = bootstrapCustomEvent('shown', 'modal', relatedTarget);
    dispatchCustomEvent.call(modal, shownCustomEvent);
  }
  function triggerHide(force) {
    modal.style.display = '';
    element && (setFocus(element));
    overlay = queryElement('.modal-backdrop');
    if (force !== 1 && overlay && overlay.classList.contains('show') && !document.getElementsByClassName('modal show')[0]) {
      overlay.classList.remove('show');
      emulateTransitionEnd(overlay,removeOverlay);
    } else {
      removeOverlay();
    }
    toggleEvents();
    modal.isAnimating = false;
    hiddenCustomEvent = bootstrapCustomEvent('hidden', 'modal');
    dispatchCustomEvent.call(modal, hiddenCustomEvent);
  }
  function clickHandler(e) {
    if ( modal.isAnimating ) { return; }
    var clickTarget = e.target,
        modalID = "#" + (modal.getAttribute('id')),
        targetAttrValue = clickTarget.getAttribute('data-target') || clickTarget.getAttribute('href'),
        elemAttrValue = element.getAttribute('data-target') || element.getAttribute('href');
    if ( !modal.classList.contains('show')
        && (clickTarget === element && targetAttrValue === modalID
        || element.contains(clickTarget) && elemAttrValue === modalID) ) {
      modal.modalTrigger = element;
      relatedTarget = element;
      self.show();
      e.preventDefault();
    }
  }
  function keyHandler(ref) {
    var which = ref.which;
    if (!modal.isAnimating && ops.keyboard && which == 27 && modal.classList.contains('show') ) {
      self.hide();
    }
  }
  function dismissHandler(e) {
    if ( modal.isAnimating ) { return; }
    var clickTarget = e.target,
        hasData = clickTarget.getAttribute('data-dismiss') === 'modal',
        parentWithData = clickTarget.closest('[data-dismiss="modal"]');
    if ( modal.classList.contains('show') && ( parentWithData || hasData
        || clickTarget === modal && ops.backdrop !== 'static' ) ) {
      self.hide(); relatedTarget = null;
      e.preventDefault();
    }
  }
  self.toggle = function () {
    if ( modal.classList.contains('show') ) {self.hide();} else {self.show();}
  };
  self.show = function () {
    if (modal.classList.contains('show') && !!modal.isAnimating ) {return}
    showCustomEvent = bootstrapCustomEvent('show', 'modal', relatedTarget);
    dispatchCustomEvent.call(modal, showCustomEvent);
    if ( showCustomEvent.defaultPrevented ) { return; }
    modal.isAnimating = true;
    var currentOpen = document.getElementsByClassName('modal show')[0];
    if (currentOpen && currentOpen !== modal) {
      currentOpen.modalTrigger && currentOpen.modalTrigger.Modal.hide();
      currentOpen.Modal && currentOpen.Modal.hide();
    }
    if ( ops.backdrop ) {
      overlay = createOverlay();
    }
    if ( overlay && !currentOpen && !overlay.classList.contains('show') ) {
      overlay.offsetWidth;
      overlayDelay = getElementTransitionDuration(overlay);
      overlay.classList.add('show');
    }
    !currentOpen ? setTimeout( beforeShow, overlay && overlayDelay ? overlayDelay:0 ) : beforeShow();
  };
  self.hide = function (force) {
    if ( !modal.classList.contains('show') ) {return}
    hideCustomEvent = bootstrapCustomEvent( 'hide', 'modal');
    dispatchCustomEvent.call(modal, hideCustomEvent);
    if ( hideCustomEvent.defaultPrevented ) { return; }
    modal.isAnimating = true;
    modal.classList.remove('show');
    modal.setAttribute('aria-hidden', true);
    modal.classList.contains('fade') && force !== 1 ? emulateTransitionEnd(modal, triggerHide) : triggerHide();
  };
  self.setContent = function (content) {
    queryElement('.modal-content',modal).innerHTML = content;
  };
  self.update = function () {
    if (modal.classList.contains('show')) {
      setScrollbar();
    }
  };
  self.dispose = function () {
    self.hide(1);
    if (element) {element.removeEventListener('click',clickHandler,false); delete element.Modal; }
    else {delete modal.Modal;}
  };
  element = queryElement(element);
  var checkModal = queryElement( element.getAttribute('data-target') || element.getAttribute('href') );
  modal = element.classList.contains('modal') ? element : checkModal;
  fixedItems = Array.from(document.getElementsByClassName('fixed-top'))
                    .concat(Array.from(document.getElementsByClassName('fixed-bottom')));
  if ( element.classList.contains('modal') ) { element = null; }
  element && element.Modal && element.Modal.dispose();
  modal && modal.Modal && modal.Modal.dispose();
  ops.keyboard = options.keyboard === false || modal.getAttribute('data-keyboard') === 'false' ? false : true;
  ops.backdrop = options.backdrop === 'static' || modal.getAttribute('data-backdrop') === 'static' ? 'static' : true;
  ops.backdrop = options.backdrop === false || modal.getAttribute('data-backdrop') === 'false' ? false : ops.backdrop;
  ops.animation = modal.classList.contains('fade') ? true : false;
  ops.content = options.content;
  modal.isAnimating = false;
  if ( element && !element.Modal ) {
    element.addEventListener('click',clickHandler,false);
  }
  if ( ops.content ) {
    self.setContent( ops.content.trim() );
  }
  if (element) {
    modal.modalTrigger = element;
    element.Modal = self;
  } else {
    modal.Modal = self;
  }
}

var mouseClickEvents = { down: 'mousedown', up: 'mouseup' };

function getScroll() {
  return {
    y : window.pageYOffset || document.documentElement.scrollTop,
    x : window.pageXOffset || document.documentElement.scrollLeft
  }
}

function styleTip(link,element,position,parent) {
  var tipPositions = /\b(top|bottom|left|right)+/,
      elementDimensions = { w : element.offsetWidth, h: element.offsetHeight },
      windowWidth = (document.documentElement.clientWidth || document.body.clientWidth),
      windowHeight = (document.documentElement.clientHeight || document.body.clientHeight),
      rect = link.getBoundingClientRect(),
      scroll = parent === document.body ? getScroll() : { x: parent.offsetLeft + parent.scrollLeft, y: parent.offsetTop + parent.scrollTop },
      linkDimensions = { w: rect.right - rect.left, h: rect.bottom - rect.top },
      isPopover = element.classList.contains('popover'),
      arrow = element.getElementsByClassName('arrow')[0],
      halfTopExceed = rect.top + linkDimensions.h/2 - elementDimensions.h/2 < 0,
      halfLeftExceed = rect.left + linkDimensions.w/2 - elementDimensions.w/2 < 0,
      halfRightExceed = rect.left + elementDimensions.w/2 + linkDimensions.w/2 >= windowWidth,
      halfBottomExceed = rect.top + elementDimensions.h/2 + linkDimensions.h/2 >= windowHeight,
      topExceed = rect.top - elementDimensions.h < 0,
      leftExceed = rect.left - elementDimensions.w < 0,
      bottomExceed = rect.top + elementDimensions.h + linkDimensions.h >= windowHeight,
      rightExceed = rect.left + elementDimensions.w + linkDimensions.w >= windowWidth;
  position = (position === 'left' || position === 'right') && leftExceed && rightExceed ? 'top' : position;
  position = position === 'top' && topExceed ? 'bottom' : position;
  position = position === 'bottom' && bottomExceed ? 'top' : position;
  position = position === 'left' && leftExceed ? 'right' : position;
  position = position === 'right' && rightExceed ? 'left' : position;
  var topPosition,
    leftPosition,
    arrowTop,
    arrowLeft,
    arrowWidth,
    arrowHeight;
  element.className.indexOf(position) === -1 && (element.className = element.className.replace(tipPositions,position));
  arrowWidth = arrow.offsetWidth; arrowHeight = arrow.offsetHeight;
  if ( position === 'left' || position === 'right' ) {
    if ( position === 'left' ) {
      leftPosition = rect.left + scroll.x - elementDimensions.w - ( isPopover ? arrowWidth : 0 );
    } else {
      leftPosition = rect.left + scroll.x + linkDimensions.w;
    }
    if (halfTopExceed) {
      topPosition = rect.top + scroll.y;
      arrowTop = linkDimensions.h/2 - arrowWidth;
    } else if (halfBottomExceed) {
      topPosition = rect.top + scroll.y - elementDimensions.h + linkDimensions.h;
      arrowTop = elementDimensions.h - linkDimensions.h/2 - arrowWidth;
    } else {
      topPosition = rect.top + scroll.y - elementDimensions.h/2 + linkDimensions.h/2;
      arrowTop = elementDimensions.h/2 - (isPopover ? arrowHeight*0.9 : arrowHeight/2);
    }
  } else if ( position === 'top' || position === 'bottom' ) {
    if ( position === 'top') {
      topPosition =  rect.top + scroll.y - elementDimensions.h - ( isPopover ? arrowHeight : 0 );
    } else {
      topPosition = rect.top + scroll.y + linkDimensions.h;
    }
    if (halfLeftExceed) {
      leftPosition = 0;
      arrowLeft = rect.left + linkDimensions.w/2 - arrowWidth;
    } else if (halfRightExceed) {
      leftPosition = windowWidth - elementDimensions.w*1.01;
      arrowLeft = elementDimensions.w - ( windowWidth - rect.left ) + linkDimensions.w/2 - arrowWidth/2;
    } else {
      leftPosition = rect.left + scroll.x - elementDimensions.w/2 + linkDimensions.w/2;
      arrowLeft = elementDimensions.w/2 - ( isPopover ? arrowWidth : arrowWidth/2 );
    }
  }
  element.style.top = topPosition + 'px';
  element.style.left = leftPosition + 'px';
  arrowTop && (arrow.style.top = arrowTop + 'px');
  arrowLeft && (arrow.style.left = arrowLeft + 'px');
}

function Popover(element,options) {
  options = options || {};
  var self = this;
  var popover = null,
      timer = 0,
      isIphone = /(iPhone|iPod|iPad)/.test(navigator.userAgent),
      titleString,
      contentString,
      ops = {};
  var triggerData,
      animationData,
      placementData,
      dismissibleData,
      delayData,
      containerData,
      closeBtn,
      showCustomEvent,
      shownCustomEvent,
      hideCustomEvent,
      hiddenCustomEvent,
      containerElement,
      containerDataElement,
      modal,
      navbarFixedTop,
      navbarFixedBottom,
      placementClass;
  function dismissibleHandler(e) {
    if (popover !== null && e.target === queryElement('.close',popover)) {
      self.hide();
    }
  }
  function getContents() {
    return {
      0 : options.title || element.getAttribute('data-title') || null,
      1 : options.content || element.getAttribute('data-content') || null
    }
  }
  function removePopover() {
    ops.container.removeChild(popover);
    timer = null; popover = null;
  }
  function createPopover() {
    titleString = getContents()[0] || null;
    contentString = getContents()[1];
    contentString = !!contentString ? contentString.trim() : null;
    popover = document.createElement('div');
    var popoverArrow = document.createElement('div');
    popoverArrow.classList.add('arrow');
    popover.appendChild(popoverArrow);
    if ( contentString !== null && ops.template === null ) {
      popover.setAttribute('role','tooltip');
      if (titleString !== null) {
        var popoverTitle = document.createElement('h3');
        popoverTitle.classList.add('popover-header');
        popoverTitle.innerHTML = ops.dismissible ? titleString + closeBtn : titleString;
        popover.appendChild(popoverTitle);
      }
      var popoverBodyMarkup = document.createElement('div');
      popoverBodyMarkup.classList.add('popover-body');
      popoverBodyMarkup.innerHTML = ops.dismissible && titleString === null ? contentString + closeBtn : contentString;
      popover.appendChild(popoverBodyMarkup);
    } else {
      var popoverTemplate = document.createElement('div');
      popoverTemplate.innerHTML = ops.template.trim();
      popover.className = popoverTemplate.firstChild.className;
      popover.innerHTML = popoverTemplate.firstChild.innerHTML;
      var popoverHeader = queryElement('.popover-header',popover),
          popoverBody = queryElement('.popover-body',popover);
      titleString && popoverHeader && (popoverHeader.innerHTML = titleString.trim());
      contentString && popoverBody && (popoverBody.innerHTML = contentString.trim());
    }
    ops.container.appendChild(popover);
    popover.style.display = 'block';
    !popover.classList.contains( 'popover') && popover.classList.add('popover');
    !popover.classList.contains( ops.animation) && popover.classList.add(ops.animation);
    !popover.classList.contains( placementClass) && popover.classList.add(placementClass);
  }
  function showPopover() {
    !popover.classList.contains('show') && ( popover.classList.add('show') );
  }
  function updatePopover() {
    styleTip(element, popover, ops.placement, ops.container);
  }
  function forceFocus () {
    if (popover === null) { element.focus(); }
  }
  function toggleEvents(action) {
    action = action ? 'addEventListener' : 'removeEventListener';
    if (ops.trigger === 'hover') {
      element[action]( mouseClickEvents.down, self.show );
      element[action]( mouseHoverEvents[0], self.show );
      if (!ops.dismissible) { element[action]( mouseHoverEvents[1], self.hide ); }
    } else if ('click' == ops.trigger) {
      element[action]( ops.trigger, self.toggle );
    } else if ('focus' == ops.trigger) {
      isIphone && element[action]( 'click', forceFocus, false );
      element[action]( ops.trigger, self.toggle );
    }
  }
  function touchHandler(e){
    if ( popover && popover.contains(e.target) || e.target === element || element.contains(e.target)) ; else {
      self.hide();
    }
  }
  function dismissHandlerToggle(action) {
    action = action ? 'addEventListener' : 'removeEventListener';
    if (ops.dismissible) {
      document[action]('click', dismissibleHandler, false );
    } else {
      'focus' == ops.trigger && element[action]( 'blur', self.hide );
      'hover' == ops.trigger && document[action]( 'touchstart', touchHandler, passiveHandler );
    }
    window[action]('resize', self.hide, passiveHandler );
  }
  function showTrigger() {
    dismissHandlerToggle(1);
    dispatchCustomEvent.call(element, shownCustomEvent);
  }
  function hideTrigger() {
    dismissHandlerToggle();
    removePopover();
    dispatchCustomEvent.call(element, hiddenCustomEvent);
  }
  self.toggle = function () {
    if (popover === null) { self.show(); }
    else { self.hide(); }
  };
  self.show = function () {
    clearTimeout(timer);
    timer = setTimeout( function () {
      if (popover === null) {
        dispatchCustomEvent.call(element, showCustomEvent);
        if ( showCustomEvent.defaultPrevented ) { return; }
        createPopover();
        updatePopover();
        showPopover();
        !!ops.animation ? emulateTransitionEnd(popover, showTrigger) : showTrigger();
      }
    }, 20 );
  };
  self.hide = function () {
    clearTimeout(timer);
    timer = setTimeout( function () {
      if (popover && popover !== null && popover.classList.contains('show')) {
        dispatchCustomEvent.call(element, hideCustomEvent);
        if ( hideCustomEvent.defaultPrevented ) { return; }
        popover.classList.remove('show');
        !!ops.animation ? emulateTransitionEnd(popover, hideTrigger) : hideTrigger();
      }
    }, ops.delay );
  };
  self.dispose = function () {
    self.hide();
    toggleEvents();
    delete element.Popover;
  };
  element = queryElement(element);
  element.Popover && element.Popover.dispose();
  triggerData = element.getAttribute('data-trigger');
  animationData = element.getAttribute('data-animation');
  placementData = element.getAttribute('data-placement');
  dismissibleData = element.getAttribute('data-dismissible');
  delayData = element.getAttribute('data-delay');
  containerData = element.getAttribute('data-container');
  closeBtn = '<button type="button" class="close">×</button>';
  showCustomEvent = bootstrapCustomEvent('show', 'popover');
  shownCustomEvent = bootstrapCustomEvent('shown', 'popover');
  hideCustomEvent = bootstrapCustomEvent('hide', 'popover');
  hiddenCustomEvent = bootstrapCustomEvent('hidden', 'popover');
  containerElement = queryElement(options.container);
  containerDataElement = queryElement(containerData);
  modal = element.closest('.modal');
  navbarFixedTop = element.closest('.fixed-top');
  navbarFixedBottom = element.closest('.fixed-bottom');
  ops.template = options.template ? options.template : null;
  ops.trigger = options.trigger ? options.trigger : triggerData || 'hover';
  ops.animation = options.animation && options.animation !== 'fade' ? options.animation : animationData || 'fade';
  ops.placement = options.placement ? options.placement : placementData || 'top';
  ops.delay = parseInt(options.delay || delayData) || 200;
  ops.dismissible = options.dismissible || dismissibleData === 'true' ? true : false;
  ops.container = containerElement ? containerElement
                          : containerDataElement ? containerDataElement
                          : navbarFixedTop ? navbarFixedTop
                          : navbarFixedBottom ? navbarFixedBottom
                          : modal ? modal : document.body;
  placementClass = "bs-popover-" + (ops.placement);
  var popoverContents = getContents();
  titleString = popoverContents[0];
  contentString = popoverContents[1];
  if ( !contentString && !ops.template ) { return; }
  if ( !element.Popover ) {
    toggleEvents(1);
  }
  element.Popover = self;
}

function ScrollSpy(element,options) {
  options = options || {};
  var self = this,
    vars,
    targetData,
    offsetData,
    spyTarget,
    scrollTarget,
    ops = {};
  function updateTargets(){
    var links = spyTarget.getElementsByTagName('A');
    if (vars.length !== links.length) {
      vars.items = [];
      vars.targets = [];
      Array.from(links).map(function (link){
        var href = link.getAttribute('href'),
          targetItem = href && href.charAt(0) === '#' && href.slice(-1) !== '#' && queryElement(href);
        if ( targetItem ) {
          vars.items.push(link);
          vars.targets.push(targetItem);
        }
      });
      vars.length = links.length;
    }
  }
  function updateItem(index) {
    var item = vars.items[index],
      targetItem = vars.targets[index],
      dropmenu = item.classList.contains('dropdown-item') && item.closest('.dropdown-menu'),
      dropLink = dropmenu && dropmenu.previousElementSibling,
      nextSibling = item.nextElementSibling,
      activeSibling = nextSibling && nextSibling.getElementsByClassName('active').length,
      targetRect = vars.isWindow && targetItem.getBoundingClientRect(),
      isActive = item.classList.contains('active') || false,
      topEdge = (vars.isWindow ? targetRect.top + vars.scrollOffset : targetItem.offsetTop) - ops.offset,
      bottomEdge = vars.isWindow ? targetRect.bottom + vars.scrollOffset - ops.offset
                 : vars.targets[index+1] ? vars.targets[index+1].offsetTop - ops.offset
                 : element.scrollHeight,
      inside = activeSibling || vars.scrollOffset >= topEdge && bottomEdge > vars.scrollOffset;
     if ( !isActive && inside ) {
      item.classList.add('active');
      if (dropLink && !dropLink.classList.contains('active') ) {
        dropLink.classList.add('active');
      }
      dispatchCustomEvent.call(element, bootstrapCustomEvent( 'activate', 'scrollspy', vars.items[index]));
    } else if ( isActive && !inside ) {
      item.classList.remove('active');
      if (dropLink && dropLink.classList.contains('active') && !item.parentNode.getElementsByClassName('active').length ) {
        dropLink.classList.remove('active');
      }
    } else if ( isActive && inside || !inside && !isActive ) {
      return;
    }
  }
  function updateItems() {
    updateTargets();
    vars.scrollOffset = vars.isWindow ? getScroll().y : element.scrollTop;
    vars.items.map(function (l,idx){ return updateItem(idx); });
  }
  function toggleEvents(action) {
    action = action ? 'addEventListener' : 'removeEventListener';
    scrollTarget[action]('scroll', self.refresh, passiveHandler );
    window[action]( 'resize', self.refresh, passiveHandler );
  }
  self.refresh = function () {
    updateItems();
  };
  self.dispose = function () {
    toggleEvents();
    delete element.ScrollSpy;
  };
  element = queryElement(element);
  element.ScrollSpy && element.ScrollSpy.dispose();
  targetData = element.getAttribute('data-target');
  offsetData = element.getAttribute('data-offset');
  spyTarget = queryElement(options.target || targetData);
  scrollTarget = element.offsetHeight < element.scrollHeight ? element : window;
  if (!spyTarget) { return }
  ops.target = spyTarget;
  ops.offset = parseInt(options.offset || offsetData) || 10;
  vars = {};
  vars.length = 0;
  vars.items = [];
  vars.targets = [];
  vars.isWindow = scrollTarget === window;
  if ( !element.ScrollSpy ) {
    toggleEvents(1);
  }
  self.refresh();
  element.ScrollSpy = self;
}

function Tab(element,options) {
  options = options || {};
  var self = this,
    heightData,
    tabs, dropdown,
    showCustomEvent,
    shownCustomEvent,
    hideCustomEvent,
    hiddenCustomEvent,
    next,
    tabsContentContainer = false,
    activeTab,
    activeContent,
    nextContent,
    containerHeight,
    equalContents,
    nextHeight,
    animateHeight;
  function triggerEnd() {
    tabsContentContainer.style.height = '';
    tabsContentContainer.classList.remove('collapsing');
    tabs.isAnimating = false;
  }
  function triggerShow() {
    if (tabsContentContainer) {
      if ( equalContents ) {
        triggerEnd();
      } else {
        setTimeout(function () {
          tabsContentContainer.style.height = nextHeight + "px";
          tabsContentContainer.offsetWidth;
          emulateTransitionEnd(tabsContentContainer, triggerEnd);
        },50);
      }
    } else {
      tabs.isAnimating = false;
    }
    shownCustomEvent = bootstrapCustomEvent('shown', 'tab', activeTab);
    dispatchCustomEvent.call(next, shownCustomEvent);
  }
  function triggerHide() {
    if (tabsContentContainer) {
      activeContent.style.float = 'left';
      nextContent.style.float = 'left';
      containerHeight = activeContent.scrollHeight;
    }
    showCustomEvent = bootstrapCustomEvent('show', 'tab', activeTab);
    hiddenCustomEvent = bootstrapCustomEvent('hidden', 'tab', next);
    dispatchCustomEvent.call(next, showCustomEvent);
    if ( showCustomEvent.defaultPrevented ) { return; }
    nextContent.classList.add('active');
    activeContent.classList.remove('active');
    if (tabsContentContainer) {
      nextHeight = nextContent.scrollHeight;
      equalContents = nextHeight === containerHeight;
      tabsContentContainer.classList.add('collapsing');
      tabsContentContainer.style.height = containerHeight + "px";
      tabsContentContainer.offsetHeight;
      activeContent.style.float = '';
      nextContent.style.float = '';
    }
    if ( nextContent.classList.contains('fade') ) {
      setTimeout(function () {
        nextContent.classList.add('show');
        emulateTransitionEnd(nextContent,triggerShow);
      },20);
    } else { triggerShow(); }
    dispatchCustomEvent.call(activeTab, hiddenCustomEvent);
  }
  function getActiveTab() {
    var activeTabs = tabs.getElementsByClassName('active'), activeTab;
    if ( activeTabs.length === 1 && !activeTabs[0].parentNode.classList.contains('dropdown') ) {
      activeTab = activeTabs[0];
    } else if ( activeTabs.length > 1 ) {
      activeTab = activeTabs[activeTabs.length-1];
    }
    return activeTab;
  }
  function getActiveContent() { return queryElement(getActiveTab().getAttribute('href')) }
  function clickHandler(e) {
    e.preventDefault();
    next = e.currentTarget;
    !tabs.isAnimating && self.show();
  }
  self.show = function () {
    next = next || element;
    if (!next.classList.contains('active')) {
      nextContent = queryElement(next.getAttribute('href'));
      activeTab = getActiveTab();
      activeContent = getActiveContent();
      hideCustomEvent = bootstrapCustomEvent( 'hide', 'tab', next);
      dispatchCustomEvent.call(activeTab, hideCustomEvent);
      if (hideCustomEvent.defaultPrevented) { return; }
      tabs.isAnimating = true;
      activeTab.classList.remove('active');
      activeTab.setAttribute('aria-selected','false');
      next.classList.add('active');
      next.setAttribute('aria-selected','true');
      if ( dropdown ) {
        if ( !element.parentNode.classList.contains('dropdown-menu') ) {
          if (dropdown.classList.contains('active')) { dropdown.classList.remove('active'); }
        } else {
          if (!dropdown.classList.contains('active')) { dropdown.classList.add('active'); }
        }
      }
      if (activeContent.classList.contains('fade')) {
        activeContent.classList.remove('show');
        emulateTransitionEnd(activeContent, triggerHide);
      } else { triggerHide(); }
    }
  };
  self.dispose = function () {
    element.removeEventListener('click',clickHandler,false);
    delete element.Tab;
  };
  element = queryElement(element);
  element.Tab && element.Tab.dispose();
  heightData = element.getAttribute('data-height');
  tabs = element.closest('.nav');
  dropdown = tabs && queryElement('.dropdown-toggle',tabs);
  animateHeight = !supportTransition || (options.height === false || heightData === 'false') ? false : true;
  tabs.isAnimating = false;
  if ( !element.Tab ) {
    element.addEventListener('click',clickHandler,false);
  }
  if (animateHeight) { tabsContentContainer = getActiveContent().parentNode; }
  element.Tab = self;
}

function Toast(element,options) {
  options = options || {};
  var self = this,
      toast, timer = 0,
      animationData,
      autohideData,
      delayData,
      showCustomEvent,
      hideCustomEvent,
      shownCustomEvent,
      hiddenCustomEvent,
      ops = {};
  function showComplete() {
    toast.classList.remove( 'showing' );
    toast.classList.add( 'show' );
    dispatchCustomEvent.call(toast,shownCustomEvent);
    if (ops.autohide) { self.hide(); }
  }
  function hideComplete() {
    toast.classList.add( 'hide' );
    dispatchCustomEvent.call(toast,hiddenCustomEvent);
  }
  function close () {
    toast.classList.remove('show' );
    ops.animation ? emulateTransitionEnd(toast, hideComplete) : hideComplete();
  }
  function disposeComplete() {
    clearTimeout(timer);
    element.removeEventListener('click',self.hide,false);
    delete element.Toast;
  }
  self.show = function () {
    if (toast && !toast.classList.contains('show')) {
      dispatchCustomEvent.call(toast,showCustomEvent);
      if (showCustomEvent.defaultPrevented) { return; }
      ops.animation && toast.classList.add( 'fade' );
      toast.classList.remove('hide' );
      toast.offsetWidth;
      toast.classList.add('showing' );
      ops.animation ? emulateTransitionEnd(toast, showComplete) : showComplete();
    }
  };
  self.hide = function (noTimer) {
    if (toast && toast.classList.contains('show')) {
      dispatchCustomEvent.call(toast,hideCustomEvent);
      if(hideCustomEvent.defaultPrevented) { return; }
      noTimer ? close() : (timer = setTimeout( close, ops.delay));
    }
  };
  self.dispose = function () {
    ops.animation ? emulateTransitionEnd(toast, disposeComplete) : disposeComplete();
  };
  element = queryElement(element);
  element.Toast && element.Toast.dispose();
  toast = element.closest('.toast');
  animationData = element.getAttribute('data-animation');
  autohideData = element.getAttribute('data-autohide');
  delayData = element.getAttribute('data-delay');
  showCustomEvent = bootstrapCustomEvent('show', 'toast');
  hideCustomEvent = bootstrapCustomEvent('hide', 'toast');
  shownCustomEvent = bootstrapCustomEvent('shown', 'toast');
  hiddenCustomEvent = bootstrapCustomEvent('hidden', 'toast');
  ops.animation = options.animation === false || animationData === 'false' ? 0 : 1;
  ops.autohide = options.autohide === false || autohideData === 'false' ? 0 : 1;
  ops.delay = parseInt(options.delay || delayData) || 500;
  if ( !element.Toast ) {
    element.addEventListener('click',self.hide,false);
  }
  element.Toast = self;
}

function Tooltip(element,options) {
  options = options || {};
  var self = this,
      tooltip = null, timer = 0, titleString,
      animationData,
      placementData,
      delayData,
      containerData,
      showCustomEvent,
      shownCustomEvent,
      hideCustomEvent,
      hiddenCustomEvent,
      containerElement,
      containerDataElement,
      modal,
      navbarFixedTop,
      navbarFixedBottom,
      placementClass,
      ops = {};
  function getTitle() {
    return element.getAttribute('title')
        || element.getAttribute('data-title')
        || element.getAttribute('data-original-title')
  }
  function removeToolTip() {
    ops.container.removeChild(tooltip);
    tooltip = null; timer = null;
  }
  function createToolTip() {
    titleString = getTitle();
    if ( titleString ) {
      tooltip = document.createElement('div');
      if (ops.template) {
        var tooltipMarkup = document.createElement('div');
        tooltipMarkup.innerHTML = ops.template.trim();
        tooltip.className = tooltipMarkup.firstChild.className;
        tooltip.innerHTML = tooltipMarkup.firstChild.innerHTML;
        queryElement('.tooltip-inner',tooltip).innerHTML = titleString.trim();
      } else {
        var tooltipArrow = document.createElement('div');
        tooltipArrow.classList.add('arrow');
        tooltip.appendChild(tooltipArrow);
        var tooltipInner = document.createElement('div');
        tooltipInner.classList.add('tooltip-inner');
        tooltip.appendChild(tooltipInner);
        tooltipInner.innerHTML = titleString;
      }
      tooltip.style.left = '0';
      tooltip.style.top = '0';
      tooltip.setAttribute('role','tooltip');
      !tooltip.classList.contains('tooltip') && tooltip.classList.add('tooltip');
      !tooltip.classList.contains(ops.animation) && tooltip.classList.add(ops.animation);
      !tooltip.classList.contains(placementClass) && tooltip.classList.add(placementClass);
      ops.container.appendChild(tooltip);
    }
  }
  function updateTooltip() {
    styleTip(element, tooltip, ops.placement, ops.container);
  }
  function showTooltip() {
    !tooltip.classList.contains('show') && ( tooltip.classList.add('show') );
  }
  function touchHandler(e){
    if ( tooltip && tooltip.contains(e.target) || e.target === element || element.contains(e.target)) ; else {
      self.hide();
    }
  }
  function toggleAction(action){
    action = action ? 'addEventListener' : 'removeEventListener';
    document[action]( 'touchstart', touchHandler, passiveHandler );
    window[action]( 'resize', self.hide, passiveHandler );
  }
  function showAction() {
    toggleAction(1);
    dispatchCustomEvent.call(element, shownCustomEvent);
  }
  function hideAction() {
    toggleAction();
    removeToolTip();
    dispatchCustomEvent.call(element, hiddenCustomEvent);
  }
  function toggleEvents(action) {
    action = action ? 'addEventListener' : 'removeEventListener';
    element[action](mouseClickEvents.down, self.show,false);
    element[action](mouseHoverEvents[0], self.show,false);
    element[action](mouseHoverEvents[1], self.hide,false);
  }
  self.show = function () {
    clearTimeout(timer);
    timer = setTimeout( function () {
      if (tooltip === null) {
        dispatchCustomEvent.call(element, showCustomEvent);
        if (showCustomEvent.defaultPrevented) { return; }
        if(createToolTip() !== false) {
          updateTooltip();
          showTooltip();
          !!ops.animation ? emulateTransitionEnd(tooltip, showAction) : showAction();
        }
      }
    }, 20 );
  };
  self.hide = function () {
    clearTimeout(timer);
    timer = setTimeout( function () {
      if (tooltip && tooltip.classList.contains('show')) {
        dispatchCustomEvent.call(element, hideCustomEvent);
        if (hideCustomEvent.defaultPrevented) { return; }
        tooltip.classList.remove('show');
        !!ops.animation ? emulateTransitionEnd(tooltip, hideAction) : hideAction();
      }
    }, ops.delay);
  };
  self.toggle = function () {
    if (!tooltip) { self.show(); }
    else { self.hide(); }
  };
  self.dispose = function () {
    toggleEvents();
    self.hide();
    element.setAttribute('title', element.getAttribute('data-original-title'));
    element.removeAttribute('data-original-title');
    delete element.Tooltip;
  };
  element = queryElement(element);
  element.Tooltip && element.Tooltip.dispose();
  animationData = element.getAttribute('data-animation');
  placementData = element.getAttribute('data-placement');
  delayData = element.getAttribute('data-delay');
  containerData = element.getAttribute('data-container');
  showCustomEvent = bootstrapCustomEvent('show', 'tooltip');
  shownCustomEvent = bootstrapCustomEvent('shown', 'tooltip');
  hideCustomEvent = bootstrapCustomEvent('hide', 'tooltip');
  hiddenCustomEvent = bootstrapCustomEvent('hidden', 'tooltip');
  containerElement = queryElement(options.container);
  containerDataElement = queryElement(containerData);
  modal = element.closest('.modal');
  navbarFixedTop = element.closest('.fixed-top');
  navbarFixedBottom = element.closest('.fixed-bottom');
  ops.animation = options.animation && options.animation !== 'fade' ? options.animation : animationData || 'fade';
  ops.placement = options.placement ? options.placement : placementData || 'top';
  ops.template = options.template ? options.template : null;
  ops.delay = parseInt(options.delay || delayData) || 200;
  ops.container = containerElement ? containerElement
                          : containerDataElement ? containerDataElement
                          : navbarFixedTop ? navbarFixedTop
                          : navbarFixedBottom ? navbarFixedBottom
                          : modal ? modal : document.body;
  placementClass = "bs-tooltip-" + (ops.placement);
  titleString = getTitle();
  if ( !titleString ) { return; }
  if (!element.Tooltip) {
    element.setAttribute('data-original-title',titleString);
    element.removeAttribute('title');
    toggleEvents(1);
  }
  element.Tooltip = self;
}

var componentsInit = {};

function initializeDataAPI( Constructor, collection ){
  Array.from(collection).map(function (x){ return new Constructor(x); });
}
function initCallback(lookUp){
  lookUp = lookUp || document;
  for (var component in componentsInit) {
    initializeDataAPI( componentsInit[component][0], lookUp.querySelectorAll (componentsInit[component][1]) );
  }
}

componentsInit.Alert = [ Alert, '[data-dismiss="alert"]'];
componentsInit.Button = [ Button, '[data-toggle="buttons"]' ];
componentsInit.Carousel = [ Carousel, '[data-ride="carousel"]' ];
componentsInit.Collapse = [ Collapse, '[data-toggle="collapse"]' ];
componentsInit.Dropdown = [ Dropdown, '[data-toggle="dropdown"]'];
componentsInit.Modal = [ Modal, '[data-toggle="modal"]' ];
componentsInit.Popover = [ Popover, '[data-toggle="popover"],[data-tip="popover"]' ];
componentsInit.ScrollSpy = [ ScrollSpy, '[data-spy="scroll"]' ];
componentsInit.Tab = [ Tab, '[data-toggle="tab"]' ];
componentsInit.Toast = [ Toast, '[data-dismiss="toast"]' ];
componentsInit.Tooltip = [ Tooltip, '[data-toggle="tooltip"],[data-tip="tooltip"]' ];
document.body ? initCallback() : document.addEventListener( 'DOMContentLoaded', function initWrapper(){
	initCallback();
	document.removeEventListener('DOMContentLoaded',initWrapper,false);
}, false );

function removeElementDataAPI( ConstructorName, collection ){
  Array.from(collection).map(function (x){ return x[ConstructorName].dispose(); });
}
function removeDataAPI(lookUp) {
  lookUp = lookUp || document;
  for (var component in componentsInit) {
    removeElementDataAPI( component, lookUp.querySelectorAll (componentsInit[component][1]) );
  }
}

var version = "3.0.10";

var index = {
  Alert: Alert,
  Button: Button,
  Carousel: Carousel,
  Collapse: Collapse,
  Dropdown: Dropdown,
  Modal: Modal,
  Popover: Popover,
  ScrollSpy: ScrollSpy,
  Tab: Tab,
  Toast: Toast,
  Tooltip: Tooltip,
  initCallback: initCallback,
  removeDataAPI: removeDataAPI,
  componentsInit: componentsInit,
  Version: version
};

/* harmony default export */ __webpack_exports__["default"] = (index);


/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYm9vdHN0cmFwLm5hdGl2ZS9kaXN0L2Jvb3RzdHJhcC1uYXRpdmUuZXNtLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2Isb0NBQW9DLGtDQUFrQyxFQUFFO0FBQ3hFOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0Esa0ZBQWtGLGlCQUFpQjtBQUNuRztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0RBQWdELFFBQVE7QUFDeEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsUUFBUTtBQUMzQjtBQUNBO0FBQ0E7QUFDQSxpREFBaUQsUUFBUTtBQUN6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQWlELFFBQVE7QUFDekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsNkJBQTZCLHlCQUF5QixFQUFFO0FBQ3hEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLFFBQVE7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLHVDQUF1QztBQUN2QztBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVELHVDQUF1QyxnQkFBZ0I7O0FBRXZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLFFBQVE7QUFDakM7QUFDQTtBQUNBO0FBQ0EsS0FBSyxPQUFPLGNBQWM7QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUIsUUFBUTtBQUNqQztBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLFFBQVE7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUIsUUFBUTtBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQixvQkFBb0IsUUFBUTtBQUN0RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRDQUE0QztBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNENBQTRDLDhCQUE4QjtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLHlCQUF5QixRQUFRO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EscUJBQXFCLDBCQUEwQjtBQUMvQyxzQ0FBc0MsVUFBVTtBQUNoRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRDQUE0QyxRQUFRO0FBQ3BEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxxQ0FBcUMsbUdBQW1HO0FBQ3hJO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0NBQXNDLHlEQUF5RCxFQUFFO0FBQ2pHLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQjtBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLGNBQWM7QUFDcEM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZDQUE2QyxRQUFRO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLDZDQUE2QyxRQUFRO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsbUVBQW1FO0FBQ25FO0FBQ0EsaURBQWlELGFBQWE7QUFDOUQsWUFBWSxhQUFhO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQyxRQUFRO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlGQUF5RixRQUFRO0FBQ2pHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0NBQW9DLG9CQUFvQjtBQUN4RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkNBQTZDLFFBQVE7QUFDckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2Q0FBNkMsUUFBUTtBQUNyRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNERBQTRELGFBQWE7QUFDekUsVUFBVSxhQUFhO0FBQ3ZCO0FBQ0E7QUFDQSw0REFBNEQsYUFBYTtBQUN6RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCLFFBQVE7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhCQUE4QixRQUFRO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2Q0FBNkMsYUFBYSxPQUFPO0FBQ2pFO0FBQ0E7QUFDQSxtRUFBbUU7QUFDbkU7QUFDQTtBQUNBLDZDQUE2QyxRQUFRO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4Q0FBOEM7QUFDOUM7QUFDQTtBQUNBLDZDQUE2QyxRQUFRO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQix3REFBd0Qsc0JBQXNCO0FBQ2hHLFVBQVU7QUFDVjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4Q0FBOEMsZ0JBQWdCO0FBQzlEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBLHdCQUF3Qjs7QUFFeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSwyQkFBMkIsbURBQW1EO0FBQzlFO0FBQ0E7QUFDQTtBQUNBLHlEQUF5RCxtRkFBbUY7QUFDNUksd0JBQXdCLHVEQUF1RDtBQUMvRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQztBQUNqQztBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQixpQkFBaUI7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLG1EQUFtRDtBQUNoRixLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVHQUF1RztBQUN2RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQkFBMkIsYUFBYTtBQUN4QyxVQUFVLGFBQWE7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQWlELFFBQVE7QUFDekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBaUQsUUFBUTtBQUN6RDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMENBQTBDLFFBQVE7QUFDbEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQ0FBb0Msd0JBQXdCLEVBQUU7QUFDOUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2Q0FBNkMsUUFBUTtBQUNyRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSyxPQUFPLGVBQWU7QUFDM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkNBQTZDLFFBQVE7QUFDckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzREFBc0QscUNBQXFDO0FBQzNGLFNBQVM7QUFDVCx1REFBdUQsa0NBQWtDO0FBQ3pGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPLE9BQU8sZUFBZTtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQixzREFBc0Q7QUFDNUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixhQUFhO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2Q0FBNkMsUUFBUTtBQUNyRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRDQUE0QyxRQUFRO0FBQ3BEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUdBQXVHO0FBQ3ZHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0NBQStDLFFBQVE7QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtDQUErQyxRQUFRO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsbUJBQW1CLGFBQWE7QUFDaEMsVUFBVSxhQUFhO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLFFBQVE7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSwwQ0FBMEMsMkJBQTJCLEVBQUU7QUFDdkU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQSwwQ0FBMEMscUNBQXFDLEVBQUU7QUFDakY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFZSxvRUFBSyxFQUFDIiwiZmlsZSI6InZlbmRvcnN+YXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyohXG4gICogTmF0aXZlIEphdmFTY3JpcHQgZm9yIEJvb3RzdHJhcCB2My4wLjEwIChodHRwczovL3RoZWRucC5naXRodWIuaW8vYm9vdHN0cmFwLm5hdGl2ZS8pXG4gICogQ29weXJpZ2h0IDIwMTUtMjAyMCDCqSBkbnBfdGhlbWVcbiAgKiBMaWNlbnNlZCB1bmRlciBNSVQgKGh0dHBzOi8vZ2l0aHViLmNvbS90aGVkbnAvYm9vdHN0cmFwLm5hdGl2ZS9ibG9iL21hc3Rlci9MSUNFTlNFKVxuICAqL1xudmFyIHRyYW5zaXRpb25FbmRFdmVudCA9ICd3ZWJraXRUcmFuc2l0aW9uJyBpbiBkb2N1bWVudC5oZWFkLnN0eWxlID8gJ3dlYmtpdFRyYW5zaXRpb25FbmQnIDogJ3RyYW5zaXRpb25lbmQnO1xuXG52YXIgc3VwcG9ydFRyYW5zaXRpb24gPSAnd2Via2l0VHJhbnNpdGlvbicgaW4gZG9jdW1lbnQuaGVhZC5zdHlsZSB8fCAndHJhbnNpdGlvbicgaW4gZG9jdW1lbnQuaGVhZC5zdHlsZTtcblxudmFyIHRyYW5zaXRpb25EdXJhdGlvbiA9ICd3ZWJraXRUcmFuc2l0aW9uJyBpbiBkb2N1bWVudC5oZWFkLnN0eWxlID8gJ3dlYmtpdFRyYW5zaXRpb25EdXJhdGlvbicgOiAndHJhbnNpdGlvbkR1cmF0aW9uJztcblxuZnVuY3Rpb24gZ2V0RWxlbWVudFRyYW5zaXRpb25EdXJhdGlvbihlbGVtZW50KSB7XG4gIHZhciBkdXJhdGlvbiA9IHN1cHBvcnRUcmFuc2l0aW9uID8gcGFyc2VGbG9hdChnZXRDb21wdXRlZFN0eWxlKGVsZW1lbnQpW3RyYW5zaXRpb25EdXJhdGlvbl0pIDogMDtcbiAgZHVyYXRpb24gPSB0eXBlb2YgZHVyYXRpb24gPT09ICdudW1iZXInICYmICFpc05hTihkdXJhdGlvbikgPyBkdXJhdGlvbiAqIDEwMDAgOiAwO1xuICByZXR1cm4gZHVyYXRpb247XG59XG5cbmZ1bmN0aW9uIGVtdWxhdGVUcmFuc2l0aW9uRW5kKGVsZW1lbnQsaGFuZGxlcil7XG4gIHZhciBjYWxsZWQgPSAwLCBkdXJhdGlvbiA9IGdldEVsZW1lbnRUcmFuc2l0aW9uRHVyYXRpb24oZWxlbWVudCk7XG4gIGR1cmF0aW9uID8gZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCB0cmFuc2l0aW9uRW5kRXZlbnQsIGZ1bmN0aW9uIHRyYW5zaXRpb25FbmRXcmFwcGVyKGUpe1xuICAgICAgICAgICAgICAhY2FsbGVkICYmIGhhbmRsZXIoZSksIGNhbGxlZCA9IDE7XG4gICAgICAgICAgICAgIGVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lciggdHJhbnNpdGlvbkVuZEV2ZW50LCB0cmFuc2l0aW9uRW5kV3JhcHBlcik7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICA6IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7ICFjYWxsZWQgJiYgaGFuZGxlcigpLCBjYWxsZWQgPSAxOyB9LCAxNyk7XG59XG5cbmZ1bmN0aW9uIHF1ZXJ5RWxlbWVudChzZWxlY3RvciwgcGFyZW50KSB7XG4gIHZhciBsb29rVXAgPSBwYXJlbnQgJiYgcGFyZW50IGluc3RhbmNlb2YgRWxlbWVudCA/IHBhcmVudCA6IGRvY3VtZW50O1xuICByZXR1cm4gc2VsZWN0b3IgaW5zdGFuY2VvZiBFbGVtZW50ID8gc2VsZWN0b3IgOiBsb29rVXAucXVlcnlTZWxlY3RvcihzZWxlY3Rvcik7XG59XG5cbmZ1bmN0aW9uIGJvb3RzdHJhcEN1c3RvbUV2ZW50KGV2ZW50TmFtZSwgY29tcG9uZW50TmFtZSwgcmVsYXRlZCkge1xuICB2YXIgT3JpZ2luYWxDdXN0b21FdmVudCA9IG5ldyBDdXN0b21FdmVudCggZXZlbnROYW1lICsgJy5icy4nICsgY29tcG9uZW50TmFtZSwge2NhbmNlbGFibGU6IHRydWV9KTtcbiAgT3JpZ2luYWxDdXN0b21FdmVudC5yZWxhdGVkVGFyZ2V0ID0gcmVsYXRlZDtcbiAgcmV0dXJuIE9yaWdpbmFsQ3VzdG9tRXZlbnQ7XG59XG5cbmZ1bmN0aW9uIGRpc3BhdGNoQ3VzdG9tRXZlbnQoY3VzdG9tRXZlbnQpe1xuICB0aGlzICYmIHRoaXMuZGlzcGF0Y2hFdmVudChjdXN0b21FdmVudCk7XG59XG5cbmZ1bmN0aW9uIEFsZXJ0KGVsZW1lbnQpIHtcbiAgdmFyIHNlbGYgPSB0aGlzLFxuICAgIGFsZXJ0LFxuICAgIGNsb3NlQ3VzdG9tRXZlbnQgPSBib290c3RyYXBDdXN0b21FdmVudCgnY2xvc2UnLCdhbGVydCcpLFxuICAgIGNsb3NlZEN1c3RvbUV2ZW50ID0gYm9vdHN0cmFwQ3VzdG9tRXZlbnQoJ2Nsb3NlZCcsJ2FsZXJ0Jyk7XG4gIGZ1bmN0aW9uIHRyaWdnZXJIYW5kbGVyKCkge1xuICAgIGFsZXJ0LmNsYXNzTGlzdC5jb250YWlucygnZmFkZScpID8gZW11bGF0ZVRyYW5zaXRpb25FbmQoYWxlcnQsdHJhbnNpdGlvbkVuZEhhbmRsZXIpIDogdHJhbnNpdGlvbkVuZEhhbmRsZXIoKTtcbiAgfVxuICBmdW5jdGlvbiB0b2dnbGVFdmVudHMoYWN0aW9uKXtcbiAgICBhY3Rpb24gPSBhY3Rpb24gPyAnYWRkRXZlbnRMaXN0ZW5lcicgOiAncmVtb3ZlRXZlbnRMaXN0ZW5lcic7XG4gICAgZWxlbWVudFthY3Rpb25dKCdjbGljaycsY2xpY2tIYW5kbGVyLGZhbHNlKTtcbiAgfVxuICBmdW5jdGlvbiBjbGlja0hhbmRsZXIoZSkge1xuICAgIGFsZXJ0ID0gZSAmJiBlLnRhcmdldC5jbG9zZXN0KFwiLmFsZXJ0XCIpO1xuICAgIGVsZW1lbnQgPSBxdWVyeUVsZW1lbnQoJ1tkYXRhLWRpc21pc3M9XCJhbGVydFwiXScsYWxlcnQpO1xuICAgIGVsZW1lbnQgJiYgYWxlcnQgJiYgKGVsZW1lbnQgPT09IGUudGFyZ2V0IHx8IGVsZW1lbnQuY29udGFpbnMoZS50YXJnZXQpKSAmJiBzZWxmLmNsb3NlKCk7XG4gIH1cbiAgZnVuY3Rpb24gdHJhbnNpdGlvbkVuZEhhbmRsZXIoKSB7XG4gICAgdG9nZ2xlRXZlbnRzKCk7XG4gICAgYWxlcnQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChhbGVydCk7XG4gICAgZGlzcGF0Y2hDdXN0b21FdmVudC5jYWxsKGFsZXJ0LGNsb3NlZEN1c3RvbUV2ZW50KTtcbiAgfVxuICBzZWxmLmNsb3NlID0gZnVuY3Rpb24gKCkge1xuICAgIGlmICggYWxlcnQgJiYgZWxlbWVudCAmJiBhbGVydC5jbGFzc0xpc3QuY29udGFpbnMoJ3Nob3cnKSApIHtcbiAgICAgIGRpc3BhdGNoQ3VzdG9tRXZlbnQuY2FsbChhbGVydCxjbG9zZUN1c3RvbUV2ZW50KTtcbiAgICAgIGlmICggY2xvc2VDdXN0b21FdmVudC5kZWZhdWx0UHJldmVudGVkICkgeyByZXR1cm47IH1cbiAgICAgIHNlbGYuZGlzcG9zZSgpO1xuICAgICAgYWxlcnQuY2xhc3NMaXN0LnJlbW92ZSgnc2hvdycpO1xuICAgICAgdHJpZ2dlckhhbmRsZXIoKTtcbiAgICB9XG4gIH07XG4gIHNlbGYuZGlzcG9zZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB0b2dnbGVFdmVudHMoKTtcbiAgICBkZWxldGUgZWxlbWVudC5BbGVydDtcbiAgfTtcbiAgZWxlbWVudCA9IHF1ZXJ5RWxlbWVudChlbGVtZW50KTtcbiAgYWxlcnQgPSBlbGVtZW50LmNsb3Nlc3QoJy5hbGVydCcpO1xuICBlbGVtZW50LkFsZXJ0ICYmIGVsZW1lbnQuQWxlcnQuZGlzcG9zZSgpO1xuICBpZiAoICFlbGVtZW50LkFsZXJ0ICkge1xuICAgIHRvZ2dsZUV2ZW50cygxKTtcbiAgfVxuICBzZWxmLmVsZW1lbnQgPSBlbGVtZW50O1xuICBlbGVtZW50LkFsZXJ0ID0gc2VsZjtcbn1cblxuZnVuY3Rpb24gQnV0dG9uKGVsZW1lbnQpIHtcbiAgdmFyIHNlbGYgPSB0aGlzLCBsYWJlbHMsXG4gICAgICBjaGFuZ2VDdXN0b21FdmVudCA9IGJvb3RzdHJhcEN1c3RvbUV2ZW50KCdjaGFuZ2UnLCAnYnV0dG9uJyk7XG4gIGZ1bmN0aW9uIHRvZ2dsZShlKSB7XG4gICAgdmFyIGlucHV0LFxuICAgICAgICBsYWJlbCA9IGUudGFyZ2V0LnRhZ05hbWUgPT09ICdMQUJFTCcgPyBlLnRhcmdldFxuICAgICAgICAgICAgICA6IGUudGFyZ2V0LmNsb3Nlc3QoJ0xBQkVMJykgPyBlLnRhcmdldC5jbG9zZXN0KCdMQUJFTCcpIDogbnVsbDtcbiAgICBpbnB1dCA9IGxhYmVsICYmIGxhYmVsLmdldEVsZW1lbnRzQnlUYWdOYW1lKCdJTlBVVCcpWzBdO1xuICAgIGlmICggIWlucHV0ICkgeyByZXR1cm47IH1cbiAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwoaW5wdXQsIGNoYW5nZUN1c3RvbUV2ZW50KTtcbiAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwoZWxlbWVudCwgY2hhbmdlQ3VzdG9tRXZlbnQpO1xuICAgIGlmICggaW5wdXQudHlwZSA9PT0gJ2NoZWNrYm94JyApIHtcbiAgICAgIGlmICggY2hhbmdlQ3VzdG9tRXZlbnQuZGVmYXVsdFByZXZlbnRlZCApIHsgcmV0dXJuOyB9XG4gICAgICBpZiAoICFpbnB1dC5jaGVja2VkICkge1xuICAgICAgICBsYWJlbC5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcbiAgICAgICAgaW5wdXQuZ2V0QXR0cmlidXRlKCdjaGVja2VkJyk7XG4gICAgICAgIGlucHV0LnNldEF0dHJpYnV0ZSgnY2hlY2tlZCcsJ2NoZWNrZWQnKTtcbiAgICAgICAgaW5wdXQuY2hlY2tlZCA9IHRydWU7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBsYWJlbC5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKTtcbiAgICAgICAgaW5wdXQuZ2V0QXR0cmlidXRlKCdjaGVja2VkJyk7XG4gICAgICAgIGlucHV0LnJlbW92ZUF0dHJpYnV0ZSgnY2hlY2tlZCcpO1xuICAgICAgICBpbnB1dC5jaGVja2VkID0gZmFsc2U7XG4gICAgICB9XG4gICAgICBpZiAoIWVsZW1lbnQudG9nZ2xlZCkge1xuICAgICAgICBlbGVtZW50LnRvZ2dsZWQgPSB0cnVlO1xuICAgICAgfVxuICAgIH1cbiAgICBpZiAoIGlucHV0LnR5cGUgPT09ICdyYWRpbycgJiYgIWVsZW1lbnQudG9nZ2xlZCApIHtcbiAgICAgIGlmICggY2hhbmdlQ3VzdG9tRXZlbnQuZGVmYXVsdFByZXZlbnRlZCApIHsgcmV0dXJuOyB9XG4gICAgICBpZiAoICFpbnB1dC5jaGVja2VkIHx8IChlLnNjcmVlblggPT09IDAgJiYgZS5zY3JlZW5ZID09IDApICkge1xuICAgICAgICBsYWJlbC5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcbiAgICAgICAgbGFiZWwuY2xhc3NMaXN0LmFkZCgnZm9jdXMnKTtcbiAgICAgICAgaW5wdXQuc2V0QXR0cmlidXRlKCdjaGVja2VkJywnY2hlY2tlZCcpO1xuICAgICAgICBpbnB1dC5jaGVja2VkID0gdHJ1ZTtcbiAgICAgICAgZWxlbWVudC50b2dnbGVkID0gdHJ1ZTtcbiAgICAgICAgQXJyYXkuZnJvbShsYWJlbHMpLm1hcChmdW5jdGlvbiAob3RoZXJMYWJlbCl7XG4gICAgICAgICAgdmFyIG90aGVySW5wdXQgPSBvdGhlckxhYmVsLmdldEVsZW1lbnRzQnlUYWdOYW1lKCdJTlBVVCcpWzBdO1xuICAgICAgICAgIGlmICggb3RoZXJMYWJlbCAhPT0gbGFiZWwgJiYgb3RoZXJMYWJlbC5jbGFzc0xpc3QuY29udGFpbnMoJ2FjdGl2ZScpICkgIHtcbiAgICAgICAgICAgIGRpc3BhdGNoQ3VzdG9tRXZlbnQuY2FsbChvdGhlcklucHV0LCBjaGFuZ2VDdXN0b21FdmVudCk7XG4gICAgICAgICAgICBvdGhlckxhYmVsLmNsYXNzTGlzdC5yZW1vdmUoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgb3RoZXJJbnB1dC5yZW1vdmVBdHRyaWJ1dGUoJ2NoZWNrZWQnKTtcbiAgICAgICAgICAgIG90aGVySW5wdXQuY2hlY2tlZCA9IGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuICAgIHNldFRpbWVvdXQoIGZ1bmN0aW9uICgpIHsgZWxlbWVudC50b2dnbGVkID0gZmFsc2U7IH0sIDUwICk7XG4gIH1cbiAgZnVuY3Rpb24ga2V5SGFuZGxlcihlKSB7XG4gICAgdmFyIGtleSA9IGUud2hpY2ggfHwgZS5rZXlDb2RlO1xuICAgIGtleSA9PT0gMzIgJiYgZS50YXJnZXQgPT09IGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQgJiYgdG9nZ2xlKGUpO1xuICB9XG4gIGZ1bmN0aW9uIHByZXZlbnRTY3JvbGwoZSkge1xuICAgIHZhciBrZXkgPSBlLndoaWNoIHx8IGUua2V5Q29kZTtcbiAgICBrZXkgPT09IDMyICYmIGUucHJldmVudERlZmF1bHQoKTtcbiAgfVxuICBmdW5jdGlvbiBmb2N1c1RvZ2dsZShlKSB7XG4gICAgaWYgKGUudGFyZ2V0LnRhZ05hbWUgPT09ICdJTlBVVCcgKSB7XG4gICAgICB2YXIgYWN0aW9uID0gZS50eXBlID09PSAnZm9jdXNpbicgPyAnYWRkJyA6ICdyZW1vdmUnO1xuICAgICAgZS50YXJnZXQuY2xvc2VzdCgnLmJ0bicpLmNsYXNzTGlzdFthY3Rpb25dKCdmb2N1cycpO1xuICAgIH1cbiAgfVxuICBmdW5jdGlvbiB0b2dnbGVFdmVudHMoYWN0aW9uKSB7XG4gICAgYWN0aW9uID0gYWN0aW9uID8gJ2FkZEV2ZW50TGlzdGVuZXInIDogJ3JlbW92ZUV2ZW50TGlzdGVuZXInO1xuICAgIGVsZW1lbnRbYWN0aW9uXSgnY2xpY2snLHRvZ2dsZSxmYWxzZSApO1xuICAgIGVsZW1lbnRbYWN0aW9uXSgna2V5dXAnLGtleUhhbmRsZXIsZmFsc2UpLCBlbGVtZW50W2FjdGlvbl0oJ2tleWRvd24nLHByZXZlbnRTY3JvbGwsZmFsc2UpO1xuICAgIGVsZW1lbnRbYWN0aW9uXSgnZm9jdXNpbicsZm9jdXNUb2dnbGUsZmFsc2UpLCBlbGVtZW50W2FjdGlvbl0oJ2ZvY3Vzb3V0Jyxmb2N1c1RvZ2dsZSxmYWxzZSk7XG4gIH1cbiAgc2VsZi5kaXNwb3NlID0gZnVuY3Rpb24gKCkge1xuICAgIHRvZ2dsZUV2ZW50cygpO1xuICAgIGRlbGV0ZSBlbGVtZW50LkJ1dHRvbjtcbiAgfTtcbiAgZWxlbWVudCA9IHF1ZXJ5RWxlbWVudChlbGVtZW50KTtcbiAgZWxlbWVudC5CdXR0b24gJiYgZWxlbWVudC5CdXR0b24uZGlzcG9zZSgpO1xuICBsYWJlbHMgPSBlbGVtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2J0bicpO1xuICBpZiAoIWxhYmVscy5sZW5ndGgpIHsgcmV0dXJuOyB9XG4gIGlmICggIWVsZW1lbnQuQnV0dG9uICkge1xuICAgIHRvZ2dsZUV2ZW50cygxKTtcbiAgfVxuICBlbGVtZW50LnRvZ2dsZWQgPSBmYWxzZTtcbiAgZWxlbWVudC5CdXR0b24gPSBzZWxmO1xuICBBcnJheS5mcm9tKGxhYmVscykubWFwKGZ1bmN0aW9uIChidG4pe1xuICAgICFidG4uY2xhc3NMaXN0LmNvbnRhaW5zKCdhY3RpdmUnKVxuICAgICAgJiYgcXVlcnlFbGVtZW50KCdpbnB1dDpjaGVja2VkJyxidG4pXG4gICAgICAmJiBidG4uY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XG4gICAgYnRuLmNsYXNzTGlzdC5jb250YWlucygnYWN0aXZlJylcbiAgICAgICYmICFxdWVyeUVsZW1lbnQoJ2lucHV0OmNoZWNrZWQnLGJ0bilcbiAgICAgICYmIGJ0bi5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKTtcbiAgfSk7XG59XG5cbnZhciBtb3VzZUhvdmVyRXZlbnRzID0gKCdvbm1vdXNlbGVhdmUnIGluIGRvY3VtZW50KSA/IFsgJ21vdXNlZW50ZXInLCAnbW91c2VsZWF2ZSddIDogWyAnbW91c2VvdmVyJywgJ21vdXNlb3V0JyBdO1xuXG52YXIgc3VwcG9ydFBhc3NpdmUgPSAoZnVuY3Rpb24gKCkge1xuICB2YXIgcmVzdWx0ID0gZmFsc2U7XG4gIHRyeSB7XG4gICAgdmFyIG9wdHMgPSBPYmplY3QuZGVmaW5lUHJvcGVydHkoe30sICdwYXNzaXZlJywge1xuICAgICAgZ2V0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgcmVzdWx0ID0gdHJ1ZTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZnVuY3Rpb24gd3JhcCgpe1xuICAgICAgZG9jdW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIHdyYXAsIG9wdHMpO1xuICAgIH0sIG9wdHMpO1xuICB9IGNhdGNoIChlKSB7fVxuICByZXR1cm4gcmVzdWx0O1xufSkoKTtcblxudmFyIHBhc3NpdmVIYW5kbGVyID0gc3VwcG9ydFBhc3NpdmUgPyB7IHBhc3NpdmU6IHRydWUgfSA6IGZhbHNlO1xuXG5mdW5jdGlvbiBpc0VsZW1lbnRJblNjcm9sbFJhbmdlKGVsZW1lbnQpIHtcbiAgdmFyIGJjciA9IGVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksXG4gICAgICB2aWV3cG9ydEhlaWdodCA9IHdpbmRvdy5pbm5lckhlaWdodCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0O1xuICByZXR1cm4gYmNyLnRvcCA8PSB2aWV3cG9ydEhlaWdodCAmJiBiY3IuYm90dG9tID49IDA7XG59XG5cbmZ1bmN0aW9uIENhcm91c2VsIChlbGVtZW50LG9wdGlvbnMpIHtcbiAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG4gIHZhciBzZWxmID0gdGhpcyxcbiAgICB2YXJzLCBvcHMsXG4gICAgc2xpZGVDdXN0b21FdmVudCwgc2xpZEN1c3RvbUV2ZW50LFxuICAgIHNsaWRlcywgbGVmdEFycm93LCByaWdodEFycm93LCBpbmRpY2F0b3IsIGluZGljYXRvcnM7XG4gIGZ1bmN0aW9uIHBhdXNlSGFuZGxlcigpIHtcbiAgICBpZiAoIG9wcy5pbnRlcnZhbCAhPT1mYWxzZSAmJiAhZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoJ3BhdXNlZCcpICkge1xuICAgICAgZWxlbWVudC5jbGFzc0xpc3QuYWRkKCdwYXVzZWQnKTtcbiAgICAgICF2YXJzLmlzU2xpZGluZyAmJiAoIGNsZWFySW50ZXJ2YWwodmFycy50aW1lciksIHZhcnMudGltZXIgPSBudWxsICk7XG4gICAgfVxuICB9XG4gIGZ1bmN0aW9uIHJlc3VtZUhhbmRsZXIoKSB7XG4gICAgaWYgKCBvcHMuaW50ZXJ2YWwgIT09IGZhbHNlICYmIGVsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKCdwYXVzZWQnKSApIHtcbiAgICAgIGVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgncGF1c2VkJyk7XG4gICAgICAhdmFycy5pc1NsaWRpbmcgJiYgKCBjbGVhckludGVydmFsKHZhcnMudGltZXIpLCB2YXJzLnRpbWVyID0gbnVsbCApO1xuICAgICAgIXZhcnMuaXNTbGlkaW5nICYmIHNlbGYuY3ljbGUoKTtcbiAgICB9XG4gIH1cbiAgZnVuY3Rpb24gaW5kaWNhdG9ySGFuZGxlcihlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIGlmICh2YXJzLmlzU2xpZGluZykgeyByZXR1cm47IH1cbiAgICB2YXIgZXZlbnRUYXJnZXQgPSBlLnRhcmdldDtcbiAgICBpZiAoIGV2ZW50VGFyZ2V0ICYmICFldmVudFRhcmdldC5jbGFzc0xpc3QuY29udGFpbnMoJ2FjdGl2ZScpICYmIGV2ZW50VGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS1zbGlkZS10bycpICkge1xuICAgICAgdmFycy5pbmRleCA9IHBhcnNlSW50KCBldmVudFRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtc2xpZGUtdG8nKSk7XG4gICAgfSBlbHNlIHsgcmV0dXJuIGZhbHNlOyB9XG4gICAgc2VsZi5zbGlkZVRvKCB2YXJzLmluZGV4ICk7XG4gIH1cbiAgZnVuY3Rpb24gY29udHJvbHNIYW5kbGVyKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgaWYgKHZhcnMuaXNTbGlkaW5nKSB7IHJldHVybjsgfVxuICAgIHZhciBldmVudFRhcmdldCA9IGUuY3VycmVudFRhcmdldCB8fCBlLnNyY0VsZW1lbnQ7XG4gICAgaWYgKCBldmVudFRhcmdldCA9PT0gcmlnaHRBcnJvdyApIHtcbiAgICAgIHZhcnMuaW5kZXgrKztcbiAgICB9IGVsc2UgaWYgKCBldmVudFRhcmdldCA9PT0gbGVmdEFycm93ICkge1xuICAgICAgdmFycy5pbmRleC0tO1xuICAgIH1cbiAgICBzZWxmLnNsaWRlVG8oIHZhcnMuaW5kZXggKTtcbiAgfVxuICBmdW5jdGlvbiBrZXlIYW5kbGVyKHJlZikge1xuICAgIHZhciB3aGljaCA9IHJlZi53aGljaDtcbiAgICBpZiAodmFycy5pc1NsaWRpbmcpIHsgcmV0dXJuOyB9XG4gICAgc3dpdGNoICh3aGljaCkge1xuICAgICAgY2FzZSAzOTpcbiAgICAgICAgdmFycy5pbmRleCsrO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgMzc6XG4gICAgICAgIHZhcnMuaW5kZXgtLTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBkZWZhdWx0OiByZXR1cm47XG4gICAgfVxuICAgIHNlbGYuc2xpZGVUbyggdmFycy5pbmRleCApO1xuICB9XG4gIGZ1bmN0aW9uIHRvZ2dsZUV2ZW50cyhhY3Rpb24pIHtcbiAgICBhY3Rpb24gPSBhY3Rpb24gPyAnYWRkRXZlbnRMaXN0ZW5lcicgOiAncmVtb3ZlRXZlbnRMaXN0ZW5lcic7XG4gICAgaWYgKCBvcHMucGF1c2UgJiYgb3BzLmludGVydmFsICkge1xuICAgICAgZWxlbWVudFthY3Rpb25dKCBtb3VzZUhvdmVyRXZlbnRzWzBdLCBwYXVzZUhhbmRsZXIsIGZhbHNlICk7XG4gICAgICBlbGVtZW50W2FjdGlvbl0oIG1vdXNlSG92ZXJFdmVudHNbMV0sIHJlc3VtZUhhbmRsZXIsIGZhbHNlICk7XG4gICAgICBlbGVtZW50W2FjdGlvbl0oICd0b3VjaHN0YXJ0JywgcGF1c2VIYW5kbGVyLCBwYXNzaXZlSGFuZGxlciApO1xuICAgICAgZWxlbWVudFthY3Rpb25dKCAndG91Y2hlbmQnLCByZXN1bWVIYW5kbGVyLCBwYXNzaXZlSGFuZGxlciApO1xuICAgIH1cbiAgICBvcHMudG91Y2ggJiYgc2xpZGVzLmxlbmd0aCA+IDEgJiYgZWxlbWVudFthY3Rpb25dKCAndG91Y2hzdGFydCcsIHRvdWNoRG93bkhhbmRsZXIsIHBhc3NpdmVIYW5kbGVyICk7XG4gICAgcmlnaHRBcnJvdyAmJiByaWdodEFycm93W2FjdGlvbl0oICdjbGljaycsIGNvbnRyb2xzSGFuZGxlcixmYWxzZSApO1xuICAgIGxlZnRBcnJvdyAmJiBsZWZ0QXJyb3dbYWN0aW9uXSggJ2NsaWNrJywgY29udHJvbHNIYW5kbGVyLGZhbHNlICk7XG4gICAgaW5kaWNhdG9yICYmIGluZGljYXRvclthY3Rpb25dKCAnY2xpY2snLCBpbmRpY2F0b3JIYW5kbGVyLGZhbHNlICk7XG4gICAgb3BzLmtleWJvYXJkICYmIHdpbmRvd1thY3Rpb25dKCAna2V5ZG93bicsIGtleUhhbmRsZXIsZmFsc2UgKTtcbiAgfVxuICBmdW5jdGlvbiB0b2dnbGVUb3VjaEV2ZW50cyhhY3Rpb24pIHtcbiAgICBhY3Rpb24gPSBhY3Rpb24gPyAnYWRkRXZlbnRMaXN0ZW5lcicgOiAncmVtb3ZlRXZlbnRMaXN0ZW5lcic7XG4gICAgZWxlbWVudFthY3Rpb25dKCAndG91Y2htb3ZlJywgdG91Y2hNb3ZlSGFuZGxlciwgcGFzc2l2ZUhhbmRsZXIgKTtcbiAgICBlbGVtZW50W2FjdGlvbl0oICd0b3VjaGVuZCcsIHRvdWNoRW5kSGFuZGxlciwgcGFzc2l2ZUhhbmRsZXIgKTtcbiAgfVxuICBmdW5jdGlvbiB0b3VjaERvd25IYW5kbGVyKGUpIHtcbiAgICBpZiAoIHZhcnMuaXNUb3VjaCApIHsgcmV0dXJuOyB9XG4gICAgdmFycy50b3VjaFBvc2l0aW9uLnN0YXJ0WCA9IGUuY2hhbmdlZFRvdWNoZXNbMF0ucGFnZVg7XG4gICAgaWYgKCBlbGVtZW50LmNvbnRhaW5zKGUudGFyZ2V0KSApIHtcbiAgICAgIHZhcnMuaXNUb3VjaCA9IHRydWU7XG4gICAgICB0b2dnbGVUb3VjaEV2ZW50cygxKTtcbiAgICB9XG4gIH1cbiAgZnVuY3Rpb24gdG91Y2hNb3ZlSGFuZGxlcihlKSB7XG4gICAgaWYgKCAhdmFycy5pc1RvdWNoICkgeyBlLnByZXZlbnREZWZhdWx0KCk7IHJldHVybjsgfVxuICAgIHZhcnMudG91Y2hQb3NpdGlvbi5jdXJyZW50WCA9IGUuY2hhbmdlZFRvdWNoZXNbMF0ucGFnZVg7XG4gICAgaWYgKCBlLnR5cGUgPT09ICd0b3VjaG1vdmUnICYmIGUuY2hhbmdlZFRvdWNoZXMubGVuZ3RoID4gMSApIHtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gIH1cbiAgZnVuY3Rpb24gdG91Y2hFbmRIYW5kbGVyIChlKSB7XG4gICAgaWYgKCAhdmFycy5pc1RvdWNoIHx8IHZhcnMuaXNTbGlkaW5nICkgeyByZXR1cm4gfVxuICAgIHZhcnMudG91Y2hQb3NpdGlvbi5lbmRYID0gdmFycy50b3VjaFBvc2l0aW9uLmN1cnJlbnRYIHx8IGUuY2hhbmdlZFRvdWNoZXNbMF0ucGFnZVg7XG4gICAgaWYgKCB2YXJzLmlzVG91Y2ggKSB7XG4gICAgICBpZiAoICghZWxlbWVudC5jb250YWlucyhlLnRhcmdldCkgfHwgIWVsZW1lbnQuY29udGFpbnMoZS5yZWxhdGVkVGFyZ2V0KSApXG4gICAgICAgICAgJiYgTWF0aC5hYnModmFycy50b3VjaFBvc2l0aW9uLnN0YXJ0WCAtIHZhcnMudG91Y2hQb3NpdGlvbi5lbmRYKSA8IDc1ICkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAoIHZhcnMudG91Y2hQb3NpdGlvbi5jdXJyZW50WCA8IHZhcnMudG91Y2hQb3NpdGlvbi5zdGFydFggKSB7XG4gICAgICAgICAgdmFycy5pbmRleCsrO1xuICAgICAgICB9IGVsc2UgaWYgKCB2YXJzLnRvdWNoUG9zaXRpb24uY3VycmVudFggPiB2YXJzLnRvdWNoUG9zaXRpb24uc3RhcnRYICkge1xuICAgICAgICAgIHZhcnMuaW5kZXgtLTtcbiAgICAgICAgfVxuICAgICAgICB2YXJzLmlzVG91Y2ggPSBmYWxzZTtcbiAgICAgICAgc2VsZi5zbGlkZVRvKHZhcnMuaW5kZXgpO1xuICAgICAgfVxuICAgICAgdG9nZ2xlVG91Y2hFdmVudHMoKTtcbiAgICB9XG4gIH1cbiAgZnVuY3Rpb24gc2V0QWN0aXZlUGFnZShwYWdlSW5kZXgpIHtcbiAgICBBcnJheS5mcm9tKGluZGljYXRvcnMpLm1hcChmdW5jdGlvbiAoeCl7eC5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKTt9KTtcbiAgICBpbmRpY2F0b3JzW3BhZ2VJbmRleF0gJiYgaW5kaWNhdG9yc1twYWdlSW5kZXhdLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpO1xuICB9XG4gIGZ1bmN0aW9uIHRyYW5zaXRpb25FbmRIYW5kbGVyKGUpe1xuICAgIGlmICh2YXJzLnRvdWNoUG9zaXRpb24pe1xuICAgICAgdmFyIG5leHQgPSB2YXJzLmluZGV4LFxuICAgICAgICAgIHRpbWVvdXQgPSBlICYmIGUudGFyZ2V0ICE9PSBzbGlkZXNbbmV4dF0gPyBlLmVsYXBzZWRUaW1lKjEwMDArMTAwIDogMjAsXG4gICAgICAgICAgYWN0aXZlSXRlbSA9IHNlbGYuZ2V0QWN0aXZlSW5kZXgoKSxcbiAgICAgICAgICBvcmllbnRhdGlvbiA9IHZhcnMuZGlyZWN0aW9uID09PSAnbGVmdCcgPyAnbmV4dCcgOiAncHJldic7XG4gICAgICB2YXJzLmlzU2xpZGluZyAmJiBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHZhcnMudG91Y2hQb3NpdGlvbil7XG4gICAgICAgICAgdmFycy5pc1NsaWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICBzbGlkZXNbbmV4dF0uY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XG4gICAgICAgICAgc2xpZGVzW2FjdGl2ZUl0ZW1dLmNsYXNzTGlzdC5yZW1vdmUoJ2FjdGl2ZScpO1xuICAgICAgICAgIHNsaWRlc1tuZXh0XS5jbGFzc0xpc3QucmVtb3ZlKChcImNhcm91c2VsLWl0ZW0tXCIgKyBvcmllbnRhdGlvbikpO1xuICAgICAgICAgIHNsaWRlc1tuZXh0XS5jbGFzc0xpc3QucmVtb3ZlKChcImNhcm91c2VsLWl0ZW0tXCIgKyAodmFycy5kaXJlY3Rpb24pKSk7XG4gICAgICAgICAgc2xpZGVzW2FjdGl2ZUl0ZW1dLmNsYXNzTGlzdC5yZW1vdmUoKFwiY2Fyb3VzZWwtaXRlbS1cIiArICh2YXJzLmRpcmVjdGlvbikpKTtcbiAgICAgICAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwoZWxlbWVudCwgc2xpZEN1c3RvbUV2ZW50KTtcbiAgICAgICAgICBpZiAoICFkb2N1bWVudC5oaWRkZW4gJiYgb3BzLmludGVydmFsICYmICFlbGVtZW50LmNsYXNzTGlzdC5jb250YWlucygncGF1c2VkJykgKSB7XG4gICAgICAgICAgICBzZWxmLmN5Y2xlKCk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9LCB0aW1lb3V0KTtcbiAgICB9XG4gIH1cbiAgc2VsZi5jeWNsZSA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAodmFycy50aW1lcikge1xuICAgICAgY2xlYXJJbnRlcnZhbCh2YXJzLnRpbWVyKTtcbiAgICAgIHZhcnMudGltZXIgPSBudWxsO1xuICAgIH1cbiAgICB2YXJzLnRpbWVyID0gc2V0SW50ZXJ2YWwoZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGlkeCA9IHZhcnMuaW5kZXggfHwgc2VsZi5nZXRBY3RpdmVJbmRleCgpO1xuICAgICAgaXNFbGVtZW50SW5TY3JvbGxSYW5nZShlbGVtZW50KSAmJiAoaWR4KyssIHNlbGYuc2xpZGVUbyggaWR4ICkgKTtcbiAgICB9LCBvcHMuaW50ZXJ2YWwpO1xuICB9O1xuICBzZWxmLnNsaWRlVG8gPSBmdW5jdGlvbiAobmV4dCkge1xuICAgIGlmICh2YXJzLmlzU2xpZGluZykgeyByZXR1cm47IH1cbiAgICB2YXIgYWN0aXZlSXRlbSA9IHNlbGYuZ2V0QWN0aXZlSW5kZXgoKSwgb3JpZW50YXRpb247XG4gICAgaWYgKCBhY3RpdmVJdGVtID09PSBuZXh0ICkge1xuICAgICAgcmV0dXJuO1xuICAgIH0gZWxzZSBpZiAgKCAoYWN0aXZlSXRlbSA8IG5leHQgKSB8fCAoYWN0aXZlSXRlbSA9PT0gMCAmJiBuZXh0ID09PSBzbGlkZXMubGVuZ3RoIC0xICkgKSB7XG4gICAgICB2YXJzLmRpcmVjdGlvbiA9ICdsZWZ0JztcbiAgICB9IGVsc2UgaWYgICggKGFjdGl2ZUl0ZW0gPiBuZXh0KSB8fCAoYWN0aXZlSXRlbSA9PT0gc2xpZGVzLmxlbmd0aCAtIDEgJiYgbmV4dCA9PT0gMCApICkge1xuICAgICAgdmFycy5kaXJlY3Rpb24gPSAncmlnaHQnO1xuICAgIH1cbiAgICBpZiAoIG5leHQgPCAwICkgeyBuZXh0ID0gc2xpZGVzLmxlbmd0aCAtIDE7IH1cbiAgICBlbHNlIGlmICggbmV4dCA+PSBzbGlkZXMubGVuZ3RoICl7IG5leHQgPSAwOyB9XG4gICAgb3JpZW50YXRpb24gPSB2YXJzLmRpcmVjdGlvbiA9PT0gJ2xlZnQnID8gJ25leHQnIDogJ3ByZXYnO1xuICAgIHNsaWRlQ3VzdG9tRXZlbnQgPSBib290c3RyYXBDdXN0b21FdmVudCgnc2xpZGUnLCAnY2Fyb3VzZWwnLCBzbGlkZXNbbmV4dF0pO1xuICAgIHNsaWRDdXN0b21FdmVudCA9IGJvb3RzdHJhcEN1c3RvbUV2ZW50KCdzbGlkJywgJ2Nhcm91c2VsJywgc2xpZGVzW25leHRdKTtcbiAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwoZWxlbWVudCwgc2xpZGVDdXN0b21FdmVudCk7XG4gICAgaWYgKHNsaWRlQ3VzdG9tRXZlbnQuZGVmYXVsdFByZXZlbnRlZCkgeyByZXR1cm47IH1cbiAgICB2YXJzLmluZGV4ID0gbmV4dDtcbiAgICB2YXJzLmlzU2xpZGluZyA9IHRydWU7XG4gICAgY2xlYXJJbnRlcnZhbCh2YXJzLnRpbWVyKTtcbiAgICB2YXJzLnRpbWVyID0gbnVsbDtcbiAgICBzZXRBY3RpdmVQYWdlKCBuZXh0ICk7XG4gICAgaWYgKCBnZXRFbGVtZW50VHJhbnNpdGlvbkR1cmF0aW9uKHNsaWRlc1tuZXh0XSkgJiYgZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoJ3NsaWRlJykgKSB7XG4gICAgICBzbGlkZXNbbmV4dF0uY2xhc3NMaXN0LmFkZCgoXCJjYXJvdXNlbC1pdGVtLVwiICsgb3JpZW50YXRpb24pKTtcbiAgICAgIHNsaWRlc1tuZXh0XS5vZmZzZXRXaWR0aDtcbiAgICAgIHNsaWRlc1tuZXh0XS5jbGFzc0xpc3QuYWRkKChcImNhcm91c2VsLWl0ZW0tXCIgKyAodmFycy5kaXJlY3Rpb24pKSk7XG4gICAgICBzbGlkZXNbYWN0aXZlSXRlbV0uY2xhc3NMaXN0LmFkZCgoXCJjYXJvdXNlbC1pdGVtLVwiICsgKHZhcnMuZGlyZWN0aW9uKSkpO1xuICAgICAgZW11bGF0ZVRyYW5zaXRpb25FbmQoc2xpZGVzW25leHRdLCB0cmFuc2l0aW9uRW5kSGFuZGxlcik7XG4gICAgfSBlbHNlIHtcbiAgICAgIHNsaWRlc1tuZXh0XS5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcbiAgICAgIHNsaWRlc1tuZXh0XS5vZmZzZXRXaWR0aDtcbiAgICAgIHNsaWRlc1thY3RpdmVJdGVtXS5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKTtcbiAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXJzLmlzU2xpZGluZyA9IGZhbHNlO1xuICAgICAgICBpZiAoIG9wcy5pbnRlcnZhbCAmJiBlbGVtZW50ICYmICFlbGVtZW50LmNsYXNzTGlzdC5jb250YWlucygncGF1c2VkJykgKSB7XG4gICAgICAgICAgc2VsZi5jeWNsZSgpO1xuICAgICAgICB9XG4gICAgICAgIGRpc3BhdGNoQ3VzdG9tRXZlbnQuY2FsbChlbGVtZW50LCBzbGlkQ3VzdG9tRXZlbnQpO1xuICAgICAgfSwgMTAwICk7XG4gICAgfVxuICB9O1xuICBzZWxmLmdldEFjdGl2ZUluZGV4ID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gQXJyYXkuZnJvbShzbGlkZXMpLmluZGV4T2YoZWxlbWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdjYXJvdXNlbC1pdGVtIGFjdGl2ZScpWzBdKSB8fCAwOyB9O1xuICBzZWxmLmRpc3Bvc2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGl0ZW1DbGFzc2VzID0gWydsZWZ0JywncmlnaHQnLCdwcmV2JywnbmV4dCddO1xuICAgIEFycmF5LmZyb20oc2xpZGVzKS5tYXAoZnVuY3Rpb24gKHNsaWRlLGlkeCkge1xuICAgICAgc2xpZGUuY2xhc3NMaXN0LmNvbnRhaW5zKCdhY3RpdmUnKSAmJiBzZXRBY3RpdmVQYWdlKCBpZHggKTtcbiAgICAgIGl0ZW1DbGFzc2VzLm1hcChmdW5jdGlvbiAoY2xzKSB7IHJldHVybiBzbGlkZS5jbGFzc0xpc3QucmVtb3ZlKChcImNhcm91c2VsLWl0ZW0tXCIgKyBjbHMpKTsgfSk7XG4gICAgfSk7XG4gICAgY2xlYXJJbnRlcnZhbCh2YXJzLnRpbWVyKTtcbiAgICB0b2dnbGVFdmVudHMoKTtcbiAgICB2YXJzID0ge307XG4gICAgb3BzID0ge307XG4gICAgZGVsZXRlIGVsZW1lbnQuQ2Fyb3VzZWw7XG4gIH07XG4gIGVsZW1lbnQgPSBxdWVyeUVsZW1lbnQoIGVsZW1lbnQgKTtcbiAgZWxlbWVudC5DYXJvdXNlbCAmJiBlbGVtZW50LkNhcm91c2VsLmRpc3Bvc2UoKTtcbiAgc2xpZGVzID0gZWxlbWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdjYXJvdXNlbC1pdGVtJyk7XG4gIGxlZnRBcnJvdyA9IGVsZW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnY2Fyb3VzZWwtY29udHJvbC1wcmV2JylbMF07XG4gIHJpZ2h0QXJyb3cgPSBlbGVtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2Nhcm91c2VsLWNvbnRyb2wtbmV4dCcpWzBdO1xuICBpbmRpY2F0b3IgPSBlbGVtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2Nhcm91c2VsLWluZGljYXRvcnMnKVswXTtcbiAgaW5kaWNhdG9ycyA9IGluZGljYXRvciAmJiBpbmRpY2F0b3IuZ2V0RWxlbWVudHNCeVRhZ05hbWUoIFwiTElcIiApIHx8IFtdO1xuICBpZiAoc2xpZGVzLmxlbmd0aCA8IDIpIHsgcmV0dXJuIH1cbiAgdmFyXG4gICAgaW50ZXJ2YWxBdHRyaWJ1dGUgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1pbnRlcnZhbCcpLFxuICAgIGludGVydmFsRGF0YSA9IGludGVydmFsQXR0cmlidXRlID09PSAnZmFsc2UnID8gMCA6IHBhcnNlSW50KGludGVydmFsQXR0cmlidXRlKSxcbiAgICB0b3VjaERhdGEgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS10b3VjaCcpID09PSAnZmFsc2UnID8gMCA6IDEsXG4gICAgcGF1c2VEYXRhID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtcGF1c2UnKSA9PT0gJ2hvdmVyJyB8fCBmYWxzZSxcbiAgICBrZXlib2FyZERhdGEgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1rZXlib2FyZCcpID09PSAndHJ1ZScgfHwgZmFsc2UsXG4gICAgaW50ZXJ2YWxPcHRpb24gPSBvcHRpb25zLmludGVydmFsLFxuICAgIHRvdWNoT3B0aW9uID0gb3B0aW9ucy50b3VjaDtcbiAgb3BzID0ge307XG4gIG9wcy5rZXlib2FyZCA9IG9wdGlvbnMua2V5Ym9hcmQgPT09IHRydWUgfHwga2V5Ym9hcmREYXRhO1xuICBvcHMucGF1c2UgPSAob3B0aW9ucy5wYXVzZSA9PT0gJ2hvdmVyJyB8fCBwYXVzZURhdGEpID8gJ2hvdmVyJyA6IGZhbHNlO1xuICBvcHMudG91Y2ggPSB0b3VjaE9wdGlvbiB8fCB0b3VjaERhdGE7XG4gIG9wcy5pbnRlcnZhbCA9IHR5cGVvZiBpbnRlcnZhbE9wdGlvbiA9PT0gJ251bWJlcicgPyBpbnRlcnZhbE9wdGlvblxuICAgICAgICAgICAgICA6IGludGVydmFsT3B0aW9uID09PSBmYWxzZSB8fCBpbnRlcnZhbERhdGEgPT09IDAgfHwgaW50ZXJ2YWxEYXRhID09PSBmYWxzZSA/IDBcbiAgICAgICAgICAgICAgOiBpc05hTihpbnRlcnZhbERhdGEpID8gNTAwMFxuICAgICAgICAgICAgICA6IGludGVydmFsRGF0YTtcbiAgaWYgKHNlbGYuZ2V0QWN0aXZlSW5kZXgoKTwwKSB7XG4gICAgc2xpZGVzLmxlbmd0aCAmJiBzbGlkZXNbMF0uY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XG4gICAgaW5kaWNhdG9ycy5sZW5ndGggJiYgc2V0QWN0aXZlUGFnZSgwKTtcbiAgfVxuICB2YXJzID0ge307XG4gIHZhcnMuZGlyZWN0aW9uID0gJ2xlZnQnO1xuICB2YXJzLmluZGV4ID0gMDtcbiAgdmFycy50aW1lciA9IG51bGw7XG4gIHZhcnMuaXNTbGlkaW5nID0gZmFsc2U7XG4gIHZhcnMuaXNUb3VjaCA9IGZhbHNlO1xuICB2YXJzLnRvdWNoUG9zaXRpb24gPSB7XG4gICAgc3RhcnRYIDogMCxcbiAgICBjdXJyZW50WCA6IDAsXG4gICAgZW5kWCA6IDBcbiAgfTtcbiAgdG9nZ2xlRXZlbnRzKDEpO1xuICBpZiAoIG9wcy5pbnRlcnZhbCApeyBzZWxmLmN5Y2xlKCk7IH1cbiAgZWxlbWVudC5DYXJvdXNlbCA9IHNlbGY7XG59XG5cbmZ1bmN0aW9uIENvbGxhcHNlKGVsZW1lbnQsb3B0aW9ucykge1xuICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgdmFyIHNlbGYgPSB0aGlzO1xuICB2YXIgYWNjb3JkaW9uID0gbnVsbCxcbiAgICAgIGNvbGxhcHNlID0gbnVsbCxcbiAgICAgIGFjdGl2ZUNvbGxhcHNlLFxuICAgICAgYWN0aXZlRWxlbWVudCxcbiAgICAgIHNob3dDdXN0b21FdmVudCxcbiAgICAgIHNob3duQ3VzdG9tRXZlbnQsXG4gICAgICBoaWRlQ3VzdG9tRXZlbnQsXG4gICAgICBoaWRkZW5DdXN0b21FdmVudDtcbiAgZnVuY3Rpb24gb3BlbkFjdGlvbihjb2xsYXBzZUVsZW1lbnQsIHRvZ2dsZSkge1xuICAgIGRpc3BhdGNoQ3VzdG9tRXZlbnQuY2FsbChjb2xsYXBzZUVsZW1lbnQsIHNob3dDdXN0b21FdmVudCk7XG4gICAgaWYgKCBzaG93Q3VzdG9tRXZlbnQuZGVmYXVsdFByZXZlbnRlZCApIHsgcmV0dXJuOyB9XG4gICAgY29sbGFwc2VFbGVtZW50LmlzQW5pbWF0aW5nID0gdHJ1ZTtcbiAgICBjb2xsYXBzZUVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnY29sbGFwc2luZycpO1xuICAgIGNvbGxhcHNlRWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKCdjb2xsYXBzZScpO1xuICAgIGNvbGxhcHNlRWxlbWVudC5zdHlsZS5oZWlnaHQgPSAoY29sbGFwc2VFbGVtZW50LnNjcm9sbEhlaWdodCkgKyBcInB4XCI7XG4gICAgZW11bGF0ZVRyYW5zaXRpb25FbmQoY29sbGFwc2VFbGVtZW50LCBmdW5jdGlvbiAoKSB7XG4gICAgICBjb2xsYXBzZUVsZW1lbnQuaXNBbmltYXRpbmcgPSBmYWxzZTtcbiAgICAgIGNvbGxhcHNlRWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtZXhwYW5kZWQnLCd0cnVlJyk7XG4gICAgICB0b2dnbGUuc2V0QXR0cmlidXRlKCdhcmlhLWV4cGFuZGVkJywndHJ1ZScpO1xuICAgICAgY29sbGFwc2VFbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoJ2NvbGxhcHNpbmcnKTtcbiAgICAgIGNvbGxhcHNlRWxlbWVudC5jbGFzc0xpc3QuYWRkKCdjb2xsYXBzZScpO1xuICAgICAgY29sbGFwc2VFbGVtZW50LmNsYXNzTGlzdC5hZGQoJ3Nob3cnKTtcbiAgICAgIGNvbGxhcHNlRWxlbWVudC5zdHlsZS5oZWlnaHQgPSAnJztcbiAgICAgIGRpc3BhdGNoQ3VzdG9tRXZlbnQuY2FsbChjb2xsYXBzZUVsZW1lbnQsIHNob3duQ3VzdG9tRXZlbnQpO1xuICAgIH0pO1xuICB9XG4gIGZ1bmN0aW9uIGNsb3NlQWN0aW9uKGNvbGxhcHNlRWxlbWVudCwgdG9nZ2xlKSB7XG4gICAgZGlzcGF0Y2hDdXN0b21FdmVudC5jYWxsKGNvbGxhcHNlRWxlbWVudCwgaGlkZUN1c3RvbUV2ZW50KTtcbiAgICBpZiAoIGhpZGVDdXN0b21FdmVudC5kZWZhdWx0UHJldmVudGVkICkgeyByZXR1cm47IH1cbiAgICBjb2xsYXBzZUVsZW1lbnQuaXNBbmltYXRpbmcgPSB0cnVlO1xuICAgIGNvbGxhcHNlRWxlbWVudC5zdHlsZS5oZWlnaHQgPSAoY29sbGFwc2VFbGVtZW50LnNjcm9sbEhlaWdodCkgKyBcInB4XCI7XG4gICAgY29sbGFwc2VFbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoJ2NvbGxhcHNlJyk7XG4gICAgY29sbGFwc2VFbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoJ3Nob3cnKTtcbiAgICBjb2xsYXBzZUVsZW1lbnQuY2xhc3NMaXN0LmFkZCgnY29sbGFwc2luZycpO1xuICAgIGNvbGxhcHNlRWxlbWVudC5vZmZzZXRXaWR0aDtcbiAgICBjb2xsYXBzZUVsZW1lbnQuc3R5bGUuaGVpZ2h0ID0gJzBweCc7XG4gICAgZW11bGF0ZVRyYW5zaXRpb25FbmQoY29sbGFwc2VFbGVtZW50LCBmdW5jdGlvbiAoKSB7XG4gICAgICBjb2xsYXBzZUVsZW1lbnQuaXNBbmltYXRpbmcgPSBmYWxzZTtcbiAgICAgIGNvbGxhcHNlRWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtZXhwYW5kZWQnLCdmYWxzZScpO1xuICAgICAgdG9nZ2xlLnNldEF0dHJpYnV0ZSgnYXJpYS1leHBhbmRlZCcsJ2ZhbHNlJyk7XG4gICAgICBjb2xsYXBzZUVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSgnY29sbGFwc2luZycpO1xuICAgICAgY29sbGFwc2VFbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2NvbGxhcHNlJyk7XG4gICAgICBjb2xsYXBzZUVsZW1lbnQuc3R5bGUuaGVpZ2h0ID0gJyc7XG4gICAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwoY29sbGFwc2VFbGVtZW50LCBoaWRkZW5DdXN0b21FdmVudCk7XG4gICAgfSk7XG4gIH1cbiAgc2VsZi50b2dnbGUgPSBmdW5jdGlvbiAoZSkge1xuICAgIGlmIChlICYmIGUudGFyZ2V0LnRhZ05hbWUgPT09ICdBJyB8fCBlbGVtZW50LnRhZ05hbWUgPT09ICdBJykge2UucHJldmVudERlZmF1bHQoKTt9XG4gICAgaWYgKGVsZW1lbnQuY29udGFpbnMoZS50YXJnZXQpIHx8IGUudGFyZ2V0ID09PSBlbGVtZW50KSB7XG4gICAgICBpZiAoIWNvbGxhcHNlLmNsYXNzTGlzdC5jb250YWlucygnc2hvdycpKSB7IHNlbGYuc2hvdygpOyB9XG4gICAgICBlbHNlIHsgc2VsZi5oaWRlKCk7IH1cbiAgICB9XG4gIH07XG4gIHNlbGYuaGlkZSA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAoIGNvbGxhcHNlLmlzQW5pbWF0aW5nICkgeyByZXR1cm47IH1cbiAgICBjbG9zZUFjdGlvbihjb2xsYXBzZSxlbGVtZW50KTtcbiAgICBlbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2NvbGxhcHNlZCcpO1xuICB9O1xuICBzZWxmLnNob3cgPSBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKCBhY2NvcmRpb24gKSB7XG4gICAgICBhY3RpdmVDb2xsYXBzZSA9IGFjY29yZGlvbi5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFwiY29sbGFwc2Ugc2hvd1wiKVswXTtcbiAgICAgIGFjdGl2ZUVsZW1lbnQgPSBhY3RpdmVDb2xsYXBzZSAmJiAocXVlcnlFbGVtZW50KChcIltkYXRhLXRhcmdldD1cXFwiI1wiICsgKGFjdGl2ZUNvbGxhcHNlLmlkKSArIFwiXFxcIl1cIiksYWNjb3JkaW9uKVxuICAgICAgICAgICAgICAgICAgICB8fCBxdWVyeUVsZW1lbnQoKFwiW2hyZWY9XFxcIiNcIiArIChhY3RpdmVDb2xsYXBzZS5pZCkgKyBcIlxcXCJdXCIpLGFjY29yZGlvbikgKTtcbiAgICB9XG4gICAgaWYgKCAhY29sbGFwc2UuaXNBbmltYXRpbmcgKSB7XG4gICAgICBpZiAoIGFjdGl2ZUVsZW1lbnQgJiYgYWN0aXZlQ29sbGFwc2UgIT09IGNvbGxhcHNlICkge1xuICAgICAgICBjbG9zZUFjdGlvbihhY3RpdmVDb2xsYXBzZSxhY3RpdmVFbGVtZW50KTtcbiAgICAgICAgYWN0aXZlRWxlbWVudC5jbGFzc0xpc3QuYWRkKCdjb2xsYXBzZWQnKTtcbiAgICAgIH1cbiAgICAgIG9wZW5BY3Rpb24oY29sbGFwc2UsZWxlbWVudCk7XG4gICAgICBlbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoJ2NvbGxhcHNlZCcpO1xuICAgIH1cbiAgfTtcbiAgc2VsZi5kaXNwb3NlID0gZnVuY3Rpb24gKCkge1xuICAgIGVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcignY2xpY2snLHNlbGYudG9nZ2xlLGZhbHNlKTtcbiAgICBkZWxldGUgZWxlbWVudC5Db2xsYXBzZTtcbiAgfTtcbiAgICBlbGVtZW50ID0gcXVlcnlFbGVtZW50KGVsZW1lbnQpO1xuICAgIGVsZW1lbnQuQ29sbGFwc2UgJiYgZWxlbWVudC5Db2xsYXBzZS5kaXNwb3NlKCk7XG4gICAgdmFyIGFjY29yZGlvbkRhdGEgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1wYXJlbnQnKTtcbiAgICBzaG93Q3VzdG9tRXZlbnQgPSBib290c3RyYXBDdXN0b21FdmVudCgnc2hvdycsICdjb2xsYXBzZScpO1xuICAgIHNob3duQ3VzdG9tRXZlbnQgPSBib290c3RyYXBDdXN0b21FdmVudCgnc2hvd24nLCAnY29sbGFwc2UnKTtcbiAgICBoaWRlQ3VzdG9tRXZlbnQgPSBib290c3RyYXBDdXN0b21FdmVudCgnaGlkZScsICdjb2xsYXBzZScpO1xuICAgIGhpZGRlbkN1c3RvbUV2ZW50ID0gYm9vdHN0cmFwQ3VzdG9tRXZlbnQoJ2hpZGRlbicsICdjb2xsYXBzZScpO1xuICAgIGNvbGxhcHNlID0gcXVlcnlFbGVtZW50KG9wdGlvbnMudGFyZ2V0IHx8IGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLXRhcmdldCcpIHx8IGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdocmVmJykpO1xuICAgIGNvbGxhcHNlLmlzQW5pbWF0aW5nID0gZmFsc2U7XG4gICAgYWNjb3JkaW9uID0gZWxlbWVudC5jbG9zZXN0KG9wdGlvbnMucGFyZW50IHx8IGFjY29yZGlvbkRhdGEpO1xuICAgIGlmICggIWVsZW1lbnQuQ29sbGFwc2UgKSB7XG4gICAgICBlbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJyxzZWxmLnRvZ2dsZSxmYWxzZSk7XG4gICAgfVxuICAgIGVsZW1lbnQuQ29sbGFwc2UgPSBzZWxmO1xufVxuXG5mdW5jdGlvbiBzZXRGb2N1cyAoZWxlbWVudCl7XG4gIGVsZW1lbnQuZm9jdXMgPyBlbGVtZW50LmZvY3VzKCkgOiBlbGVtZW50LnNldEFjdGl2ZSgpO1xufVxuXG5mdW5jdGlvbiBEcm9wZG93bihlbGVtZW50LG9wdGlvbikge1xuICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICBzaG93Q3VzdG9tRXZlbnQsXG4gICAgICBzaG93bkN1c3RvbUV2ZW50LFxuICAgICAgaGlkZUN1c3RvbUV2ZW50LFxuICAgICAgaGlkZGVuQ3VzdG9tRXZlbnQsXG4gICAgICByZWxhdGVkVGFyZ2V0ID0gbnVsbCxcbiAgICAgIHBhcmVudCwgbWVudSwgbWVudUl0ZW1zID0gW10sXG4gICAgICBwZXJzaXN0O1xuICBmdW5jdGlvbiBwcmV2ZW50RW1wdHlBbmNob3IoYW5jaG9yKSB7XG4gICAgKGFuY2hvci5ocmVmICYmIGFuY2hvci5ocmVmLnNsaWNlKC0xKSA9PT0gJyMnIHx8IGFuY2hvci5wYXJlbnROb2RlICYmIGFuY2hvci5wYXJlbnROb2RlLmhyZWZcbiAgICAgICYmIGFuY2hvci5wYXJlbnROb2RlLmhyZWYuc2xpY2UoLTEpID09PSAnIycpICYmIHRoaXMucHJldmVudERlZmF1bHQoKTtcbiAgfVxuICBmdW5jdGlvbiB0b2dnbGVEaXNtaXNzKCkge1xuICAgIHZhciBhY3Rpb24gPSBlbGVtZW50Lm9wZW4gPyAnYWRkRXZlbnRMaXN0ZW5lcicgOiAncmVtb3ZlRXZlbnRMaXN0ZW5lcic7XG4gICAgZG9jdW1lbnRbYWN0aW9uXSgnY2xpY2snLGRpc21pc3NIYW5kbGVyLGZhbHNlKTtcbiAgICBkb2N1bWVudFthY3Rpb25dKCdrZXlkb3duJyxwcmV2ZW50U2Nyb2xsLGZhbHNlKTtcbiAgICBkb2N1bWVudFthY3Rpb25dKCdrZXl1cCcsa2V5SGFuZGxlcixmYWxzZSk7XG4gICAgZG9jdW1lbnRbYWN0aW9uXSgnZm9jdXMnLGRpc21pc3NIYW5kbGVyLGZhbHNlKTtcbiAgfVxuICBmdW5jdGlvbiBkaXNtaXNzSGFuZGxlcihlKSB7XG4gICAgdmFyIGV2ZW50VGFyZ2V0ID0gZS50YXJnZXQsXG4gICAgICAgICAgaGFzRGF0YSA9IGV2ZW50VGFyZ2V0ICYmIChldmVudFRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtdG9nZ2xlJylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfHwgZXZlbnRUYXJnZXQucGFyZW50Tm9kZSAmJiBldmVudFRhcmdldC5wYXJlbnROb2RlLmdldEF0dHJpYnV0ZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAmJiBldmVudFRhcmdldC5wYXJlbnROb2RlLmdldEF0dHJpYnV0ZSgnZGF0YS10b2dnbGUnKSk7XG4gICAgaWYgKCBlLnR5cGUgPT09ICdmb2N1cycgJiYgKGV2ZW50VGFyZ2V0ID09PSBlbGVtZW50IHx8IGV2ZW50VGFyZ2V0ID09PSBtZW51IHx8IG1lbnUuY29udGFpbnMoZXZlbnRUYXJnZXQpICkgKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmICggKGV2ZW50VGFyZ2V0ID09PSBtZW51IHx8IG1lbnUuY29udGFpbnMoZXZlbnRUYXJnZXQpKSAmJiAocGVyc2lzdCB8fCBoYXNEYXRhKSApIHsgcmV0dXJuOyB9XG4gICAgZWxzZSB7XG4gICAgICByZWxhdGVkVGFyZ2V0ID0gZXZlbnRUYXJnZXQgPT09IGVsZW1lbnQgfHwgZWxlbWVudC5jb250YWlucyhldmVudFRhcmdldCkgPyBlbGVtZW50IDogbnVsbDtcbiAgICAgIHNlbGYuaGlkZSgpO1xuICAgIH1cbiAgICBwcmV2ZW50RW1wdHlBbmNob3IuY2FsbChlLGV2ZW50VGFyZ2V0KTtcbiAgfVxuICBmdW5jdGlvbiBjbGlja0hhbmRsZXIoZSkge1xuICAgIHJlbGF0ZWRUYXJnZXQgPSBlbGVtZW50O1xuICAgIHNlbGYuc2hvdygpO1xuICAgIHByZXZlbnRFbXB0eUFuY2hvci5jYWxsKGUsZS50YXJnZXQpO1xuICB9XG4gIGZ1bmN0aW9uIHByZXZlbnRTY3JvbGwoZSkge1xuICAgIHZhciBrZXkgPSBlLndoaWNoIHx8IGUua2V5Q29kZTtcbiAgICBpZigga2V5ID09PSAzOCB8fCBrZXkgPT09IDQwICkgeyBlLnByZXZlbnREZWZhdWx0KCk7IH1cbiAgfVxuICBmdW5jdGlvbiBrZXlIYW5kbGVyKGUpIHtcbiAgICB2YXIga2V5ID0gZS53aGljaCB8fCBlLmtleUNvZGUsXG4gICAgICAgIGFjdGl2ZUl0ZW0gPSBkb2N1bWVudC5hY3RpdmVFbGVtZW50LFxuICAgICAgICBpc1NhbWVFbGVtZW50ID0gYWN0aXZlSXRlbSA9PT0gZWxlbWVudCxcbiAgICAgICAgaXNJbnNpZGVNZW51ID0gbWVudS5jb250YWlucyhhY3RpdmVJdGVtKSxcbiAgICAgICAgaXNNZW51SXRlbSA9IGFjdGl2ZUl0ZW0ucGFyZW50Tm9kZSA9PT0gbWVudSB8fCBhY3RpdmVJdGVtLnBhcmVudE5vZGUucGFyZW50Tm9kZSA9PT0gbWVudSxcbiAgICAgICAgaWR4ID0gbWVudUl0ZW1zLmluZGV4T2YoYWN0aXZlSXRlbSk7XG4gICAgaWYgKCBpc01lbnVJdGVtICkge1xuICAgICAgaWR4ID0gaXNTYW1lRWxlbWVudCA/IDBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgOiBrZXkgPT09IDM4ID8gKGlkeD4xP2lkeC0xOjApXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDoga2V5ID09PSA0MCA/IChpZHg8bWVudUl0ZW1zLmxlbmd0aC0xP2lkeCsxOmlkeCkgOiBpZHg7XG4gICAgICBtZW51SXRlbXNbaWR4XSAmJiBzZXRGb2N1cyhtZW51SXRlbXNbaWR4XSk7XG4gICAgfVxuICAgIGlmICggKG1lbnVJdGVtcy5sZW5ndGggJiYgaXNNZW51SXRlbVxuICAgICAgICAgIHx8ICFtZW51SXRlbXMubGVuZ3RoICYmIChpc0luc2lkZU1lbnUgfHwgaXNTYW1lRWxlbWVudClcbiAgICAgICAgICB8fCAhaXNJbnNpZGVNZW51IClcbiAgICAgICAgICAmJiBlbGVtZW50Lm9wZW4gJiYga2V5ID09PSAyN1xuICAgICkge1xuICAgICAgc2VsZi50b2dnbGUoKTtcbiAgICAgIHJlbGF0ZWRUYXJnZXQgPSBudWxsO1xuICAgIH1cbiAgfVxuICBzZWxmLnNob3cgPSBmdW5jdGlvbiAoKSB7XG4gICAgc2hvd0N1c3RvbUV2ZW50ID0gYm9vdHN0cmFwQ3VzdG9tRXZlbnQoJ3Nob3cnLCAnZHJvcGRvd24nLCByZWxhdGVkVGFyZ2V0KTtcbiAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwocGFyZW50LCBzaG93Q3VzdG9tRXZlbnQpO1xuICAgIGlmICggc2hvd0N1c3RvbUV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQgKSB7IHJldHVybjsgfVxuICAgIG1lbnUuY2xhc3NMaXN0LmFkZCgnc2hvdycpO1xuICAgIHBhcmVudC5jbGFzc0xpc3QuYWRkKCdzaG93Jyk7XG4gICAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtZXhwYW5kZWQnLHRydWUpO1xuICAgIGVsZW1lbnQub3BlbiA9IHRydWU7XG4gICAgZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdjbGljaycsY2xpY2tIYW5kbGVyLGZhbHNlKTtcbiAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgIHNldEZvY3VzKCBtZW51LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdJTlBVVCcpWzBdIHx8IGVsZW1lbnQgKTtcbiAgICAgIHRvZ2dsZURpc21pc3MoKTtcbiAgICAgIHNob3duQ3VzdG9tRXZlbnQgPSBib290c3RyYXBDdXN0b21FdmVudCggJ3Nob3duJywgJ2Ryb3Bkb3duJywgcmVsYXRlZFRhcmdldCk7XG4gICAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwocGFyZW50LCBzaG93bkN1c3RvbUV2ZW50KTtcbiAgICB9LDEpO1xuICB9O1xuICBzZWxmLmhpZGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgaGlkZUN1c3RvbUV2ZW50ID0gYm9vdHN0cmFwQ3VzdG9tRXZlbnQoJ2hpZGUnLCAnZHJvcGRvd24nLCByZWxhdGVkVGFyZ2V0KTtcbiAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwocGFyZW50LCBoaWRlQ3VzdG9tRXZlbnQpO1xuICAgIGlmICggaGlkZUN1c3RvbUV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQgKSB7IHJldHVybjsgfVxuICAgIG1lbnUuY2xhc3NMaXN0LnJlbW92ZSgnc2hvdycpO1xuICAgIHBhcmVudC5jbGFzc0xpc3QucmVtb3ZlKCdzaG93Jyk7XG4gICAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtZXhwYW5kZWQnLGZhbHNlKTtcbiAgICBlbGVtZW50Lm9wZW4gPSBmYWxzZTtcbiAgICB0b2dnbGVEaXNtaXNzKCk7XG4gICAgc2V0Rm9jdXMoZWxlbWVudCk7XG4gICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICBlbGVtZW50LkRyb3Bkb3duICYmIGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLGNsaWNrSGFuZGxlcixmYWxzZSk7XG4gICAgfSwxKTtcbiAgICBoaWRkZW5DdXN0b21FdmVudCA9IGJvb3RzdHJhcEN1c3RvbUV2ZW50KCdoaWRkZW4nLCAnZHJvcGRvd24nLCByZWxhdGVkVGFyZ2V0KTtcbiAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwocGFyZW50LCBoaWRkZW5DdXN0b21FdmVudCk7XG4gIH07XG4gIHNlbGYudG9nZ2xlID0gZnVuY3Rpb24gKCkge1xuICAgIGlmIChwYXJlbnQuY2xhc3NMaXN0LmNvbnRhaW5zKCdzaG93JykgJiYgZWxlbWVudC5vcGVuKSB7IHNlbGYuaGlkZSgpOyB9XG4gICAgZWxzZSB7IHNlbGYuc2hvdygpOyB9XG4gIH07XG4gIHNlbGYuZGlzcG9zZSA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAocGFyZW50LmNsYXNzTGlzdC5jb250YWlucygnc2hvdycpICYmIGVsZW1lbnQub3BlbikgeyBzZWxmLmhpZGUoKTsgfVxuICAgIGVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcignY2xpY2snLGNsaWNrSGFuZGxlcixmYWxzZSk7XG4gICAgZGVsZXRlIGVsZW1lbnQuRHJvcGRvd247XG4gIH07XG4gIGVsZW1lbnQgPSBxdWVyeUVsZW1lbnQoZWxlbWVudCk7XG4gIGVsZW1lbnQuRHJvcGRvd24gJiYgZWxlbWVudC5Ecm9wZG93bi5kaXNwb3NlKCk7XG4gIHBhcmVudCA9IGVsZW1lbnQucGFyZW50Tm9kZTtcbiAgbWVudSA9IHF1ZXJ5RWxlbWVudCgnLmRyb3Bkb3duLW1lbnUnLCBwYXJlbnQpO1xuICBBcnJheS5mcm9tKG1lbnUuY2hpbGRyZW4pLm1hcChmdW5jdGlvbiAoY2hpbGQpe1xuICAgIGNoaWxkLmNoaWxkcmVuLmxlbmd0aCAmJiAoY2hpbGQuY2hpbGRyZW5bMF0udGFnTmFtZSA9PT0gJ0EnICYmIG1lbnVJdGVtcy5wdXNoKGNoaWxkLmNoaWxkcmVuWzBdKSk7XG4gICAgY2hpbGQudGFnTmFtZSA9PT0gJ0EnICYmIG1lbnVJdGVtcy5wdXNoKGNoaWxkKTtcbiAgfSk7XG4gIGlmICggIWVsZW1lbnQuRHJvcGRvd24gKSB7XG4gICAgISgndGFiaW5kZXgnIGluIG1lbnUpICYmIG1lbnUuc2V0QXR0cmlidXRlKCd0YWJpbmRleCcsICcwJyk7XG4gICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsY2xpY2tIYW5kbGVyLGZhbHNlKTtcbiAgfVxuICBwZXJzaXN0ID0gb3B0aW9uID09PSB0cnVlIHx8IGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLXBlcnNpc3QnKSA9PT0gJ3RydWUnIHx8IGZhbHNlO1xuICBlbGVtZW50Lm9wZW4gPSBmYWxzZTtcbiAgZWxlbWVudC5Ecm9wZG93biA9IHNlbGY7XG59XG5cbmZ1bmN0aW9uIE1vZGFsKGVsZW1lbnQsb3B0aW9ucykge1xuICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgdmFyIHNlbGYgPSB0aGlzLCBtb2RhbCxcbiAgICBzaG93Q3VzdG9tRXZlbnQsXG4gICAgc2hvd25DdXN0b21FdmVudCxcbiAgICBoaWRlQ3VzdG9tRXZlbnQsXG4gICAgaGlkZGVuQ3VzdG9tRXZlbnQsXG4gICAgcmVsYXRlZFRhcmdldCA9IG51bGwsXG4gICAgc2Nyb2xsQmFyV2lkdGgsXG4gICAgb3ZlcmxheSxcbiAgICBvdmVybGF5RGVsYXksXG4gICAgZml4ZWRJdGVtcyxcbiAgICBvcHMgPSB7fTtcbiAgZnVuY3Rpb24gc2V0U2Nyb2xsYmFyKCkge1xuICAgIHZhciBvcGVuTW9kYWwgPSBkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5jb250YWlucygnbW9kYWwtb3BlbicpLFxuICAgICAgICBib2R5UGFkID0gcGFyc2VJbnQoZ2V0Q29tcHV0ZWRTdHlsZShkb2N1bWVudC5ib2R5KS5wYWRkaW5nUmlnaHQpLFxuICAgICAgICBib2R5T3ZlcmZsb3cgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0ICE9PSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsSGVpZ2h0XG4gICAgICAgICAgICAgICAgICAgIHx8IGRvY3VtZW50LmJvZHkuY2xpZW50SGVpZ2h0ICE9PSBkb2N1bWVudC5ib2R5LnNjcm9sbEhlaWdodCxcbiAgICAgICAgbW9kYWxPdmVyZmxvdyA9IG1vZGFsLmNsaWVudEhlaWdodCAhPT0gbW9kYWwuc2Nyb2xsSGVpZ2h0O1xuICAgIHNjcm9sbEJhcldpZHRoID0gbWVhc3VyZVNjcm9sbGJhcigpO1xuICAgIG1vZGFsLnN0eWxlLnBhZGRpbmdSaWdodCA9ICFtb2RhbE92ZXJmbG93ICYmIHNjcm9sbEJhcldpZHRoID8gKHNjcm9sbEJhcldpZHRoICsgXCJweFwiKSA6ICcnO1xuICAgIGRvY3VtZW50LmJvZHkuc3R5bGUucGFkZGluZ1JpZ2h0ID0gbW9kYWxPdmVyZmxvdyB8fCBib2R5T3ZlcmZsb3cgPyAoKGJvZHlQYWQgKyAob3Blbk1vZGFsID8gMDpzY3JvbGxCYXJXaWR0aCkpICsgXCJweFwiKSA6ICcnO1xuICAgIGZpeGVkSXRlbXMubGVuZ3RoICYmIGZpeGVkSXRlbXMubWFwKGZ1bmN0aW9uIChmaXhlZCl7XG4gICAgICB2YXIgaXRlbVBhZCA9IGdldENvbXB1dGVkU3R5bGUoZml4ZWQpLnBhZGRpbmdSaWdodDtcbiAgICAgIGZpeGVkLnN0eWxlLnBhZGRpbmdSaWdodCA9IG1vZGFsT3ZlcmZsb3cgfHwgYm9keU92ZXJmbG93ID8gKChwYXJzZUludChpdGVtUGFkKSArIChvcGVuTW9kYWw/MDpzY3JvbGxCYXJXaWR0aCkpICsgXCJweFwiKSA6ICgocGFyc2VJbnQoaXRlbVBhZCkpICsgXCJweFwiKTtcbiAgICB9KTtcbiAgfVxuICBmdW5jdGlvbiByZXNldFNjcm9sbGJhcigpIHtcbiAgICBkb2N1bWVudC5ib2R5LnN0eWxlLnBhZGRpbmdSaWdodCA9ICcnO1xuICAgIG1vZGFsLnN0eWxlLnBhZGRpbmdSaWdodCA9ICcnO1xuICAgIGZpeGVkSXRlbXMubGVuZ3RoICYmIGZpeGVkSXRlbXMubWFwKGZ1bmN0aW9uIChmaXhlZCl7XG4gICAgICBmaXhlZC5zdHlsZS5wYWRkaW5nUmlnaHQgPSAnJztcbiAgICB9KTtcbiAgfVxuICBmdW5jdGlvbiBtZWFzdXJlU2Nyb2xsYmFyKCkge1xuICAgIHZhciBzY3JvbGxEaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKSwgd2lkdGhWYWx1ZTtcbiAgICBzY3JvbGxEaXYuY2xhc3NOYW1lID0gJ21vZGFsLXNjcm9sbGJhci1tZWFzdXJlJztcbiAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHNjcm9sbERpdik7XG4gICAgd2lkdGhWYWx1ZSA9IHNjcm9sbERpdi5vZmZzZXRXaWR0aCAtIHNjcm9sbERpdi5jbGllbnRXaWR0aDtcbiAgICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKHNjcm9sbERpdik7XG4gICAgcmV0dXJuIHdpZHRoVmFsdWU7XG4gIH1cbiAgZnVuY3Rpb24gY3JlYXRlT3ZlcmxheSgpIHtcbiAgICB2YXIgbmV3T3ZlcmxheSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgIG92ZXJsYXkgPSBxdWVyeUVsZW1lbnQoJy5tb2RhbC1iYWNrZHJvcCcpO1xuICAgIGlmICggb3ZlcmxheSA9PT0gbnVsbCApIHtcbiAgICAgIG5ld092ZXJsYXkuc2V0QXR0cmlidXRlKCdjbGFzcycsICdtb2RhbC1iYWNrZHJvcCcgKyAob3BzLmFuaW1hdGlvbiA/ICcgZmFkZScgOiAnJykpO1xuICAgICAgb3ZlcmxheSA9IG5ld092ZXJsYXk7XG4gICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKG92ZXJsYXkpO1xuICAgIH1cbiAgICByZXR1cm4gb3ZlcmxheTtcbiAgfVxuICBmdW5jdGlvbiByZW1vdmVPdmVybGF5ICgpIHtcbiAgICBvdmVybGF5ID0gcXVlcnlFbGVtZW50KCcubW9kYWwtYmFja2Ryb3AnKTtcbiAgICBpZiAoIG92ZXJsYXkgJiYgIWRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ21vZGFsIHNob3cnKVswXSApIHtcbiAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQob3ZlcmxheSk7IG92ZXJsYXkgPSBudWxsO1xuICAgIH1cbiAgICBvdmVybGF5ID09PSBudWxsICYmIChkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5yZW1vdmUoJ21vZGFsLW9wZW4nKSwgcmVzZXRTY3JvbGxiYXIoKSk7XG4gIH1cbiAgZnVuY3Rpb24gdG9nZ2xlRXZlbnRzKGFjdGlvbikge1xuICAgIGFjdGlvbiA9IGFjdGlvbiA/ICdhZGRFdmVudExpc3RlbmVyJyA6ICdyZW1vdmVFdmVudExpc3RlbmVyJztcbiAgICB3aW5kb3dbYWN0aW9uXSggJ3Jlc2l6ZScsIHNlbGYudXBkYXRlLCBwYXNzaXZlSGFuZGxlcik7XG4gICAgbW9kYWxbYWN0aW9uXSggJ2NsaWNrJyxkaXNtaXNzSGFuZGxlcixmYWxzZSk7XG4gICAgZG9jdW1lbnRbYWN0aW9uXSggJ2tleWRvd24nLGtleUhhbmRsZXIsZmFsc2UpO1xuICB9XG4gIGZ1bmN0aW9uIGJlZm9yZVNob3coKSB7XG4gICAgbW9kYWwuc3R5bGUuZGlzcGxheSA9ICdibG9jayc7XG4gICAgc2V0U2Nyb2xsYmFyKCk7XG4gICAgIWRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ21vZGFsIHNob3cnKVswXSAmJiBkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5hZGQoJ21vZGFsLW9wZW4nKTtcbiAgICBtb2RhbC5jbGFzc0xpc3QuYWRkKCdzaG93Jyk7XG4gICAgbW9kYWwuc2V0QXR0cmlidXRlKCdhcmlhLWhpZGRlbicsIGZhbHNlKTtcbiAgICBtb2RhbC5jbGFzc0xpc3QuY29udGFpbnMoJ2ZhZGUnKSA/IGVtdWxhdGVUcmFuc2l0aW9uRW5kKG1vZGFsLCB0cmlnZ2VyU2hvdykgOiB0cmlnZ2VyU2hvdygpO1xuICB9XG4gIGZ1bmN0aW9uIHRyaWdnZXJTaG93KCkge1xuICAgIHNldEZvY3VzKG1vZGFsKTtcbiAgICBtb2RhbC5pc0FuaW1hdGluZyA9IGZhbHNlO1xuICAgIHRvZ2dsZUV2ZW50cygxKTtcbiAgICBzaG93bkN1c3RvbUV2ZW50ID0gYm9vdHN0cmFwQ3VzdG9tRXZlbnQoJ3Nob3duJywgJ21vZGFsJywgcmVsYXRlZFRhcmdldCk7XG4gICAgZGlzcGF0Y2hDdXN0b21FdmVudC5jYWxsKG1vZGFsLCBzaG93bkN1c3RvbUV2ZW50KTtcbiAgfVxuICBmdW5jdGlvbiB0cmlnZ2VySGlkZShmb3JjZSkge1xuICAgIG1vZGFsLnN0eWxlLmRpc3BsYXkgPSAnJztcbiAgICBlbGVtZW50ICYmIChzZXRGb2N1cyhlbGVtZW50KSk7XG4gICAgb3ZlcmxheSA9IHF1ZXJ5RWxlbWVudCgnLm1vZGFsLWJhY2tkcm9wJyk7XG4gICAgaWYgKGZvcmNlICE9PSAxICYmIG92ZXJsYXkgJiYgb3ZlcmxheS5jbGFzc0xpc3QuY29udGFpbnMoJ3Nob3cnKSAmJiAhZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnbW9kYWwgc2hvdycpWzBdKSB7XG4gICAgICBvdmVybGF5LmNsYXNzTGlzdC5yZW1vdmUoJ3Nob3cnKTtcbiAgICAgIGVtdWxhdGVUcmFuc2l0aW9uRW5kKG92ZXJsYXkscmVtb3ZlT3ZlcmxheSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJlbW92ZU92ZXJsYXkoKTtcbiAgICB9XG4gICAgdG9nZ2xlRXZlbnRzKCk7XG4gICAgbW9kYWwuaXNBbmltYXRpbmcgPSBmYWxzZTtcbiAgICBoaWRkZW5DdXN0b21FdmVudCA9IGJvb3RzdHJhcEN1c3RvbUV2ZW50KCdoaWRkZW4nLCAnbW9kYWwnKTtcbiAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwobW9kYWwsIGhpZGRlbkN1c3RvbUV2ZW50KTtcbiAgfVxuICBmdW5jdGlvbiBjbGlja0hhbmRsZXIoZSkge1xuICAgIGlmICggbW9kYWwuaXNBbmltYXRpbmcgKSB7IHJldHVybjsgfVxuICAgIHZhciBjbGlja1RhcmdldCA9IGUudGFyZ2V0LFxuICAgICAgICBtb2RhbElEID0gXCIjXCIgKyAobW9kYWwuZ2V0QXR0cmlidXRlKCdpZCcpKSxcbiAgICAgICAgdGFyZ2V0QXR0clZhbHVlID0gY2xpY2tUYXJnZXQuZ2V0QXR0cmlidXRlKCdkYXRhLXRhcmdldCcpIHx8IGNsaWNrVGFyZ2V0LmdldEF0dHJpYnV0ZSgnaHJlZicpLFxuICAgICAgICBlbGVtQXR0clZhbHVlID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtdGFyZ2V0JykgfHwgZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2hyZWYnKTtcbiAgICBpZiAoICFtb2RhbC5jbGFzc0xpc3QuY29udGFpbnMoJ3Nob3cnKVxuICAgICAgICAmJiAoY2xpY2tUYXJnZXQgPT09IGVsZW1lbnQgJiYgdGFyZ2V0QXR0clZhbHVlID09PSBtb2RhbElEXG4gICAgICAgIHx8IGVsZW1lbnQuY29udGFpbnMoY2xpY2tUYXJnZXQpICYmIGVsZW1BdHRyVmFsdWUgPT09IG1vZGFsSUQpICkge1xuICAgICAgbW9kYWwubW9kYWxUcmlnZ2VyID0gZWxlbWVudDtcbiAgICAgIHJlbGF0ZWRUYXJnZXQgPSBlbGVtZW50O1xuICAgICAgc2VsZi5zaG93KCk7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgfVxuICB9XG4gIGZ1bmN0aW9uIGtleUhhbmRsZXIocmVmKSB7XG4gICAgdmFyIHdoaWNoID0gcmVmLndoaWNoO1xuICAgIGlmICghbW9kYWwuaXNBbmltYXRpbmcgJiYgb3BzLmtleWJvYXJkICYmIHdoaWNoID09IDI3ICYmIG1vZGFsLmNsYXNzTGlzdC5jb250YWlucygnc2hvdycpICkge1xuICAgICAgc2VsZi5oaWRlKCk7XG4gICAgfVxuICB9XG4gIGZ1bmN0aW9uIGRpc21pc3NIYW5kbGVyKGUpIHtcbiAgICBpZiAoIG1vZGFsLmlzQW5pbWF0aW5nICkgeyByZXR1cm47IH1cbiAgICB2YXIgY2xpY2tUYXJnZXQgPSBlLnRhcmdldCxcbiAgICAgICAgaGFzRGF0YSA9IGNsaWNrVGFyZ2V0LmdldEF0dHJpYnV0ZSgnZGF0YS1kaXNtaXNzJykgPT09ICdtb2RhbCcsXG4gICAgICAgIHBhcmVudFdpdGhEYXRhID0gY2xpY2tUYXJnZXQuY2xvc2VzdCgnW2RhdGEtZGlzbWlzcz1cIm1vZGFsXCJdJyk7XG4gICAgaWYgKCBtb2RhbC5jbGFzc0xpc3QuY29udGFpbnMoJ3Nob3cnKSAmJiAoIHBhcmVudFdpdGhEYXRhIHx8IGhhc0RhdGFcbiAgICAgICAgfHwgY2xpY2tUYXJnZXQgPT09IG1vZGFsICYmIG9wcy5iYWNrZHJvcCAhPT0gJ3N0YXRpYycgKSApIHtcbiAgICAgIHNlbGYuaGlkZSgpOyByZWxhdGVkVGFyZ2V0ID0gbnVsbDtcbiAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICB9XG4gIH1cbiAgc2VsZi50b2dnbGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKCBtb2RhbC5jbGFzc0xpc3QuY29udGFpbnMoJ3Nob3cnKSApIHtzZWxmLmhpZGUoKTt9IGVsc2Uge3NlbGYuc2hvdygpO31cbiAgfTtcbiAgc2VsZi5zaG93ID0gZnVuY3Rpb24gKCkge1xuICAgIGlmIChtb2RhbC5jbGFzc0xpc3QuY29udGFpbnMoJ3Nob3cnKSAmJiAhIW1vZGFsLmlzQW5pbWF0aW5nICkge3JldHVybn1cbiAgICBzaG93Q3VzdG9tRXZlbnQgPSBib290c3RyYXBDdXN0b21FdmVudCgnc2hvdycsICdtb2RhbCcsIHJlbGF0ZWRUYXJnZXQpO1xuICAgIGRpc3BhdGNoQ3VzdG9tRXZlbnQuY2FsbChtb2RhbCwgc2hvd0N1c3RvbUV2ZW50KTtcbiAgICBpZiAoIHNob3dDdXN0b21FdmVudC5kZWZhdWx0UHJldmVudGVkICkgeyByZXR1cm47IH1cbiAgICBtb2RhbC5pc0FuaW1hdGluZyA9IHRydWU7XG4gICAgdmFyIGN1cnJlbnRPcGVuID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnbW9kYWwgc2hvdycpWzBdO1xuICAgIGlmIChjdXJyZW50T3BlbiAmJiBjdXJyZW50T3BlbiAhPT0gbW9kYWwpIHtcbiAgICAgIGN1cnJlbnRPcGVuLm1vZGFsVHJpZ2dlciAmJiBjdXJyZW50T3Blbi5tb2RhbFRyaWdnZXIuTW9kYWwuaGlkZSgpO1xuICAgICAgY3VycmVudE9wZW4uTW9kYWwgJiYgY3VycmVudE9wZW4uTW9kYWwuaGlkZSgpO1xuICAgIH1cbiAgICBpZiAoIG9wcy5iYWNrZHJvcCApIHtcbiAgICAgIG92ZXJsYXkgPSBjcmVhdGVPdmVybGF5KCk7XG4gICAgfVxuICAgIGlmICggb3ZlcmxheSAmJiAhY3VycmVudE9wZW4gJiYgIW92ZXJsYXkuY2xhc3NMaXN0LmNvbnRhaW5zKCdzaG93JykgKSB7XG4gICAgICBvdmVybGF5Lm9mZnNldFdpZHRoO1xuICAgICAgb3ZlcmxheURlbGF5ID0gZ2V0RWxlbWVudFRyYW5zaXRpb25EdXJhdGlvbihvdmVybGF5KTtcbiAgICAgIG92ZXJsYXkuY2xhc3NMaXN0LmFkZCgnc2hvdycpO1xuICAgIH1cbiAgICAhY3VycmVudE9wZW4gPyBzZXRUaW1lb3V0KCBiZWZvcmVTaG93LCBvdmVybGF5ICYmIG92ZXJsYXlEZWxheSA/IG92ZXJsYXlEZWxheTowICkgOiBiZWZvcmVTaG93KCk7XG4gIH07XG4gIHNlbGYuaGlkZSA9IGZ1bmN0aW9uIChmb3JjZSkge1xuICAgIGlmICggIW1vZGFsLmNsYXNzTGlzdC5jb250YWlucygnc2hvdycpICkge3JldHVybn1cbiAgICBoaWRlQ3VzdG9tRXZlbnQgPSBib290c3RyYXBDdXN0b21FdmVudCggJ2hpZGUnLCAnbW9kYWwnKTtcbiAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwobW9kYWwsIGhpZGVDdXN0b21FdmVudCk7XG4gICAgaWYgKCBoaWRlQ3VzdG9tRXZlbnQuZGVmYXVsdFByZXZlbnRlZCApIHsgcmV0dXJuOyB9XG4gICAgbW9kYWwuaXNBbmltYXRpbmcgPSB0cnVlO1xuICAgIG1vZGFsLmNsYXNzTGlzdC5yZW1vdmUoJ3Nob3cnKTtcbiAgICBtb2RhbC5zZXRBdHRyaWJ1dGUoJ2FyaWEtaGlkZGVuJywgdHJ1ZSk7XG4gICAgbW9kYWwuY2xhc3NMaXN0LmNvbnRhaW5zKCdmYWRlJykgJiYgZm9yY2UgIT09IDEgPyBlbXVsYXRlVHJhbnNpdGlvbkVuZChtb2RhbCwgdHJpZ2dlckhpZGUpIDogdHJpZ2dlckhpZGUoKTtcbiAgfTtcbiAgc2VsZi5zZXRDb250ZW50ID0gZnVuY3Rpb24gKGNvbnRlbnQpIHtcbiAgICBxdWVyeUVsZW1lbnQoJy5tb2RhbC1jb250ZW50Jyxtb2RhbCkuaW5uZXJIVE1MID0gY29udGVudDtcbiAgfTtcbiAgc2VsZi51cGRhdGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKG1vZGFsLmNsYXNzTGlzdC5jb250YWlucygnc2hvdycpKSB7XG4gICAgICBzZXRTY3JvbGxiYXIoKTtcbiAgICB9XG4gIH07XG4gIHNlbGYuZGlzcG9zZSA9IGZ1bmN0aW9uICgpIHtcbiAgICBzZWxmLmhpZGUoMSk7XG4gICAgaWYgKGVsZW1lbnQpIHtlbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2NsaWNrJyxjbGlja0hhbmRsZXIsZmFsc2UpOyBkZWxldGUgZWxlbWVudC5Nb2RhbDsgfVxuICAgIGVsc2Uge2RlbGV0ZSBtb2RhbC5Nb2RhbDt9XG4gIH07XG4gIGVsZW1lbnQgPSBxdWVyeUVsZW1lbnQoZWxlbWVudCk7XG4gIHZhciBjaGVja01vZGFsID0gcXVlcnlFbGVtZW50KCBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS10YXJnZXQnKSB8fCBlbGVtZW50LmdldEF0dHJpYnV0ZSgnaHJlZicpICk7XG4gIG1vZGFsID0gZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoJ21vZGFsJykgPyBlbGVtZW50IDogY2hlY2tNb2RhbDtcbiAgZml4ZWRJdGVtcyA9IEFycmF5LmZyb20oZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnZml4ZWQtdG9wJykpXG4gICAgICAgICAgICAgICAgICAgIC5jb25jYXQoQXJyYXkuZnJvbShkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdmaXhlZC1ib3R0b20nKSkpO1xuICBpZiAoIGVsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKCdtb2RhbCcpICkgeyBlbGVtZW50ID0gbnVsbDsgfVxuICBlbGVtZW50ICYmIGVsZW1lbnQuTW9kYWwgJiYgZWxlbWVudC5Nb2RhbC5kaXNwb3NlKCk7XG4gIG1vZGFsICYmIG1vZGFsLk1vZGFsICYmIG1vZGFsLk1vZGFsLmRpc3Bvc2UoKTtcbiAgb3BzLmtleWJvYXJkID0gb3B0aW9ucy5rZXlib2FyZCA9PT0gZmFsc2UgfHwgbW9kYWwuZ2V0QXR0cmlidXRlKCdkYXRhLWtleWJvYXJkJykgPT09ICdmYWxzZScgPyBmYWxzZSA6IHRydWU7XG4gIG9wcy5iYWNrZHJvcCA9IG9wdGlvbnMuYmFja2Ryb3AgPT09ICdzdGF0aWMnIHx8IG1vZGFsLmdldEF0dHJpYnV0ZSgnZGF0YS1iYWNrZHJvcCcpID09PSAnc3RhdGljJyA/ICdzdGF0aWMnIDogdHJ1ZTtcbiAgb3BzLmJhY2tkcm9wID0gb3B0aW9ucy5iYWNrZHJvcCA9PT0gZmFsc2UgfHwgbW9kYWwuZ2V0QXR0cmlidXRlKCdkYXRhLWJhY2tkcm9wJykgPT09ICdmYWxzZScgPyBmYWxzZSA6IG9wcy5iYWNrZHJvcDtcbiAgb3BzLmFuaW1hdGlvbiA9IG1vZGFsLmNsYXNzTGlzdC5jb250YWlucygnZmFkZScpID8gdHJ1ZSA6IGZhbHNlO1xuICBvcHMuY29udGVudCA9IG9wdGlvbnMuY29udGVudDtcbiAgbW9kYWwuaXNBbmltYXRpbmcgPSBmYWxzZTtcbiAgaWYgKCBlbGVtZW50ICYmICFlbGVtZW50Lk1vZGFsICkge1xuICAgIGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLGNsaWNrSGFuZGxlcixmYWxzZSk7XG4gIH1cbiAgaWYgKCBvcHMuY29udGVudCApIHtcbiAgICBzZWxmLnNldENvbnRlbnQoIG9wcy5jb250ZW50LnRyaW0oKSApO1xuICB9XG4gIGlmIChlbGVtZW50KSB7XG4gICAgbW9kYWwubW9kYWxUcmlnZ2VyID0gZWxlbWVudDtcbiAgICBlbGVtZW50Lk1vZGFsID0gc2VsZjtcbiAgfSBlbHNlIHtcbiAgICBtb2RhbC5Nb2RhbCA9IHNlbGY7XG4gIH1cbn1cblxudmFyIG1vdXNlQ2xpY2tFdmVudHMgPSB7IGRvd246ICdtb3VzZWRvd24nLCB1cDogJ21vdXNldXAnIH07XG5cbmZ1bmN0aW9uIGdldFNjcm9sbCgpIHtcbiAgcmV0dXJuIHtcbiAgICB5IDogd2luZG93LnBhZ2VZT2Zmc2V0IHx8IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxUb3AsXG4gICAgeCA6IHdpbmRvdy5wYWdlWE9mZnNldCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsTGVmdFxuICB9XG59XG5cbmZ1bmN0aW9uIHN0eWxlVGlwKGxpbmssZWxlbWVudCxwb3NpdGlvbixwYXJlbnQpIHtcbiAgdmFyIHRpcFBvc2l0aW9ucyA9IC9cXGIodG9wfGJvdHRvbXxsZWZ0fHJpZ2h0KSsvLFxuICAgICAgZWxlbWVudERpbWVuc2lvbnMgPSB7IHcgOiBlbGVtZW50Lm9mZnNldFdpZHRoLCBoOiBlbGVtZW50Lm9mZnNldEhlaWdodCB9LFxuICAgICAgd2luZG93V2lkdGggPSAoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsaWVudFdpZHRoIHx8IGRvY3VtZW50LmJvZHkuY2xpZW50V2lkdGgpLFxuICAgICAgd2luZG93SGVpZ2h0ID0gKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRIZWlnaHQgfHwgZG9jdW1lbnQuYm9keS5jbGllbnRIZWlnaHQpLFxuICAgICAgcmVjdCA9IGxpbmsuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksXG4gICAgICBzY3JvbGwgPSBwYXJlbnQgPT09IGRvY3VtZW50LmJvZHkgPyBnZXRTY3JvbGwoKSA6IHsgeDogcGFyZW50Lm9mZnNldExlZnQgKyBwYXJlbnQuc2Nyb2xsTGVmdCwgeTogcGFyZW50Lm9mZnNldFRvcCArIHBhcmVudC5zY3JvbGxUb3AgfSxcbiAgICAgIGxpbmtEaW1lbnNpb25zID0geyB3OiByZWN0LnJpZ2h0IC0gcmVjdC5sZWZ0LCBoOiByZWN0LmJvdHRvbSAtIHJlY3QudG9wIH0sXG4gICAgICBpc1BvcG92ZXIgPSBlbGVtZW50LmNsYXNzTGlzdC5jb250YWlucygncG9wb3ZlcicpLFxuICAgICAgYXJyb3cgPSBlbGVtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2Fycm93JylbMF0sXG4gICAgICBoYWxmVG9wRXhjZWVkID0gcmVjdC50b3AgKyBsaW5rRGltZW5zaW9ucy5oLzIgLSBlbGVtZW50RGltZW5zaW9ucy5oLzIgPCAwLFxuICAgICAgaGFsZkxlZnRFeGNlZWQgPSByZWN0LmxlZnQgKyBsaW5rRGltZW5zaW9ucy53LzIgLSBlbGVtZW50RGltZW5zaW9ucy53LzIgPCAwLFxuICAgICAgaGFsZlJpZ2h0RXhjZWVkID0gcmVjdC5sZWZ0ICsgZWxlbWVudERpbWVuc2lvbnMudy8yICsgbGlua0RpbWVuc2lvbnMudy8yID49IHdpbmRvd1dpZHRoLFxuICAgICAgaGFsZkJvdHRvbUV4Y2VlZCA9IHJlY3QudG9wICsgZWxlbWVudERpbWVuc2lvbnMuaC8yICsgbGlua0RpbWVuc2lvbnMuaC8yID49IHdpbmRvd0hlaWdodCxcbiAgICAgIHRvcEV4Y2VlZCA9IHJlY3QudG9wIC0gZWxlbWVudERpbWVuc2lvbnMuaCA8IDAsXG4gICAgICBsZWZ0RXhjZWVkID0gcmVjdC5sZWZ0IC0gZWxlbWVudERpbWVuc2lvbnMudyA8IDAsXG4gICAgICBib3R0b21FeGNlZWQgPSByZWN0LnRvcCArIGVsZW1lbnREaW1lbnNpb25zLmggKyBsaW5rRGltZW5zaW9ucy5oID49IHdpbmRvd0hlaWdodCxcbiAgICAgIHJpZ2h0RXhjZWVkID0gcmVjdC5sZWZ0ICsgZWxlbWVudERpbWVuc2lvbnMudyArIGxpbmtEaW1lbnNpb25zLncgPj0gd2luZG93V2lkdGg7XG4gIHBvc2l0aW9uID0gKHBvc2l0aW9uID09PSAnbGVmdCcgfHwgcG9zaXRpb24gPT09ICdyaWdodCcpICYmIGxlZnRFeGNlZWQgJiYgcmlnaHRFeGNlZWQgPyAndG9wJyA6IHBvc2l0aW9uO1xuICBwb3NpdGlvbiA9IHBvc2l0aW9uID09PSAndG9wJyAmJiB0b3BFeGNlZWQgPyAnYm90dG9tJyA6IHBvc2l0aW9uO1xuICBwb3NpdGlvbiA9IHBvc2l0aW9uID09PSAnYm90dG9tJyAmJiBib3R0b21FeGNlZWQgPyAndG9wJyA6IHBvc2l0aW9uO1xuICBwb3NpdGlvbiA9IHBvc2l0aW9uID09PSAnbGVmdCcgJiYgbGVmdEV4Y2VlZCA/ICdyaWdodCcgOiBwb3NpdGlvbjtcbiAgcG9zaXRpb24gPSBwb3NpdGlvbiA9PT0gJ3JpZ2h0JyAmJiByaWdodEV4Y2VlZCA/ICdsZWZ0JyA6IHBvc2l0aW9uO1xuICB2YXIgdG9wUG9zaXRpb24sXG4gICAgbGVmdFBvc2l0aW9uLFxuICAgIGFycm93VG9wLFxuICAgIGFycm93TGVmdCxcbiAgICBhcnJvd1dpZHRoLFxuICAgIGFycm93SGVpZ2h0O1xuICBlbGVtZW50LmNsYXNzTmFtZS5pbmRleE9mKHBvc2l0aW9uKSA9PT0gLTEgJiYgKGVsZW1lbnQuY2xhc3NOYW1lID0gZWxlbWVudC5jbGFzc05hbWUucmVwbGFjZSh0aXBQb3NpdGlvbnMscG9zaXRpb24pKTtcbiAgYXJyb3dXaWR0aCA9IGFycm93Lm9mZnNldFdpZHRoOyBhcnJvd0hlaWdodCA9IGFycm93Lm9mZnNldEhlaWdodDtcbiAgaWYgKCBwb3NpdGlvbiA9PT0gJ2xlZnQnIHx8IHBvc2l0aW9uID09PSAncmlnaHQnICkge1xuICAgIGlmICggcG9zaXRpb24gPT09ICdsZWZ0JyApIHtcbiAgICAgIGxlZnRQb3NpdGlvbiA9IHJlY3QubGVmdCArIHNjcm9sbC54IC0gZWxlbWVudERpbWVuc2lvbnMudyAtICggaXNQb3BvdmVyID8gYXJyb3dXaWR0aCA6IDAgKTtcbiAgICB9IGVsc2Uge1xuICAgICAgbGVmdFBvc2l0aW9uID0gcmVjdC5sZWZ0ICsgc2Nyb2xsLnggKyBsaW5rRGltZW5zaW9ucy53O1xuICAgIH1cbiAgICBpZiAoaGFsZlRvcEV4Y2VlZCkge1xuICAgICAgdG9wUG9zaXRpb24gPSByZWN0LnRvcCArIHNjcm9sbC55O1xuICAgICAgYXJyb3dUb3AgPSBsaW5rRGltZW5zaW9ucy5oLzIgLSBhcnJvd1dpZHRoO1xuICAgIH0gZWxzZSBpZiAoaGFsZkJvdHRvbUV4Y2VlZCkge1xuICAgICAgdG9wUG9zaXRpb24gPSByZWN0LnRvcCArIHNjcm9sbC55IC0gZWxlbWVudERpbWVuc2lvbnMuaCArIGxpbmtEaW1lbnNpb25zLmg7XG4gICAgICBhcnJvd1RvcCA9IGVsZW1lbnREaW1lbnNpb25zLmggLSBsaW5rRGltZW5zaW9ucy5oLzIgLSBhcnJvd1dpZHRoO1xuICAgIH0gZWxzZSB7XG4gICAgICB0b3BQb3NpdGlvbiA9IHJlY3QudG9wICsgc2Nyb2xsLnkgLSBlbGVtZW50RGltZW5zaW9ucy5oLzIgKyBsaW5rRGltZW5zaW9ucy5oLzI7XG4gICAgICBhcnJvd1RvcCA9IGVsZW1lbnREaW1lbnNpb25zLmgvMiAtIChpc1BvcG92ZXIgPyBhcnJvd0hlaWdodCowLjkgOiBhcnJvd0hlaWdodC8yKTtcbiAgICB9XG4gIH0gZWxzZSBpZiAoIHBvc2l0aW9uID09PSAndG9wJyB8fCBwb3NpdGlvbiA9PT0gJ2JvdHRvbScgKSB7XG4gICAgaWYgKCBwb3NpdGlvbiA9PT0gJ3RvcCcpIHtcbiAgICAgIHRvcFBvc2l0aW9uID0gIHJlY3QudG9wICsgc2Nyb2xsLnkgLSBlbGVtZW50RGltZW5zaW9ucy5oIC0gKCBpc1BvcG92ZXIgPyBhcnJvd0hlaWdodCA6IDAgKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdG9wUG9zaXRpb24gPSByZWN0LnRvcCArIHNjcm9sbC55ICsgbGlua0RpbWVuc2lvbnMuaDtcbiAgICB9XG4gICAgaWYgKGhhbGZMZWZ0RXhjZWVkKSB7XG4gICAgICBsZWZ0UG9zaXRpb24gPSAwO1xuICAgICAgYXJyb3dMZWZ0ID0gcmVjdC5sZWZ0ICsgbGlua0RpbWVuc2lvbnMudy8yIC0gYXJyb3dXaWR0aDtcbiAgICB9IGVsc2UgaWYgKGhhbGZSaWdodEV4Y2VlZCkge1xuICAgICAgbGVmdFBvc2l0aW9uID0gd2luZG93V2lkdGggLSBlbGVtZW50RGltZW5zaW9ucy53KjEuMDE7XG4gICAgICBhcnJvd0xlZnQgPSBlbGVtZW50RGltZW5zaW9ucy53IC0gKCB3aW5kb3dXaWR0aCAtIHJlY3QubGVmdCApICsgbGlua0RpbWVuc2lvbnMudy8yIC0gYXJyb3dXaWR0aC8yO1xuICAgIH0gZWxzZSB7XG4gICAgICBsZWZ0UG9zaXRpb24gPSByZWN0LmxlZnQgKyBzY3JvbGwueCAtIGVsZW1lbnREaW1lbnNpb25zLncvMiArIGxpbmtEaW1lbnNpb25zLncvMjtcbiAgICAgIGFycm93TGVmdCA9IGVsZW1lbnREaW1lbnNpb25zLncvMiAtICggaXNQb3BvdmVyID8gYXJyb3dXaWR0aCA6IGFycm93V2lkdGgvMiApO1xuICAgIH1cbiAgfVxuICBlbGVtZW50LnN0eWxlLnRvcCA9IHRvcFBvc2l0aW9uICsgJ3B4JztcbiAgZWxlbWVudC5zdHlsZS5sZWZ0ID0gbGVmdFBvc2l0aW9uICsgJ3B4JztcbiAgYXJyb3dUb3AgJiYgKGFycm93LnN0eWxlLnRvcCA9IGFycm93VG9wICsgJ3B4Jyk7XG4gIGFycm93TGVmdCAmJiAoYXJyb3cuc3R5bGUubGVmdCA9IGFycm93TGVmdCArICdweCcpO1xufVxuXG5mdW5jdGlvbiBQb3BvdmVyKGVsZW1lbnQsb3B0aW9ucykge1xuICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgdmFyIHNlbGYgPSB0aGlzO1xuICB2YXIgcG9wb3ZlciA9IG51bGwsXG4gICAgICB0aW1lciA9IDAsXG4gICAgICBpc0lwaG9uZSA9IC8oaVBob25lfGlQb2R8aVBhZCkvLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCksXG4gICAgICB0aXRsZVN0cmluZyxcbiAgICAgIGNvbnRlbnRTdHJpbmcsXG4gICAgICBvcHMgPSB7fTtcbiAgdmFyIHRyaWdnZXJEYXRhLFxuICAgICAgYW5pbWF0aW9uRGF0YSxcbiAgICAgIHBsYWNlbWVudERhdGEsXG4gICAgICBkaXNtaXNzaWJsZURhdGEsXG4gICAgICBkZWxheURhdGEsXG4gICAgICBjb250YWluZXJEYXRhLFxuICAgICAgY2xvc2VCdG4sXG4gICAgICBzaG93Q3VzdG9tRXZlbnQsXG4gICAgICBzaG93bkN1c3RvbUV2ZW50LFxuICAgICAgaGlkZUN1c3RvbUV2ZW50LFxuICAgICAgaGlkZGVuQ3VzdG9tRXZlbnQsXG4gICAgICBjb250YWluZXJFbGVtZW50LFxuICAgICAgY29udGFpbmVyRGF0YUVsZW1lbnQsXG4gICAgICBtb2RhbCxcbiAgICAgIG5hdmJhckZpeGVkVG9wLFxuICAgICAgbmF2YmFyRml4ZWRCb3R0b20sXG4gICAgICBwbGFjZW1lbnRDbGFzcztcbiAgZnVuY3Rpb24gZGlzbWlzc2libGVIYW5kbGVyKGUpIHtcbiAgICBpZiAocG9wb3ZlciAhPT0gbnVsbCAmJiBlLnRhcmdldCA9PT0gcXVlcnlFbGVtZW50KCcuY2xvc2UnLHBvcG92ZXIpKSB7XG4gICAgICBzZWxmLmhpZGUoKTtcbiAgICB9XG4gIH1cbiAgZnVuY3Rpb24gZ2V0Q29udGVudHMoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIDAgOiBvcHRpb25zLnRpdGxlIHx8IGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLXRpdGxlJykgfHwgbnVsbCxcbiAgICAgIDEgOiBvcHRpb25zLmNvbnRlbnQgfHwgZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtY29udGVudCcpIHx8IG51bGxcbiAgICB9XG4gIH1cbiAgZnVuY3Rpb24gcmVtb3ZlUG9wb3ZlcigpIHtcbiAgICBvcHMuY29udGFpbmVyLnJlbW92ZUNoaWxkKHBvcG92ZXIpO1xuICAgIHRpbWVyID0gbnVsbDsgcG9wb3ZlciA9IG51bGw7XG4gIH1cbiAgZnVuY3Rpb24gY3JlYXRlUG9wb3ZlcigpIHtcbiAgICB0aXRsZVN0cmluZyA9IGdldENvbnRlbnRzKClbMF0gfHwgbnVsbDtcbiAgICBjb250ZW50U3RyaW5nID0gZ2V0Q29udGVudHMoKVsxXTtcbiAgICBjb250ZW50U3RyaW5nID0gISFjb250ZW50U3RyaW5nID8gY29udGVudFN0cmluZy50cmltKCkgOiBudWxsO1xuICAgIHBvcG92ZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICB2YXIgcG9wb3ZlckFycm93ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgcG9wb3ZlckFycm93LmNsYXNzTGlzdC5hZGQoJ2Fycm93Jyk7XG4gICAgcG9wb3Zlci5hcHBlbmRDaGlsZChwb3BvdmVyQXJyb3cpO1xuICAgIGlmICggY29udGVudFN0cmluZyAhPT0gbnVsbCAmJiBvcHMudGVtcGxhdGUgPT09IG51bGwgKSB7XG4gICAgICBwb3BvdmVyLnNldEF0dHJpYnV0ZSgncm9sZScsJ3Rvb2x0aXAnKTtcbiAgICAgIGlmICh0aXRsZVN0cmluZyAhPT0gbnVsbCkge1xuICAgICAgICB2YXIgcG9wb3ZlclRpdGxlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaDMnKTtcbiAgICAgICAgcG9wb3ZlclRpdGxlLmNsYXNzTGlzdC5hZGQoJ3BvcG92ZXItaGVhZGVyJyk7XG4gICAgICAgIHBvcG92ZXJUaXRsZS5pbm5lckhUTUwgPSBvcHMuZGlzbWlzc2libGUgPyB0aXRsZVN0cmluZyArIGNsb3NlQnRuIDogdGl0bGVTdHJpbmc7XG4gICAgICAgIHBvcG92ZXIuYXBwZW5kQ2hpbGQocG9wb3ZlclRpdGxlKTtcbiAgICAgIH1cbiAgICAgIHZhciBwb3BvdmVyQm9keU1hcmt1cCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgcG9wb3ZlckJvZHlNYXJrdXAuY2xhc3NMaXN0LmFkZCgncG9wb3Zlci1ib2R5Jyk7XG4gICAgICBwb3BvdmVyQm9keU1hcmt1cC5pbm5lckhUTUwgPSBvcHMuZGlzbWlzc2libGUgJiYgdGl0bGVTdHJpbmcgPT09IG51bGwgPyBjb250ZW50U3RyaW5nICsgY2xvc2VCdG4gOiBjb250ZW50U3RyaW5nO1xuICAgICAgcG9wb3Zlci5hcHBlbmRDaGlsZChwb3BvdmVyQm9keU1hcmt1cCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHZhciBwb3BvdmVyVGVtcGxhdGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgIHBvcG92ZXJUZW1wbGF0ZS5pbm5lckhUTUwgPSBvcHMudGVtcGxhdGUudHJpbSgpO1xuICAgICAgcG9wb3Zlci5jbGFzc05hbWUgPSBwb3BvdmVyVGVtcGxhdGUuZmlyc3RDaGlsZC5jbGFzc05hbWU7XG4gICAgICBwb3BvdmVyLmlubmVySFRNTCA9IHBvcG92ZXJUZW1wbGF0ZS5maXJzdENoaWxkLmlubmVySFRNTDtcbiAgICAgIHZhciBwb3BvdmVySGVhZGVyID0gcXVlcnlFbGVtZW50KCcucG9wb3Zlci1oZWFkZXInLHBvcG92ZXIpLFxuICAgICAgICAgIHBvcG92ZXJCb2R5ID0gcXVlcnlFbGVtZW50KCcucG9wb3Zlci1ib2R5Jyxwb3BvdmVyKTtcbiAgICAgIHRpdGxlU3RyaW5nICYmIHBvcG92ZXJIZWFkZXIgJiYgKHBvcG92ZXJIZWFkZXIuaW5uZXJIVE1MID0gdGl0bGVTdHJpbmcudHJpbSgpKTtcbiAgICAgIGNvbnRlbnRTdHJpbmcgJiYgcG9wb3ZlckJvZHkgJiYgKHBvcG92ZXJCb2R5LmlubmVySFRNTCA9IGNvbnRlbnRTdHJpbmcudHJpbSgpKTtcbiAgICB9XG4gICAgb3BzLmNvbnRhaW5lci5hcHBlbmRDaGlsZChwb3BvdmVyKTtcbiAgICBwb3BvdmVyLnN0eWxlLmRpc3BsYXkgPSAnYmxvY2snO1xuICAgICFwb3BvdmVyLmNsYXNzTGlzdC5jb250YWlucyggJ3BvcG92ZXInKSAmJiBwb3BvdmVyLmNsYXNzTGlzdC5hZGQoJ3BvcG92ZXInKTtcbiAgICAhcG9wb3Zlci5jbGFzc0xpc3QuY29udGFpbnMoIG9wcy5hbmltYXRpb24pICYmIHBvcG92ZXIuY2xhc3NMaXN0LmFkZChvcHMuYW5pbWF0aW9uKTtcbiAgICAhcG9wb3Zlci5jbGFzc0xpc3QuY29udGFpbnMoIHBsYWNlbWVudENsYXNzKSAmJiBwb3BvdmVyLmNsYXNzTGlzdC5hZGQocGxhY2VtZW50Q2xhc3MpO1xuICB9XG4gIGZ1bmN0aW9uIHNob3dQb3BvdmVyKCkge1xuICAgICFwb3BvdmVyLmNsYXNzTGlzdC5jb250YWlucygnc2hvdycpICYmICggcG9wb3Zlci5jbGFzc0xpc3QuYWRkKCdzaG93JykgKTtcbiAgfVxuICBmdW5jdGlvbiB1cGRhdGVQb3BvdmVyKCkge1xuICAgIHN0eWxlVGlwKGVsZW1lbnQsIHBvcG92ZXIsIG9wcy5wbGFjZW1lbnQsIG9wcy5jb250YWluZXIpO1xuICB9XG4gIGZ1bmN0aW9uIGZvcmNlRm9jdXMgKCkge1xuICAgIGlmIChwb3BvdmVyID09PSBudWxsKSB7IGVsZW1lbnQuZm9jdXMoKTsgfVxuICB9XG4gIGZ1bmN0aW9uIHRvZ2dsZUV2ZW50cyhhY3Rpb24pIHtcbiAgICBhY3Rpb24gPSBhY3Rpb24gPyAnYWRkRXZlbnRMaXN0ZW5lcicgOiAncmVtb3ZlRXZlbnRMaXN0ZW5lcic7XG4gICAgaWYgKG9wcy50cmlnZ2VyID09PSAnaG92ZXInKSB7XG4gICAgICBlbGVtZW50W2FjdGlvbl0oIG1vdXNlQ2xpY2tFdmVudHMuZG93biwgc2VsZi5zaG93ICk7XG4gICAgICBlbGVtZW50W2FjdGlvbl0oIG1vdXNlSG92ZXJFdmVudHNbMF0sIHNlbGYuc2hvdyApO1xuICAgICAgaWYgKCFvcHMuZGlzbWlzc2libGUpIHsgZWxlbWVudFthY3Rpb25dKCBtb3VzZUhvdmVyRXZlbnRzWzFdLCBzZWxmLmhpZGUgKTsgfVxuICAgIH0gZWxzZSBpZiAoJ2NsaWNrJyA9PSBvcHMudHJpZ2dlcikge1xuICAgICAgZWxlbWVudFthY3Rpb25dKCBvcHMudHJpZ2dlciwgc2VsZi50b2dnbGUgKTtcbiAgICB9IGVsc2UgaWYgKCdmb2N1cycgPT0gb3BzLnRyaWdnZXIpIHtcbiAgICAgIGlzSXBob25lICYmIGVsZW1lbnRbYWN0aW9uXSggJ2NsaWNrJywgZm9yY2VGb2N1cywgZmFsc2UgKTtcbiAgICAgIGVsZW1lbnRbYWN0aW9uXSggb3BzLnRyaWdnZXIsIHNlbGYudG9nZ2xlICk7XG4gICAgfVxuICB9XG4gIGZ1bmN0aW9uIHRvdWNoSGFuZGxlcihlKXtcbiAgICBpZiAoIHBvcG92ZXIgJiYgcG9wb3Zlci5jb250YWlucyhlLnRhcmdldCkgfHwgZS50YXJnZXQgPT09IGVsZW1lbnQgfHwgZWxlbWVudC5jb250YWlucyhlLnRhcmdldCkpIDsgZWxzZSB7XG4gICAgICBzZWxmLmhpZGUoKTtcbiAgICB9XG4gIH1cbiAgZnVuY3Rpb24gZGlzbWlzc0hhbmRsZXJUb2dnbGUoYWN0aW9uKSB7XG4gICAgYWN0aW9uID0gYWN0aW9uID8gJ2FkZEV2ZW50TGlzdGVuZXInIDogJ3JlbW92ZUV2ZW50TGlzdGVuZXInO1xuICAgIGlmIChvcHMuZGlzbWlzc2libGUpIHtcbiAgICAgIGRvY3VtZW50W2FjdGlvbl0oJ2NsaWNrJywgZGlzbWlzc2libGVIYW5kbGVyLCBmYWxzZSApO1xuICAgIH0gZWxzZSB7XG4gICAgICAnZm9jdXMnID09IG9wcy50cmlnZ2VyICYmIGVsZW1lbnRbYWN0aW9uXSggJ2JsdXInLCBzZWxmLmhpZGUgKTtcbiAgICAgICdob3ZlcicgPT0gb3BzLnRyaWdnZXIgJiYgZG9jdW1lbnRbYWN0aW9uXSggJ3RvdWNoc3RhcnQnLCB0b3VjaEhhbmRsZXIsIHBhc3NpdmVIYW5kbGVyICk7XG4gICAgfVxuICAgIHdpbmRvd1thY3Rpb25dKCdyZXNpemUnLCBzZWxmLmhpZGUsIHBhc3NpdmVIYW5kbGVyICk7XG4gIH1cbiAgZnVuY3Rpb24gc2hvd1RyaWdnZXIoKSB7XG4gICAgZGlzbWlzc0hhbmRsZXJUb2dnbGUoMSk7XG4gICAgZGlzcGF0Y2hDdXN0b21FdmVudC5jYWxsKGVsZW1lbnQsIHNob3duQ3VzdG9tRXZlbnQpO1xuICB9XG4gIGZ1bmN0aW9uIGhpZGVUcmlnZ2VyKCkge1xuICAgIGRpc21pc3NIYW5kbGVyVG9nZ2xlKCk7XG4gICAgcmVtb3ZlUG9wb3ZlcigpO1xuICAgIGRpc3BhdGNoQ3VzdG9tRXZlbnQuY2FsbChlbGVtZW50LCBoaWRkZW5DdXN0b21FdmVudCk7XG4gIH1cbiAgc2VsZi50b2dnbGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKHBvcG92ZXIgPT09IG51bGwpIHsgc2VsZi5zaG93KCk7IH1cbiAgICBlbHNlIHsgc2VsZi5oaWRlKCk7IH1cbiAgfTtcbiAgc2VsZi5zaG93ID0gZnVuY3Rpb24gKCkge1xuICAgIGNsZWFyVGltZW91dCh0aW1lcik7XG4gICAgdGltZXIgPSBzZXRUaW1lb3V0KCBmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAocG9wb3ZlciA9PT0gbnVsbCkge1xuICAgICAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwoZWxlbWVudCwgc2hvd0N1c3RvbUV2ZW50KTtcbiAgICAgICAgaWYgKCBzaG93Q3VzdG9tRXZlbnQuZGVmYXVsdFByZXZlbnRlZCApIHsgcmV0dXJuOyB9XG4gICAgICAgIGNyZWF0ZVBvcG92ZXIoKTtcbiAgICAgICAgdXBkYXRlUG9wb3ZlcigpO1xuICAgICAgICBzaG93UG9wb3ZlcigpO1xuICAgICAgICAhIW9wcy5hbmltYXRpb24gPyBlbXVsYXRlVHJhbnNpdGlvbkVuZChwb3BvdmVyLCBzaG93VHJpZ2dlcikgOiBzaG93VHJpZ2dlcigpO1xuICAgICAgfVxuICAgIH0sIDIwICk7XG4gIH07XG4gIHNlbGYuaGlkZSA9IGZ1bmN0aW9uICgpIHtcbiAgICBjbGVhclRpbWVvdXQodGltZXIpO1xuICAgIHRpbWVyID0gc2V0VGltZW91dCggZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKHBvcG92ZXIgJiYgcG9wb3ZlciAhPT0gbnVsbCAmJiBwb3BvdmVyLmNsYXNzTGlzdC5jb250YWlucygnc2hvdycpKSB7XG4gICAgICAgIGRpc3BhdGNoQ3VzdG9tRXZlbnQuY2FsbChlbGVtZW50LCBoaWRlQ3VzdG9tRXZlbnQpO1xuICAgICAgICBpZiAoIGhpZGVDdXN0b21FdmVudC5kZWZhdWx0UHJldmVudGVkICkgeyByZXR1cm47IH1cbiAgICAgICAgcG9wb3Zlci5jbGFzc0xpc3QucmVtb3ZlKCdzaG93Jyk7XG4gICAgICAgICEhb3BzLmFuaW1hdGlvbiA/IGVtdWxhdGVUcmFuc2l0aW9uRW5kKHBvcG92ZXIsIGhpZGVUcmlnZ2VyKSA6IGhpZGVUcmlnZ2VyKCk7XG4gICAgICB9XG4gICAgfSwgb3BzLmRlbGF5ICk7XG4gIH07XG4gIHNlbGYuZGlzcG9zZSA9IGZ1bmN0aW9uICgpIHtcbiAgICBzZWxmLmhpZGUoKTtcbiAgICB0b2dnbGVFdmVudHMoKTtcbiAgICBkZWxldGUgZWxlbWVudC5Qb3BvdmVyO1xuICB9O1xuICBlbGVtZW50ID0gcXVlcnlFbGVtZW50KGVsZW1lbnQpO1xuICBlbGVtZW50LlBvcG92ZXIgJiYgZWxlbWVudC5Qb3BvdmVyLmRpc3Bvc2UoKTtcbiAgdHJpZ2dlckRhdGEgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS10cmlnZ2VyJyk7XG4gIGFuaW1hdGlvbkRhdGEgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1hbmltYXRpb24nKTtcbiAgcGxhY2VtZW50RGF0YSA9IGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLXBsYWNlbWVudCcpO1xuICBkaXNtaXNzaWJsZURhdGEgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1kaXNtaXNzaWJsZScpO1xuICBkZWxheURhdGEgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1kZWxheScpO1xuICBjb250YWluZXJEYXRhID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtY29udGFpbmVyJyk7XG4gIGNsb3NlQnRuID0gJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiY2xvc2VcIj7DlzwvYnV0dG9uPic7XG4gIHNob3dDdXN0b21FdmVudCA9IGJvb3RzdHJhcEN1c3RvbUV2ZW50KCdzaG93JywgJ3BvcG92ZXInKTtcbiAgc2hvd25DdXN0b21FdmVudCA9IGJvb3RzdHJhcEN1c3RvbUV2ZW50KCdzaG93bicsICdwb3BvdmVyJyk7XG4gIGhpZGVDdXN0b21FdmVudCA9IGJvb3RzdHJhcEN1c3RvbUV2ZW50KCdoaWRlJywgJ3BvcG92ZXInKTtcbiAgaGlkZGVuQ3VzdG9tRXZlbnQgPSBib290c3RyYXBDdXN0b21FdmVudCgnaGlkZGVuJywgJ3BvcG92ZXInKTtcbiAgY29udGFpbmVyRWxlbWVudCA9IHF1ZXJ5RWxlbWVudChvcHRpb25zLmNvbnRhaW5lcik7XG4gIGNvbnRhaW5lckRhdGFFbGVtZW50ID0gcXVlcnlFbGVtZW50KGNvbnRhaW5lckRhdGEpO1xuICBtb2RhbCA9IGVsZW1lbnQuY2xvc2VzdCgnLm1vZGFsJyk7XG4gIG5hdmJhckZpeGVkVG9wID0gZWxlbWVudC5jbG9zZXN0KCcuZml4ZWQtdG9wJyk7XG4gIG5hdmJhckZpeGVkQm90dG9tID0gZWxlbWVudC5jbG9zZXN0KCcuZml4ZWQtYm90dG9tJyk7XG4gIG9wcy50ZW1wbGF0ZSA9IG9wdGlvbnMudGVtcGxhdGUgPyBvcHRpb25zLnRlbXBsYXRlIDogbnVsbDtcbiAgb3BzLnRyaWdnZXIgPSBvcHRpb25zLnRyaWdnZXIgPyBvcHRpb25zLnRyaWdnZXIgOiB0cmlnZ2VyRGF0YSB8fCAnaG92ZXInO1xuICBvcHMuYW5pbWF0aW9uID0gb3B0aW9ucy5hbmltYXRpb24gJiYgb3B0aW9ucy5hbmltYXRpb24gIT09ICdmYWRlJyA/IG9wdGlvbnMuYW5pbWF0aW9uIDogYW5pbWF0aW9uRGF0YSB8fCAnZmFkZSc7XG4gIG9wcy5wbGFjZW1lbnQgPSBvcHRpb25zLnBsYWNlbWVudCA/IG9wdGlvbnMucGxhY2VtZW50IDogcGxhY2VtZW50RGF0YSB8fCAndG9wJztcbiAgb3BzLmRlbGF5ID0gcGFyc2VJbnQob3B0aW9ucy5kZWxheSB8fCBkZWxheURhdGEpIHx8IDIwMDtcbiAgb3BzLmRpc21pc3NpYmxlID0gb3B0aW9ucy5kaXNtaXNzaWJsZSB8fCBkaXNtaXNzaWJsZURhdGEgPT09ICd0cnVlJyA/IHRydWUgOiBmYWxzZTtcbiAgb3BzLmNvbnRhaW5lciA9IGNvbnRhaW5lckVsZW1lbnQgPyBjb250YWluZXJFbGVtZW50XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDogY29udGFpbmVyRGF0YUVsZW1lbnQgPyBjb250YWluZXJEYXRhRWxlbWVudFxuICAgICAgICAgICAgICAgICAgICAgICAgICA6IG5hdmJhckZpeGVkVG9wID8gbmF2YmFyRml4ZWRUb3BcbiAgICAgICAgICAgICAgICAgICAgICAgICAgOiBuYXZiYXJGaXhlZEJvdHRvbSA/IG5hdmJhckZpeGVkQm90dG9tXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDogbW9kYWwgPyBtb2RhbCA6IGRvY3VtZW50LmJvZHk7XG4gIHBsYWNlbWVudENsYXNzID0gXCJicy1wb3BvdmVyLVwiICsgKG9wcy5wbGFjZW1lbnQpO1xuICB2YXIgcG9wb3ZlckNvbnRlbnRzID0gZ2V0Q29udGVudHMoKTtcbiAgdGl0bGVTdHJpbmcgPSBwb3BvdmVyQ29udGVudHNbMF07XG4gIGNvbnRlbnRTdHJpbmcgPSBwb3BvdmVyQ29udGVudHNbMV07XG4gIGlmICggIWNvbnRlbnRTdHJpbmcgJiYgIW9wcy50ZW1wbGF0ZSApIHsgcmV0dXJuOyB9XG4gIGlmICggIWVsZW1lbnQuUG9wb3ZlciApIHtcbiAgICB0b2dnbGVFdmVudHMoMSk7XG4gIH1cbiAgZWxlbWVudC5Qb3BvdmVyID0gc2VsZjtcbn1cblxuZnVuY3Rpb24gU2Nyb2xsU3B5KGVsZW1lbnQsb3B0aW9ucykge1xuICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgdmFyIHNlbGYgPSB0aGlzLFxuICAgIHZhcnMsXG4gICAgdGFyZ2V0RGF0YSxcbiAgICBvZmZzZXREYXRhLFxuICAgIHNweVRhcmdldCxcbiAgICBzY3JvbGxUYXJnZXQsXG4gICAgb3BzID0ge307XG4gIGZ1bmN0aW9uIHVwZGF0ZVRhcmdldHMoKXtcbiAgICB2YXIgbGlua3MgPSBzcHlUYXJnZXQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ0EnKTtcbiAgICBpZiAodmFycy5sZW5ndGggIT09IGxpbmtzLmxlbmd0aCkge1xuICAgICAgdmFycy5pdGVtcyA9IFtdO1xuICAgICAgdmFycy50YXJnZXRzID0gW107XG4gICAgICBBcnJheS5mcm9tKGxpbmtzKS5tYXAoZnVuY3Rpb24gKGxpbmspe1xuICAgICAgICB2YXIgaHJlZiA9IGxpbmsuZ2V0QXR0cmlidXRlKCdocmVmJyksXG4gICAgICAgICAgdGFyZ2V0SXRlbSA9IGhyZWYgJiYgaHJlZi5jaGFyQXQoMCkgPT09ICcjJyAmJiBocmVmLnNsaWNlKC0xKSAhPT0gJyMnICYmIHF1ZXJ5RWxlbWVudChocmVmKTtcbiAgICAgICAgaWYgKCB0YXJnZXRJdGVtICkge1xuICAgICAgICAgIHZhcnMuaXRlbXMucHVzaChsaW5rKTtcbiAgICAgICAgICB2YXJzLnRhcmdldHMucHVzaCh0YXJnZXRJdGVtKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgICB2YXJzLmxlbmd0aCA9IGxpbmtzLmxlbmd0aDtcbiAgICB9XG4gIH1cbiAgZnVuY3Rpb24gdXBkYXRlSXRlbShpbmRleCkge1xuICAgIHZhciBpdGVtID0gdmFycy5pdGVtc1tpbmRleF0sXG4gICAgICB0YXJnZXRJdGVtID0gdmFycy50YXJnZXRzW2luZGV4XSxcbiAgICAgIGRyb3BtZW51ID0gaXRlbS5jbGFzc0xpc3QuY29udGFpbnMoJ2Ryb3Bkb3duLWl0ZW0nKSAmJiBpdGVtLmNsb3Nlc3QoJy5kcm9wZG93bi1tZW51JyksXG4gICAgICBkcm9wTGluayA9IGRyb3BtZW51ICYmIGRyb3BtZW51LnByZXZpb3VzRWxlbWVudFNpYmxpbmcsXG4gICAgICBuZXh0U2libGluZyA9IGl0ZW0ubmV4dEVsZW1lbnRTaWJsaW5nLFxuICAgICAgYWN0aXZlU2libGluZyA9IG5leHRTaWJsaW5nICYmIG5leHRTaWJsaW5nLmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2FjdGl2ZScpLmxlbmd0aCxcbiAgICAgIHRhcmdldFJlY3QgPSB2YXJzLmlzV2luZG93ICYmIHRhcmdldEl0ZW0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCksXG4gICAgICBpc0FjdGl2ZSA9IGl0ZW0uY2xhc3NMaXN0LmNvbnRhaW5zKCdhY3RpdmUnKSB8fCBmYWxzZSxcbiAgICAgIHRvcEVkZ2UgPSAodmFycy5pc1dpbmRvdyA/IHRhcmdldFJlY3QudG9wICsgdmFycy5zY3JvbGxPZmZzZXQgOiB0YXJnZXRJdGVtLm9mZnNldFRvcCkgLSBvcHMub2Zmc2V0LFxuICAgICAgYm90dG9tRWRnZSA9IHZhcnMuaXNXaW5kb3cgPyB0YXJnZXRSZWN0LmJvdHRvbSArIHZhcnMuc2Nyb2xsT2Zmc2V0IC0gb3BzLm9mZnNldFxuICAgICAgICAgICAgICAgICA6IHZhcnMudGFyZ2V0c1tpbmRleCsxXSA/IHZhcnMudGFyZ2V0c1tpbmRleCsxXS5vZmZzZXRUb3AgLSBvcHMub2Zmc2V0XG4gICAgICAgICAgICAgICAgIDogZWxlbWVudC5zY3JvbGxIZWlnaHQsXG4gICAgICBpbnNpZGUgPSBhY3RpdmVTaWJsaW5nIHx8IHZhcnMuc2Nyb2xsT2Zmc2V0ID49IHRvcEVkZ2UgJiYgYm90dG9tRWRnZSA+IHZhcnMuc2Nyb2xsT2Zmc2V0O1xuICAgICBpZiAoICFpc0FjdGl2ZSAmJiBpbnNpZGUgKSB7XG4gICAgICBpdGVtLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpO1xuICAgICAgaWYgKGRyb3BMaW5rICYmICFkcm9wTGluay5jbGFzc0xpc3QuY29udGFpbnMoJ2FjdGl2ZScpICkge1xuICAgICAgICBkcm9wTGluay5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcbiAgICAgIH1cbiAgICAgIGRpc3BhdGNoQ3VzdG9tRXZlbnQuY2FsbChlbGVtZW50LCBib290c3RyYXBDdXN0b21FdmVudCggJ2FjdGl2YXRlJywgJ3Njcm9sbHNweScsIHZhcnMuaXRlbXNbaW5kZXhdKSk7XG4gICAgfSBlbHNlIGlmICggaXNBY3RpdmUgJiYgIWluc2lkZSApIHtcbiAgICAgIGl0ZW0uY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJyk7XG4gICAgICBpZiAoZHJvcExpbmsgJiYgZHJvcExpbmsuY2xhc3NMaXN0LmNvbnRhaW5zKCdhY3RpdmUnKSAmJiAhaXRlbS5wYXJlbnROb2RlLmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2FjdGl2ZScpLmxlbmd0aCApIHtcbiAgICAgICAgZHJvcExpbmsuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJyk7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmICggaXNBY3RpdmUgJiYgaW5zaWRlIHx8ICFpbnNpZGUgJiYgIWlzQWN0aXZlICkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgfVxuICBmdW5jdGlvbiB1cGRhdGVJdGVtcygpIHtcbiAgICB1cGRhdGVUYXJnZXRzKCk7XG4gICAgdmFycy5zY3JvbGxPZmZzZXQgPSB2YXJzLmlzV2luZG93ID8gZ2V0U2Nyb2xsKCkueSA6IGVsZW1lbnQuc2Nyb2xsVG9wO1xuICAgIHZhcnMuaXRlbXMubWFwKGZ1bmN0aW9uIChsLGlkeCl7IHJldHVybiB1cGRhdGVJdGVtKGlkeCk7IH0pO1xuICB9XG4gIGZ1bmN0aW9uIHRvZ2dsZUV2ZW50cyhhY3Rpb24pIHtcbiAgICBhY3Rpb24gPSBhY3Rpb24gPyAnYWRkRXZlbnRMaXN0ZW5lcicgOiAncmVtb3ZlRXZlbnRMaXN0ZW5lcic7XG4gICAgc2Nyb2xsVGFyZ2V0W2FjdGlvbl0oJ3Njcm9sbCcsIHNlbGYucmVmcmVzaCwgcGFzc2l2ZUhhbmRsZXIgKTtcbiAgICB3aW5kb3dbYWN0aW9uXSggJ3Jlc2l6ZScsIHNlbGYucmVmcmVzaCwgcGFzc2l2ZUhhbmRsZXIgKTtcbiAgfVxuICBzZWxmLnJlZnJlc2ggPSBmdW5jdGlvbiAoKSB7XG4gICAgdXBkYXRlSXRlbXMoKTtcbiAgfTtcbiAgc2VsZi5kaXNwb3NlID0gZnVuY3Rpb24gKCkge1xuICAgIHRvZ2dsZUV2ZW50cygpO1xuICAgIGRlbGV0ZSBlbGVtZW50LlNjcm9sbFNweTtcbiAgfTtcbiAgZWxlbWVudCA9IHF1ZXJ5RWxlbWVudChlbGVtZW50KTtcbiAgZWxlbWVudC5TY3JvbGxTcHkgJiYgZWxlbWVudC5TY3JvbGxTcHkuZGlzcG9zZSgpO1xuICB0YXJnZXREYXRhID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtdGFyZ2V0Jyk7XG4gIG9mZnNldERhdGEgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1vZmZzZXQnKTtcbiAgc3B5VGFyZ2V0ID0gcXVlcnlFbGVtZW50KG9wdGlvbnMudGFyZ2V0IHx8IHRhcmdldERhdGEpO1xuICBzY3JvbGxUYXJnZXQgPSBlbGVtZW50Lm9mZnNldEhlaWdodCA8IGVsZW1lbnQuc2Nyb2xsSGVpZ2h0ID8gZWxlbWVudCA6IHdpbmRvdztcbiAgaWYgKCFzcHlUYXJnZXQpIHsgcmV0dXJuIH1cbiAgb3BzLnRhcmdldCA9IHNweVRhcmdldDtcbiAgb3BzLm9mZnNldCA9IHBhcnNlSW50KG9wdGlvbnMub2Zmc2V0IHx8IG9mZnNldERhdGEpIHx8IDEwO1xuICB2YXJzID0ge307XG4gIHZhcnMubGVuZ3RoID0gMDtcbiAgdmFycy5pdGVtcyA9IFtdO1xuICB2YXJzLnRhcmdldHMgPSBbXTtcbiAgdmFycy5pc1dpbmRvdyA9IHNjcm9sbFRhcmdldCA9PT0gd2luZG93O1xuICBpZiAoICFlbGVtZW50LlNjcm9sbFNweSApIHtcbiAgICB0b2dnbGVFdmVudHMoMSk7XG4gIH1cbiAgc2VsZi5yZWZyZXNoKCk7XG4gIGVsZW1lbnQuU2Nyb2xsU3B5ID0gc2VsZjtcbn1cblxuZnVuY3Rpb24gVGFiKGVsZW1lbnQsb3B0aW9ucykge1xuICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgdmFyIHNlbGYgPSB0aGlzLFxuICAgIGhlaWdodERhdGEsXG4gICAgdGFicywgZHJvcGRvd24sXG4gICAgc2hvd0N1c3RvbUV2ZW50LFxuICAgIHNob3duQ3VzdG9tRXZlbnQsXG4gICAgaGlkZUN1c3RvbUV2ZW50LFxuICAgIGhpZGRlbkN1c3RvbUV2ZW50LFxuICAgIG5leHQsXG4gICAgdGFic0NvbnRlbnRDb250YWluZXIgPSBmYWxzZSxcbiAgICBhY3RpdmVUYWIsXG4gICAgYWN0aXZlQ29udGVudCxcbiAgICBuZXh0Q29udGVudCxcbiAgICBjb250YWluZXJIZWlnaHQsXG4gICAgZXF1YWxDb250ZW50cyxcbiAgICBuZXh0SGVpZ2h0LFxuICAgIGFuaW1hdGVIZWlnaHQ7XG4gIGZ1bmN0aW9uIHRyaWdnZXJFbmQoKSB7XG4gICAgdGFic0NvbnRlbnRDb250YWluZXIuc3R5bGUuaGVpZ2h0ID0gJyc7XG4gICAgdGFic0NvbnRlbnRDb250YWluZXIuY2xhc3NMaXN0LnJlbW92ZSgnY29sbGFwc2luZycpO1xuICAgIHRhYnMuaXNBbmltYXRpbmcgPSBmYWxzZTtcbiAgfVxuICBmdW5jdGlvbiB0cmlnZ2VyU2hvdygpIHtcbiAgICBpZiAodGFic0NvbnRlbnRDb250YWluZXIpIHtcbiAgICAgIGlmICggZXF1YWxDb250ZW50cyApIHtcbiAgICAgICAgdHJpZ2dlckVuZCgpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGFic0NvbnRlbnRDb250YWluZXIuc3R5bGUuaGVpZ2h0ID0gbmV4dEhlaWdodCArIFwicHhcIjtcbiAgICAgICAgICB0YWJzQ29udGVudENvbnRhaW5lci5vZmZzZXRXaWR0aDtcbiAgICAgICAgICBlbXVsYXRlVHJhbnNpdGlvbkVuZCh0YWJzQ29udGVudENvbnRhaW5lciwgdHJpZ2dlckVuZCk7XG4gICAgICAgIH0sNTApO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB0YWJzLmlzQW5pbWF0aW5nID0gZmFsc2U7XG4gICAgfVxuICAgIHNob3duQ3VzdG9tRXZlbnQgPSBib290c3RyYXBDdXN0b21FdmVudCgnc2hvd24nLCAndGFiJywgYWN0aXZlVGFiKTtcbiAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwobmV4dCwgc2hvd25DdXN0b21FdmVudCk7XG4gIH1cbiAgZnVuY3Rpb24gdHJpZ2dlckhpZGUoKSB7XG4gICAgaWYgKHRhYnNDb250ZW50Q29udGFpbmVyKSB7XG4gICAgICBhY3RpdmVDb250ZW50LnN0eWxlLmZsb2F0ID0gJ2xlZnQnO1xuICAgICAgbmV4dENvbnRlbnQuc3R5bGUuZmxvYXQgPSAnbGVmdCc7XG4gICAgICBjb250YWluZXJIZWlnaHQgPSBhY3RpdmVDb250ZW50LnNjcm9sbEhlaWdodDtcbiAgICB9XG4gICAgc2hvd0N1c3RvbUV2ZW50ID0gYm9vdHN0cmFwQ3VzdG9tRXZlbnQoJ3Nob3cnLCAndGFiJywgYWN0aXZlVGFiKTtcbiAgICBoaWRkZW5DdXN0b21FdmVudCA9IGJvb3RzdHJhcEN1c3RvbUV2ZW50KCdoaWRkZW4nLCAndGFiJywgbmV4dCk7XG4gICAgZGlzcGF0Y2hDdXN0b21FdmVudC5jYWxsKG5leHQsIHNob3dDdXN0b21FdmVudCk7XG4gICAgaWYgKCBzaG93Q3VzdG9tRXZlbnQuZGVmYXVsdFByZXZlbnRlZCApIHsgcmV0dXJuOyB9XG4gICAgbmV4dENvbnRlbnQuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XG4gICAgYWN0aXZlQ29udGVudC5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKTtcbiAgICBpZiAodGFic0NvbnRlbnRDb250YWluZXIpIHtcbiAgICAgIG5leHRIZWlnaHQgPSBuZXh0Q29udGVudC5zY3JvbGxIZWlnaHQ7XG4gICAgICBlcXVhbENvbnRlbnRzID0gbmV4dEhlaWdodCA9PT0gY29udGFpbmVySGVpZ2h0O1xuICAgICAgdGFic0NvbnRlbnRDb250YWluZXIuY2xhc3NMaXN0LmFkZCgnY29sbGFwc2luZycpO1xuICAgICAgdGFic0NvbnRlbnRDb250YWluZXIuc3R5bGUuaGVpZ2h0ID0gY29udGFpbmVySGVpZ2h0ICsgXCJweFwiO1xuICAgICAgdGFic0NvbnRlbnRDb250YWluZXIub2Zmc2V0SGVpZ2h0O1xuICAgICAgYWN0aXZlQ29udGVudC5zdHlsZS5mbG9hdCA9ICcnO1xuICAgICAgbmV4dENvbnRlbnQuc3R5bGUuZmxvYXQgPSAnJztcbiAgICB9XG4gICAgaWYgKCBuZXh0Q29udGVudC5jbGFzc0xpc3QuY29udGFpbnMoJ2ZhZGUnKSApIHtcbiAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICBuZXh0Q29udGVudC5jbGFzc0xpc3QuYWRkKCdzaG93Jyk7XG4gICAgICAgIGVtdWxhdGVUcmFuc2l0aW9uRW5kKG5leHRDb250ZW50LHRyaWdnZXJTaG93KTtcbiAgICAgIH0sMjApO1xuICAgIH0gZWxzZSB7IHRyaWdnZXJTaG93KCk7IH1cbiAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwoYWN0aXZlVGFiLCBoaWRkZW5DdXN0b21FdmVudCk7XG4gIH1cbiAgZnVuY3Rpb24gZ2V0QWN0aXZlVGFiKCkge1xuICAgIHZhciBhY3RpdmVUYWJzID0gdGFicy5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdhY3RpdmUnKSwgYWN0aXZlVGFiO1xuICAgIGlmICggYWN0aXZlVGFicy5sZW5ndGggPT09IDEgJiYgIWFjdGl2ZVRhYnNbMF0ucGFyZW50Tm9kZS5jbGFzc0xpc3QuY29udGFpbnMoJ2Ryb3Bkb3duJykgKSB7XG4gICAgICBhY3RpdmVUYWIgPSBhY3RpdmVUYWJzWzBdO1xuICAgIH0gZWxzZSBpZiAoIGFjdGl2ZVRhYnMubGVuZ3RoID4gMSApIHtcbiAgICAgIGFjdGl2ZVRhYiA9IGFjdGl2ZVRhYnNbYWN0aXZlVGFicy5sZW5ndGgtMV07XG4gICAgfVxuICAgIHJldHVybiBhY3RpdmVUYWI7XG4gIH1cbiAgZnVuY3Rpb24gZ2V0QWN0aXZlQ29udGVudCgpIHsgcmV0dXJuIHF1ZXJ5RWxlbWVudChnZXRBY3RpdmVUYWIoKS5nZXRBdHRyaWJ1dGUoJ2hyZWYnKSkgfVxuICBmdW5jdGlvbiBjbGlja0hhbmRsZXIoZSkge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICBuZXh0ID0gZS5jdXJyZW50VGFyZ2V0O1xuICAgICF0YWJzLmlzQW5pbWF0aW5nICYmIHNlbGYuc2hvdygpO1xuICB9XG4gIHNlbGYuc2hvdyA9IGZ1bmN0aW9uICgpIHtcbiAgICBuZXh0ID0gbmV4dCB8fCBlbGVtZW50O1xuICAgIGlmICghbmV4dC5jbGFzc0xpc3QuY29udGFpbnMoJ2FjdGl2ZScpKSB7XG4gICAgICBuZXh0Q29udGVudCA9IHF1ZXJ5RWxlbWVudChuZXh0LmdldEF0dHJpYnV0ZSgnaHJlZicpKTtcbiAgICAgIGFjdGl2ZVRhYiA9IGdldEFjdGl2ZVRhYigpO1xuICAgICAgYWN0aXZlQ29udGVudCA9IGdldEFjdGl2ZUNvbnRlbnQoKTtcbiAgICAgIGhpZGVDdXN0b21FdmVudCA9IGJvb3RzdHJhcEN1c3RvbUV2ZW50KCAnaGlkZScsICd0YWInLCBuZXh0KTtcbiAgICAgIGRpc3BhdGNoQ3VzdG9tRXZlbnQuY2FsbChhY3RpdmVUYWIsIGhpZGVDdXN0b21FdmVudCk7XG4gICAgICBpZiAoaGlkZUN1c3RvbUV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQpIHsgcmV0dXJuOyB9XG4gICAgICB0YWJzLmlzQW5pbWF0aW5nID0gdHJ1ZTtcbiAgICAgIGFjdGl2ZVRhYi5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKTtcbiAgICAgIGFjdGl2ZVRhYi5zZXRBdHRyaWJ1dGUoJ2FyaWEtc2VsZWN0ZWQnLCdmYWxzZScpO1xuICAgICAgbmV4dC5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKTtcbiAgICAgIG5leHQuc2V0QXR0cmlidXRlKCdhcmlhLXNlbGVjdGVkJywndHJ1ZScpO1xuICAgICAgaWYgKCBkcm9wZG93biApIHtcbiAgICAgICAgaWYgKCAhZWxlbWVudC5wYXJlbnROb2RlLmNsYXNzTGlzdC5jb250YWlucygnZHJvcGRvd24tbWVudScpICkge1xuICAgICAgICAgIGlmIChkcm9wZG93bi5jbGFzc0xpc3QuY29udGFpbnMoJ2FjdGl2ZScpKSB7IGRyb3Bkb3duLmNsYXNzTGlzdC5yZW1vdmUoJ2FjdGl2ZScpOyB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgaWYgKCFkcm9wZG93bi5jbGFzc0xpc3QuY29udGFpbnMoJ2FjdGl2ZScpKSB7IGRyb3Bkb3duLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpOyB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGlmIChhY3RpdmVDb250ZW50LmNsYXNzTGlzdC5jb250YWlucygnZmFkZScpKSB7XG4gICAgICAgIGFjdGl2ZUNvbnRlbnQuY2xhc3NMaXN0LnJlbW92ZSgnc2hvdycpO1xuICAgICAgICBlbXVsYXRlVHJhbnNpdGlvbkVuZChhY3RpdmVDb250ZW50LCB0cmlnZ2VySGlkZSk7XG4gICAgICB9IGVsc2UgeyB0cmlnZ2VySGlkZSgpOyB9XG4gICAgfVxuICB9O1xuICBzZWxmLmRpc3Bvc2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdjbGljaycsY2xpY2tIYW5kbGVyLGZhbHNlKTtcbiAgICBkZWxldGUgZWxlbWVudC5UYWI7XG4gIH07XG4gIGVsZW1lbnQgPSBxdWVyeUVsZW1lbnQoZWxlbWVudCk7XG4gIGVsZW1lbnQuVGFiICYmIGVsZW1lbnQuVGFiLmRpc3Bvc2UoKTtcbiAgaGVpZ2h0RGF0YSA9IGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLWhlaWdodCcpO1xuICB0YWJzID0gZWxlbWVudC5jbG9zZXN0KCcubmF2Jyk7XG4gIGRyb3Bkb3duID0gdGFicyAmJiBxdWVyeUVsZW1lbnQoJy5kcm9wZG93bi10b2dnbGUnLHRhYnMpO1xuICBhbmltYXRlSGVpZ2h0ID0gIXN1cHBvcnRUcmFuc2l0aW9uIHx8IChvcHRpb25zLmhlaWdodCA9PT0gZmFsc2UgfHwgaGVpZ2h0RGF0YSA9PT0gJ2ZhbHNlJykgPyBmYWxzZSA6IHRydWU7XG4gIHRhYnMuaXNBbmltYXRpbmcgPSBmYWxzZTtcbiAgaWYgKCAhZWxlbWVudC5UYWIgKSB7XG4gICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsY2xpY2tIYW5kbGVyLGZhbHNlKTtcbiAgfVxuICBpZiAoYW5pbWF0ZUhlaWdodCkgeyB0YWJzQ29udGVudENvbnRhaW5lciA9IGdldEFjdGl2ZUNvbnRlbnQoKS5wYXJlbnROb2RlOyB9XG4gIGVsZW1lbnQuVGFiID0gc2VsZjtcbn1cblxuZnVuY3Rpb24gVG9hc3QoZWxlbWVudCxvcHRpb25zKSB7XG4gIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICB0b2FzdCwgdGltZXIgPSAwLFxuICAgICAgYW5pbWF0aW9uRGF0YSxcbiAgICAgIGF1dG9oaWRlRGF0YSxcbiAgICAgIGRlbGF5RGF0YSxcbiAgICAgIHNob3dDdXN0b21FdmVudCxcbiAgICAgIGhpZGVDdXN0b21FdmVudCxcbiAgICAgIHNob3duQ3VzdG9tRXZlbnQsXG4gICAgICBoaWRkZW5DdXN0b21FdmVudCxcbiAgICAgIG9wcyA9IHt9O1xuICBmdW5jdGlvbiBzaG93Q29tcGxldGUoKSB7XG4gICAgdG9hc3QuY2xhc3NMaXN0LnJlbW92ZSggJ3Nob3dpbmcnICk7XG4gICAgdG9hc3QuY2xhc3NMaXN0LmFkZCggJ3Nob3cnICk7XG4gICAgZGlzcGF0Y2hDdXN0b21FdmVudC5jYWxsKHRvYXN0LHNob3duQ3VzdG9tRXZlbnQpO1xuICAgIGlmIChvcHMuYXV0b2hpZGUpIHsgc2VsZi5oaWRlKCk7IH1cbiAgfVxuICBmdW5jdGlvbiBoaWRlQ29tcGxldGUoKSB7XG4gICAgdG9hc3QuY2xhc3NMaXN0LmFkZCggJ2hpZGUnICk7XG4gICAgZGlzcGF0Y2hDdXN0b21FdmVudC5jYWxsKHRvYXN0LGhpZGRlbkN1c3RvbUV2ZW50KTtcbiAgfVxuICBmdW5jdGlvbiBjbG9zZSAoKSB7XG4gICAgdG9hc3QuY2xhc3NMaXN0LnJlbW92ZSgnc2hvdycgKTtcbiAgICBvcHMuYW5pbWF0aW9uID8gZW11bGF0ZVRyYW5zaXRpb25FbmQodG9hc3QsIGhpZGVDb21wbGV0ZSkgOiBoaWRlQ29tcGxldGUoKTtcbiAgfVxuICBmdW5jdGlvbiBkaXNwb3NlQ29tcGxldGUoKSB7XG4gICAgY2xlYXJUaW1lb3V0KHRpbWVyKTtcbiAgICBlbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2NsaWNrJyxzZWxmLmhpZGUsZmFsc2UpO1xuICAgIGRlbGV0ZSBlbGVtZW50LlRvYXN0O1xuICB9XG4gIHNlbGYuc2hvdyA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAodG9hc3QgJiYgIXRvYXN0LmNsYXNzTGlzdC5jb250YWlucygnc2hvdycpKSB7XG4gICAgICBkaXNwYXRjaEN1c3RvbUV2ZW50LmNhbGwodG9hc3Qsc2hvd0N1c3RvbUV2ZW50KTtcbiAgICAgIGlmIChzaG93Q3VzdG9tRXZlbnQuZGVmYXVsdFByZXZlbnRlZCkgeyByZXR1cm47IH1cbiAgICAgIG9wcy5hbmltYXRpb24gJiYgdG9hc3QuY2xhc3NMaXN0LmFkZCggJ2ZhZGUnICk7XG4gICAgICB0b2FzdC5jbGFzc0xpc3QucmVtb3ZlKCdoaWRlJyApO1xuICAgICAgdG9hc3Qub2Zmc2V0V2lkdGg7XG4gICAgICB0b2FzdC5jbGFzc0xpc3QuYWRkKCdzaG93aW5nJyApO1xuICAgICAgb3BzLmFuaW1hdGlvbiA/IGVtdWxhdGVUcmFuc2l0aW9uRW5kKHRvYXN0LCBzaG93Q29tcGxldGUpIDogc2hvd0NvbXBsZXRlKCk7XG4gICAgfVxuICB9O1xuICBzZWxmLmhpZGUgPSBmdW5jdGlvbiAobm9UaW1lcikge1xuICAgIGlmICh0b2FzdCAmJiB0b2FzdC5jbGFzc0xpc3QuY29udGFpbnMoJ3Nob3cnKSkge1xuICAgICAgZGlzcGF0Y2hDdXN0b21FdmVudC5jYWxsKHRvYXN0LGhpZGVDdXN0b21FdmVudCk7XG4gICAgICBpZihoaWRlQ3VzdG9tRXZlbnQuZGVmYXVsdFByZXZlbnRlZCkgeyByZXR1cm47IH1cbiAgICAgIG5vVGltZXIgPyBjbG9zZSgpIDogKHRpbWVyID0gc2V0VGltZW91dCggY2xvc2UsIG9wcy5kZWxheSkpO1xuICAgIH1cbiAgfTtcbiAgc2VsZi5kaXNwb3NlID0gZnVuY3Rpb24gKCkge1xuICAgIG9wcy5hbmltYXRpb24gPyBlbXVsYXRlVHJhbnNpdGlvbkVuZCh0b2FzdCwgZGlzcG9zZUNvbXBsZXRlKSA6IGRpc3Bvc2VDb21wbGV0ZSgpO1xuICB9O1xuICBlbGVtZW50ID0gcXVlcnlFbGVtZW50KGVsZW1lbnQpO1xuICBlbGVtZW50LlRvYXN0ICYmIGVsZW1lbnQuVG9hc3QuZGlzcG9zZSgpO1xuICB0b2FzdCA9IGVsZW1lbnQuY2xvc2VzdCgnLnRvYXN0Jyk7XG4gIGFuaW1hdGlvbkRhdGEgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1hbmltYXRpb24nKTtcbiAgYXV0b2hpZGVEYXRhID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtYXV0b2hpZGUnKTtcbiAgZGVsYXlEYXRhID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtZGVsYXknKTtcbiAgc2hvd0N1c3RvbUV2ZW50ID0gYm9vdHN0cmFwQ3VzdG9tRXZlbnQoJ3Nob3cnLCAndG9hc3QnKTtcbiAgaGlkZUN1c3RvbUV2ZW50ID0gYm9vdHN0cmFwQ3VzdG9tRXZlbnQoJ2hpZGUnLCAndG9hc3QnKTtcbiAgc2hvd25DdXN0b21FdmVudCA9IGJvb3RzdHJhcEN1c3RvbUV2ZW50KCdzaG93bicsICd0b2FzdCcpO1xuICBoaWRkZW5DdXN0b21FdmVudCA9IGJvb3RzdHJhcEN1c3RvbUV2ZW50KCdoaWRkZW4nLCAndG9hc3QnKTtcbiAgb3BzLmFuaW1hdGlvbiA9IG9wdGlvbnMuYW5pbWF0aW9uID09PSBmYWxzZSB8fCBhbmltYXRpb25EYXRhID09PSAnZmFsc2UnID8gMCA6IDE7XG4gIG9wcy5hdXRvaGlkZSA9IG9wdGlvbnMuYXV0b2hpZGUgPT09IGZhbHNlIHx8IGF1dG9oaWRlRGF0YSA9PT0gJ2ZhbHNlJyA/IDAgOiAxO1xuICBvcHMuZGVsYXkgPSBwYXJzZUludChvcHRpb25zLmRlbGF5IHx8IGRlbGF5RGF0YSkgfHwgNTAwO1xuICBpZiAoICFlbGVtZW50LlRvYXN0ICkge1xuICAgIGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLHNlbGYuaGlkZSxmYWxzZSk7XG4gIH1cbiAgZWxlbWVudC5Ub2FzdCA9IHNlbGY7XG59XG5cbmZ1bmN0aW9uIFRvb2x0aXAoZWxlbWVudCxvcHRpb25zKSB7XG4gIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICB0b29sdGlwID0gbnVsbCwgdGltZXIgPSAwLCB0aXRsZVN0cmluZyxcbiAgICAgIGFuaW1hdGlvbkRhdGEsXG4gICAgICBwbGFjZW1lbnREYXRhLFxuICAgICAgZGVsYXlEYXRhLFxuICAgICAgY29udGFpbmVyRGF0YSxcbiAgICAgIHNob3dDdXN0b21FdmVudCxcbiAgICAgIHNob3duQ3VzdG9tRXZlbnQsXG4gICAgICBoaWRlQ3VzdG9tRXZlbnQsXG4gICAgICBoaWRkZW5DdXN0b21FdmVudCxcbiAgICAgIGNvbnRhaW5lckVsZW1lbnQsXG4gICAgICBjb250YWluZXJEYXRhRWxlbWVudCxcbiAgICAgIG1vZGFsLFxuICAgICAgbmF2YmFyRml4ZWRUb3AsXG4gICAgICBuYXZiYXJGaXhlZEJvdHRvbSxcbiAgICAgIHBsYWNlbWVudENsYXNzLFxuICAgICAgb3BzID0ge307XG4gIGZ1bmN0aW9uIGdldFRpdGxlKCkge1xuICAgIHJldHVybiBlbGVtZW50LmdldEF0dHJpYnV0ZSgndGl0bGUnKVxuICAgICAgICB8fCBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS10aXRsZScpXG4gICAgICAgIHx8IGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLW9yaWdpbmFsLXRpdGxlJylcbiAgfVxuICBmdW5jdGlvbiByZW1vdmVUb29sVGlwKCkge1xuICAgIG9wcy5jb250YWluZXIucmVtb3ZlQ2hpbGQodG9vbHRpcCk7XG4gICAgdG9vbHRpcCA9IG51bGw7IHRpbWVyID0gbnVsbDtcbiAgfVxuICBmdW5jdGlvbiBjcmVhdGVUb29sVGlwKCkge1xuICAgIHRpdGxlU3RyaW5nID0gZ2V0VGl0bGUoKTtcbiAgICBpZiAoIHRpdGxlU3RyaW5nICkge1xuICAgICAgdG9vbHRpcCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgaWYgKG9wcy50ZW1wbGF0ZSkge1xuICAgICAgICB2YXIgdG9vbHRpcE1hcmt1cCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgICB0b29sdGlwTWFya3VwLmlubmVySFRNTCA9IG9wcy50ZW1wbGF0ZS50cmltKCk7XG4gICAgICAgIHRvb2x0aXAuY2xhc3NOYW1lID0gdG9vbHRpcE1hcmt1cC5maXJzdENoaWxkLmNsYXNzTmFtZTtcbiAgICAgICAgdG9vbHRpcC5pbm5lckhUTUwgPSB0b29sdGlwTWFya3VwLmZpcnN0Q2hpbGQuaW5uZXJIVE1MO1xuICAgICAgICBxdWVyeUVsZW1lbnQoJy50b29sdGlwLWlubmVyJyx0b29sdGlwKS5pbm5lckhUTUwgPSB0aXRsZVN0cmluZy50cmltKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB2YXIgdG9vbHRpcEFycm93ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgIHRvb2x0aXBBcnJvdy5jbGFzc0xpc3QuYWRkKCdhcnJvdycpO1xuICAgICAgICB0b29sdGlwLmFwcGVuZENoaWxkKHRvb2x0aXBBcnJvdyk7XG4gICAgICAgIHZhciB0b29sdGlwSW5uZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgICAgdG9vbHRpcElubmVyLmNsYXNzTGlzdC5hZGQoJ3Rvb2x0aXAtaW5uZXInKTtcbiAgICAgICAgdG9vbHRpcC5hcHBlbmRDaGlsZCh0b29sdGlwSW5uZXIpO1xuICAgICAgICB0b29sdGlwSW5uZXIuaW5uZXJIVE1MID0gdGl0bGVTdHJpbmc7XG4gICAgICB9XG4gICAgICB0b29sdGlwLnN0eWxlLmxlZnQgPSAnMCc7XG4gICAgICB0b29sdGlwLnN0eWxlLnRvcCA9ICcwJztcbiAgICAgIHRvb2x0aXAuc2V0QXR0cmlidXRlKCdyb2xlJywndG9vbHRpcCcpO1xuICAgICAgIXRvb2x0aXAuY2xhc3NMaXN0LmNvbnRhaW5zKCd0b29sdGlwJykgJiYgdG9vbHRpcC5jbGFzc0xpc3QuYWRkKCd0b29sdGlwJyk7XG4gICAgICAhdG9vbHRpcC5jbGFzc0xpc3QuY29udGFpbnMob3BzLmFuaW1hdGlvbikgJiYgdG9vbHRpcC5jbGFzc0xpc3QuYWRkKG9wcy5hbmltYXRpb24pO1xuICAgICAgIXRvb2x0aXAuY2xhc3NMaXN0LmNvbnRhaW5zKHBsYWNlbWVudENsYXNzKSAmJiB0b29sdGlwLmNsYXNzTGlzdC5hZGQocGxhY2VtZW50Q2xhc3MpO1xuICAgICAgb3BzLmNvbnRhaW5lci5hcHBlbmRDaGlsZCh0b29sdGlwKTtcbiAgICB9XG4gIH1cbiAgZnVuY3Rpb24gdXBkYXRlVG9vbHRpcCgpIHtcbiAgICBzdHlsZVRpcChlbGVtZW50LCB0b29sdGlwLCBvcHMucGxhY2VtZW50LCBvcHMuY29udGFpbmVyKTtcbiAgfVxuICBmdW5jdGlvbiBzaG93VG9vbHRpcCgpIHtcbiAgICAhdG9vbHRpcC5jbGFzc0xpc3QuY29udGFpbnMoJ3Nob3cnKSAmJiAoIHRvb2x0aXAuY2xhc3NMaXN0LmFkZCgnc2hvdycpICk7XG4gIH1cbiAgZnVuY3Rpb24gdG91Y2hIYW5kbGVyKGUpe1xuICAgIGlmICggdG9vbHRpcCAmJiB0b29sdGlwLmNvbnRhaW5zKGUudGFyZ2V0KSB8fCBlLnRhcmdldCA9PT0gZWxlbWVudCB8fCBlbGVtZW50LmNvbnRhaW5zKGUudGFyZ2V0KSkgOyBlbHNlIHtcbiAgICAgIHNlbGYuaGlkZSgpO1xuICAgIH1cbiAgfVxuICBmdW5jdGlvbiB0b2dnbGVBY3Rpb24oYWN0aW9uKXtcbiAgICBhY3Rpb24gPSBhY3Rpb24gPyAnYWRkRXZlbnRMaXN0ZW5lcicgOiAncmVtb3ZlRXZlbnRMaXN0ZW5lcic7XG4gICAgZG9jdW1lbnRbYWN0aW9uXSggJ3RvdWNoc3RhcnQnLCB0b3VjaEhhbmRsZXIsIHBhc3NpdmVIYW5kbGVyICk7XG4gICAgd2luZG93W2FjdGlvbl0oICdyZXNpemUnLCBzZWxmLmhpZGUsIHBhc3NpdmVIYW5kbGVyICk7XG4gIH1cbiAgZnVuY3Rpb24gc2hvd0FjdGlvbigpIHtcbiAgICB0b2dnbGVBY3Rpb24oMSk7XG4gICAgZGlzcGF0Y2hDdXN0b21FdmVudC5jYWxsKGVsZW1lbnQsIHNob3duQ3VzdG9tRXZlbnQpO1xuICB9XG4gIGZ1bmN0aW9uIGhpZGVBY3Rpb24oKSB7XG4gICAgdG9nZ2xlQWN0aW9uKCk7XG4gICAgcmVtb3ZlVG9vbFRpcCgpO1xuICAgIGRpc3BhdGNoQ3VzdG9tRXZlbnQuY2FsbChlbGVtZW50LCBoaWRkZW5DdXN0b21FdmVudCk7XG4gIH1cbiAgZnVuY3Rpb24gdG9nZ2xlRXZlbnRzKGFjdGlvbikge1xuICAgIGFjdGlvbiA9IGFjdGlvbiA/ICdhZGRFdmVudExpc3RlbmVyJyA6ICdyZW1vdmVFdmVudExpc3RlbmVyJztcbiAgICBlbGVtZW50W2FjdGlvbl0obW91c2VDbGlja0V2ZW50cy5kb3duLCBzZWxmLnNob3csZmFsc2UpO1xuICAgIGVsZW1lbnRbYWN0aW9uXShtb3VzZUhvdmVyRXZlbnRzWzBdLCBzZWxmLnNob3csZmFsc2UpO1xuICAgIGVsZW1lbnRbYWN0aW9uXShtb3VzZUhvdmVyRXZlbnRzWzFdLCBzZWxmLmhpZGUsZmFsc2UpO1xuICB9XG4gIHNlbGYuc2hvdyA9IGZ1bmN0aW9uICgpIHtcbiAgICBjbGVhclRpbWVvdXQodGltZXIpO1xuICAgIHRpbWVyID0gc2V0VGltZW91dCggZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKHRvb2x0aXAgPT09IG51bGwpIHtcbiAgICAgICAgZGlzcGF0Y2hDdXN0b21FdmVudC5jYWxsKGVsZW1lbnQsIHNob3dDdXN0b21FdmVudCk7XG4gICAgICAgIGlmIChzaG93Q3VzdG9tRXZlbnQuZGVmYXVsdFByZXZlbnRlZCkgeyByZXR1cm47IH1cbiAgICAgICAgaWYoY3JlYXRlVG9vbFRpcCgpICE9PSBmYWxzZSkge1xuICAgICAgICAgIHVwZGF0ZVRvb2x0aXAoKTtcbiAgICAgICAgICBzaG93VG9vbHRpcCgpO1xuICAgICAgICAgICEhb3BzLmFuaW1hdGlvbiA/IGVtdWxhdGVUcmFuc2l0aW9uRW5kKHRvb2x0aXAsIHNob3dBY3Rpb24pIDogc2hvd0FjdGlvbigpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgMjAgKTtcbiAgfTtcbiAgc2VsZi5oaWRlID0gZnVuY3Rpb24gKCkge1xuICAgIGNsZWFyVGltZW91dCh0aW1lcik7XG4gICAgdGltZXIgPSBzZXRUaW1lb3V0KCBmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAodG9vbHRpcCAmJiB0b29sdGlwLmNsYXNzTGlzdC5jb250YWlucygnc2hvdycpKSB7XG4gICAgICAgIGRpc3BhdGNoQ3VzdG9tRXZlbnQuY2FsbChlbGVtZW50LCBoaWRlQ3VzdG9tRXZlbnQpO1xuICAgICAgICBpZiAoaGlkZUN1c3RvbUV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQpIHsgcmV0dXJuOyB9XG4gICAgICAgIHRvb2x0aXAuY2xhc3NMaXN0LnJlbW92ZSgnc2hvdycpO1xuICAgICAgICAhIW9wcy5hbmltYXRpb24gPyBlbXVsYXRlVHJhbnNpdGlvbkVuZCh0b29sdGlwLCBoaWRlQWN0aW9uKSA6IGhpZGVBY3Rpb24oKTtcbiAgICAgIH1cbiAgICB9LCBvcHMuZGVsYXkpO1xuICB9O1xuICBzZWxmLnRvZ2dsZSA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAoIXRvb2x0aXApIHsgc2VsZi5zaG93KCk7IH1cbiAgICBlbHNlIHsgc2VsZi5oaWRlKCk7IH1cbiAgfTtcbiAgc2VsZi5kaXNwb3NlID0gZnVuY3Rpb24gKCkge1xuICAgIHRvZ2dsZUV2ZW50cygpO1xuICAgIHNlbGYuaGlkZSgpO1xuICAgIGVsZW1lbnQuc2V0QXR0cmlidXRlKCd0aXRsZScsIGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLW9yaWdpbmFsLXRpdGxlJykpO1xuICAgIGVsZW1lbnQucmVtb3ZlQXR0cmlidXRlKCdkYXRhLW9yaWdpbmFsLXRpdGxlJyk7XG4gICAgZGVsZXRlIGVsZW1lbnQuVG9vbHRpcDtcbiAgfTtcbiAgZWxlbWVudCA9IHF1ZXJ5RWxlbWVudChlbGVtZW50KTtcbiAgZWxlbWVudC5Ub29sdGlwICYmIGVsZW1lbnQuVG9vbHRpcC5kaXNwb3NlKCk7XG4gIGFuaW1hdGlvbkRhdGEgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1hbmltYXRpb24nKTtcbiAgcGxhY2VtZW50RGF0YSA9IGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLXBsYWNlbWVudCcpO1xuICBkZWxheURhdGEgPSBlbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1kZWxheScpO1xuICBjb250YWluZXJEYXRhID0gZWxlbWVudC5nZXRBdHRyaWJ1dGUoJ2RhdGEtY29udGFpbmVyJyk7XG4gIHNob3dDdXN0b21FdmVudCA9IGJvb3RzdHJhcEN1c3RvbUV2ZW50KCdzaG93JywgJ3Rvb2x0aXAnKTtcbiAgc2hvd25DdXN0b21FdmVudCA9IGJvb3RzdHJhcEN1c3RvbUV2ZW50KCdzaG93bicsICd0b29sdGlwJyk7XG4gIGhpZGVDdXN0b21FdmVudCA9IGJvb3RzdHJhcEN1c3RvbUV2ZW50KCdoaWRlJywgJ3Rvb2x0aXAnKTtcbiAgaGlkZGVuQ3VzdG9tRXZlbnQgPSBib290c3RyYXBDdXN0b21FdmVudCgnaGlkZGVuJywgJ3Rvb2x0aXAnKTtcbiAgY29udGFpbmVyRWxlbWVudCA9IHF1ZXJ5RWxlbWVudChvcHRpb25zLmNvbnRhaW5lcik7XG4gIGNvbnRhaW5lckRhdGFFbGVtZW50ID0gcXVlcnlFbGVtZW50KGNvbnRhaW5lckRhdGEpO1xuICBtb2RhbCA9IGVsZW1lbnQuY2xvc2VzdCgnLm1vZGFsJyk7XG4gIG5hdmJhckZpeGVkVG9wID0gZWxlbWVudC5jbG9zZXN0KCcuZml4ZWQtdG9wJyk7XG4gIG5hdmJhckZpeGVkQm90dG9tID0gZWxlbWVudC5jbG9zZXN0KCcuZml4ZWQtYm90dG9tJyk7XG4gIG9wcy5hbmltYXRpb24gPSBvcHRpb25zLmFuaW1hdGlvbiAmJiBvcHRpb25zLmFuaW1hdGlvbiAhPT0gJ2ZhZGUnID8gb3B0aW9ucy5hbmltYXRpb24gOiBhbmltYXRpb25EYXRhIHx8ICdmYWRlJztcbiAgb3BzLnBsYWNlbWVudCA9IG9wdGlvbnMucGxhY2VtZW50ID8gb3B0aW9ucy5wbGFjZW1lbnQgOiBwbGFjZW1lbnREYXRhIHx8ICd0b3AnO1xuICBvcHMudGVtcGxhdGUgPSBvcHRpb25zLnRlbXBsYXRlID8gb3B0aW9ucy50ZW1wbGF0ZSA6IG51bGw7XG4gIG9wcy5kZWxheSA9IHBhcnNlSW50KG9wdGlvbnMuZGVsYXkgfHwgZGVsYXlEYXRhKSB8fCAyMDA7XG4gIG9wcy5jb250YWluZXIgPSBjb250YWluZXJFbGVtZW50ID8gY29udGFpbmVyRWxlbWVudFxuICAgICAgICAgICAgICAgICAgICAgICAgICA6IGNvbnRhaW5lckRhdGFFbGVtZW50ID8gY29udGFpbmVyRGF0YUVsZW1lbnRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgOiBuYXZiYXJGaXhlZFRvcCA/IG5hdmJhckZpeGVkVG9wXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDogbmF2YmFyRml4ZWRCb3R0b20gPyBuYXZiYXJGaXhlZEJvdHRvbVxuICAgICAgICAgICAgICAgICAgICAgICAgICA6IG1vZGFsID8gbW9kYWwgOiBkb2N1bWVudC5ib2R5O1xuICBwbGFjZW1lbnRDbGFzcyA9IFwiYnMtdG9vbHRpcC1cIiArIChvcHMucGxhY2VtZW50KTtcbiAgdGl0bGVTdHJpbmcgPSBnZXRUaXRsZSgpO1xuICBpZiAoICF0aXRsZVN0cmluZyApIHsgcmV0dXJuOyB9XG4gIGlmICghZWxlbWVudC5Ub29sdGlwKSB7XG4gICAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2RhdGEtb3JpZ2luYWwtdGl0bGUnLHRpdGxlU3RyaW5nKTtcbiAgICBlbGVtZW50LnJlbW92ZUF0dHJpYnV0ZSgndGl0bGUnKTtcbiAgICB0b2dnbGVFdmVudHMoMSk7XG4gIH1cbiAgZWxlbWVudC5Ub29sdGlwID0gc2VsZjtcbn1cblxudmFyIGNvbXBvbmVudHNJbml0ID0ge307XG5cbmZ1bmN0aW9uIGluaXRpYWxpemVEYXRhQVBJKCBDb25zdHJ1Y3RvciwgY29sbGVjdGlvbiApe1xuICBBcnJheS5mcm9tKGNvbGxlY3Rpb24pLm1hcChmdW5jdGlvbiAoeCl7IHJldHVybiBuZXcgQ29uc3RydWN0b3IoeCk7IH0pO1xufVxuZnVuY3Rpb24gaW5pdENhbGxiYWNrKGxvb2tVcCl7XG4gIGxvb2tVcCA9IGxvb2tVcCB8fCBkb2N1bWVudDtcbiAgZm9yICh2YXIgY29tcG9uZW50IGluIGNvbXBvbmVudHNJbml0KSB7XG4gICAgaW5pdGlhbGl6ZURhdGFBUEkoIGNvbXBvbmVudHNJbml0W2NvbXBvbmVudF1bMF0sIGxvb2tVcC5xdWVyeVNlbGVjdG9yQWxsIChjb21wb25lbnRzSW5pdFtjb21wb25lbnRdWzFdKSApO1xuICB9XG59XG5cbmNvbXBvbmVudHNJbml0LkFsZXJ0ID0gWyBBbGVydCwgJ1tkYXRhLWRpc21pc3M9XCJhbGVydFwiXSddO1xuY29tcG9uZW50c0luaXQuQnV0dG9uID0gWyBCdXR0b24sICdbZGF0YS10b2dnbGU9XCJidXR0b25zXCJdJyBdO1xuY29tcG9uZW50c0luaXQuQ2Fyb3VzZWwgPSBbIENhcm91c2VsLCAnW2RhdGEtcmlkZT1cImNhcm91c2VsXCJdJyBdO1xuY29tcG9uZW50c0luaXQuQ29sbGFwc2UgPSBbIENvbGxhcHNlLCAnW2RhdGEtdG9nZ2xlPVwiY29sbGFwc2VcIl0nIF07XG5jb21wb25lbnRzSW5pdC5Ecm9wZG93biA9IFsgRHJvcGRvd24sICdbZGF0YS10b2dnbGU9XCJkcm9wZG93blwiXSddO1xuY29tcG9uZW50c0luaXQuTW9kYWwgPSBbIE1vZGFsLCAnW2RhdGEtdG9nZ2xlPVwibW9kYWxcIl0nIF07XG5jb21wb25lbnRzSW5pdC5Qb3BvdmVyID0gWyBQb3BvdmVyLCAnW2RhdGEtdG9nZ2xlPVwicG9wb3ZlclwiXSxbZGF0YS10aXA9XCJwb3BvdmVyXCJdJyBdO1xuY29tcG9uZW50c0luaXQuU2Nyb2xsU3B5ID0gWyBTY3JvbGxTcHksICdbZGF0YS1zcHk9XCJzY3JvbGxcIl0nIF07XG5jb21wb25lbnRzSW5pdC5UYWIgPSBbIFRhYiwgJ1tkYXRhLXRvZ2dsZT1cInRhYlwiXScgXTtcbmNvbXBvbmVudHNJbml0LlRvYXN0ID0gWyBUb2FzdCwgJ1tkYXRhLWRpc21pc3M9XCJ0b2FzdFwiXScgXTtcbmNvbXBvbmVudHNJbml0LlRvb2x0aXAgPSBbIFRvb2x0aXAsICdbZGF0YS10b2dnbGU9XCJ0b29sdGlwXCJdLFtkYXRhLXRpcD1cInRvb2x0aXBcIl0nIF07XG5kb2N1bWVudC5ib2R5ID8gaW5pdENhbGxiYWNrKCkgOiBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCAnRE9NQ29udGVudExvYWRlZCcsIGZ1bmN0aW9uIGluaXRXcmFwcGVyKCl7XG5cdGluaXRDYWxsYmFjaygpO1xuXHRkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJyxpbml0V3JhcHBlcixmYWxzZSk7XG59LCBmYWxzZSApO1xuXG5mdW5jdGlvbiByZW1vdmVFbGVtZW50RGF0YUFQSSggQ29uc3RydWN0b3JOYW1lLCBjb2xsZWN0aW9uICl7XG4gIEFycmF5LmZyb20oY29sbGVjdGlvbikubWFwKGZ1bmN0aW9uICh4KXsgcmV0dXJuIHhbQ29uc3RydWN0b3JOYW1lXS5kaXNwb3NlKCk7IH0pO1xufVxuZnVuY3Rpb24gcmVtb3ZlRGF0YUFQSShsb29rVXApIHtcbiAgbG9va1VwID0gbG9va1VwIHx8IGRvY3VtZW50O1xuICBmb3IgKHZhciBjb21wb25lbnQgaW4gY29tcG9uZW50c0luaXQpIHtcbiAgICByZW1vdmVFbGVtZW50RGF0YUFQSSggY29tcG9uZW50LCBsb29rVXAucXVlcnlTZWxlY3RvckFsbCAoY29tcG9uZW50c0luaXRbY29tcG9uZW50XVsxXSkgKTtcbiAgfVxufVxuXG52YXIgdmVyc2lvbiA9IFwiMy4wLjEwXCI7XG5cbnZhciBpbmRleCA9IHtcbiAgQWxlcnQ6IEFsZXJ0LFxuICBCdXR0b246IEJ1dHRvbixcbiAgQ2Fyb3VzZWw6IENhcm91c2VsLFxuICBDb2xsYXBzZTogQ29sbGFwc2UsXG4gIERyb3Bkb3duOiBEcm9wZG93bixcbiAgTW9kYWw6IE1vZGFsLFxuICBQb3BvdmVyOiBQb3BvdmVyLFxuICBTY3JvbGxTcHk6IFNjcm9sbFNweSxcbiAgVGFiOiBUYWIsXG4gIFRvYXN0OiBUb2FzdCxcbiAgVG9vbHRpcDogVG9vbHRpcCxcbiAgaW5pdENhbGxiYWNrOiBpbml0Q2FsbGJhY2ssXG4gIHJlbW92ZURhdGFBUEk6IHJlbW92ZURhdGFBUEksXG4gIGNvbXBvbmVudHNJbml0OiBjb21wb25lbnRzSW5pdCxcbiAgVmVyc2lvbjogdmVyc2lvblxufTtcblxuZXhwb3J0IGRlZmF1bHQgaW5kZXg7XG4iXSwic291cmNlUm9vdCI6IiJ9