<?php
defined('TYPO3_MODE') or die();

$fields = [
    'tx_news_subheadline' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:site/Resources/Private/Language/locallang.xlf:tx_news_domain_model_news.tx_news_subheadline',
        'config' => [
            'type' => 'input',
            'size' => 30,
        ],
    ],
    'tx_news_slider' => [
        'label' => 'LLL:EXT:site/Resources/Private/Language/locallang.xlf:tx_news_domain_model_news.tx_news_slider',
        'config' => [
            'type' => 'input',
            'size' => 10,
            'eval' => 'trim,int',
            'range' => [
                'lower' => -90,
                'upper' => 90,
            ],
            'default' => 0,
            'slider' => [
                'step' => 10,
                'width' => 200,
            ],
        ],
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_news_domain_model_news', $fields);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_news_domain_model_news', 'tx_news_subheadline', '', 'after:title');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_news_domain_model_news', 'tx_news_slider', '', 'after:tx_news_subheadline');