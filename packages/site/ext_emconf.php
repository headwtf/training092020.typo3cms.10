<?php

/**
 * Extension Manager/Repository config file for ext "site".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Site',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '10.2.0-10.4.99',
            'fluid_styled_content' => '10.2.0-10.4.99',
            'rte_ckeditor' => '10.2.0-10.4.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Head\\Site\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Ralf Hueskes',
    'author_email' => 'r.hueskes@head.wtf',
    'author_company' => 'Head',
    'version' => '1.0.0',
];
