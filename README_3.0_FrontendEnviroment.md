# 3. Frontend Enviroment



We recommend to use Webpack Encore as asset bundling. Webpack Encore is  like “Lavaral Mix” a wrapper for Webpack that allows easy build processes. Through so-called "Entrypoints", bundles of CSS, JS, fonts and other assets can be created, which can be integrated into TYPO3 CMS with little effort.Webpack Encore is known from the "Symfony" scene and can be fully integrated into TYPO3 CMS in the form of an extension (typo3_encore).

#### 3.1. Install Webpack Encore

```
$ yarn add @symfony/webpack-encore --dev
$ yarn install
```

Documentation: https://symfony.com/doc/current/frontend/encore/installation.html 



#### 3.2 Webpack Encore Configuration

Of course we need a configuration for Webpack Encore. 
This file is called **webpack.config.js** and belongs in the root directory.

```
$ touch webpack.config.js
```

Here is a best practice content:

```
var Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
  Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
  .setOutputPath('packages/site/Resources/Public/build/')
  .setPublicPath('/typo3conf/ext/site/Resources/Public/build/')
  .setManifestKeyPrefix('build/')

  .addEntry('app', './assets/app.js')

  .splitEntryChunks()
  .enableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableBuildNotifications()
  .enableSourceMaps(!Encore.isProduction())
  .enableVersioning(Encore.isProduction())
  .configureBabelPresetEnv((config) => {
    config.useBuiltIns = 'usage';
    config.corejs = 3;
  })
  .enableSassLoader()
  //.enableReactPreset()
;

module.exports = Encore.getWebpackConfig();

```

To use it with our example we have to install some additional packages:

```
$ yarn add sass-loader@^8.0.0 node-sass --dev
$ yarn add webpack-notifier@^1.6.0 --dev
```



#### 3.3. Install TYPO3 Webpack Encore extenion

Then we need an extension for TYPO3 CMS that "understands" Webpack Encore. But before we can install it, and to avoid running into another installation error, we need to set the PHP version in the project's composer.json to **7.2.5**.
[https://packagist.org/packages/ssch/typo3-encore](https://packagist.org/packages/ssch/typo3-encore)

```
$ ddev composer req ssch/typo3-encore****
```



#### 3.4 First Entrypoint and Build

Now we have almost everything for our first build. What we still need: the entry point from the **Webpack.config.js.** Create the file "app.js" and put it into the folder assets in the root directory. 

```
$ mkdir assets
$ touch assets/app.js
```

For now you will only get the following content: 

```
$ console.log ('Hi there!');
```

And now you can create your first build:

```
$ yarn encore dev
```



Let uis look in the folder "packages/site/Resources/Public/build". 
Here you should find these files now:

* app.json
* entrypoints.json
* manifest.json
* runtime.js



#### **3.5. Integrate the Entrypoint into TYPO3 CMS**

Webpack Encore has to be configured for TYPO3 CMS - it has to know where the entry points are. 
And for this the extension "typo3_encore" looks for the files "entrypoints.json" and "manifest.json".
For this we have to add small snippets to the TypoScript configuration of our site package. 

In Therory you can write any setup or configuration into the main files, but we want to keep it structured and clear and therefore we create new folders and files in which we reference the final files. 

**-- packages/site/Configuration/TypoScript/constants.typoscript:***

```
##################
### EXTENSIONS ###
##################
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site/Configuration/TypoScript/Extensions/constants.typoscript">
```

**-- packages/site/Configuration/TypoScript/setup.typoscript:**

```
##################
### EXTENSIONS ###
##################
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site/Configuration/TypoScript/Extensions/setup.typoscript">
```

We then create the two files ‘constants’ and ‘setup’ in the directory "Extensions". 
For clarity we create a new folder with the setup and constants files for all extensions.



(IDE Create File): **Extensions/setup.typoscript**
(IDE Create File): **Extensions/constants.typoscript**

**-- packages/site/Configuration/TypoScript/Extensions/setup.typoscript**

```
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site/Configuration/TypoScript/Extensions/typo3_encore/setup.typoscript">
```
**-- packages/site/Configuration/TypoScript/Extensions/constants.typoscript**
```
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:site/Configuration/TypoScript/Extensions/typo3_encore/constants.typoscript">
```


(IDE Create File): **Extensions/typo3_encore/setup.typoscript**
(IDE Create File): **Extensions/typo3_encore/constants.typoscript**

**-- packages/site/Configuration/TypoScript/Extensions/typo3_encore/setup.typoscript**

```
plugin.tx_typo3encore {
    settings {
        entrypointJsonPath = {$plugin.tx_typo3encore.settings.entrypointJsonPath}
        manifestJsonPath = {$plugin.tx_typo3encore.settings.manifestJsonPath}

        preload {
            enable = {$plugin.tx_typo3encore.settings.preload.enable}
            crossorigin = {$plugin.tx_typo3encore.settings.preload.crossorigin}
        }
    }
}

module.tx_typo3encore.settings < plugin.tx_typo3encore.settings

```

**-- packages/site/Configuration/TypoScript/Extensions/typo3_encore/constants.typoscript**

```
plugin.tx_typo3encore {
  settings {
    entrypointJsonPath = EXT:site/Resources/Public/build/entrypoints.json
    manifestJsonPath = EXT:site/Resources/Public/build/manifest.json

    preload {
      enable = 1
      crossorigin =
    }
  }
}
```

Back to **packages/site/Configuration/TypoScript/setup.typoscript** 
– here you change the configuration as follows 

```
includeCSS {
    10 = typo3_encore:app
}
includeJSFooter {
    10 = typo3_encore:app
}
```

* Clear your caches now and go to the start page. 
* Check your JavaScript console. If it says "'Hi there!'", then you've done it!
  



**3.6. Install Bootstrap CSS and FontAwesome**

```
$ yarn add bootstrap 
$ yarn add bootstrap.native 
$ yarn add @fortawesome/fontawesome-free
```

Add custom sass to **assets/app.js**:
```	
// Styles
// ------
// Main Styles
require("./scss/main.scss");
```

**Create main.scs into assets/scss/**
(IDE Create) /scss/main.scss 

```	
// Bootstrap Defaults
@import "~bootstrap/scss/functions";
@import "~bootstrap/scss/variables";
@import "~bootstrap/scss/mixins";

// Font Awesome Pro
@import "~@fortawesome/fontawesome-free/scss/fontawesome.scss";
@import "~@fortawesome/fontawesome-free/scss/brands.scss";
@import "~@fortawesome/fontawesome-free/scss/regular.scss";
@import "~@fortawesome/fontawesome-free/scss/solid.scss";

// Bootstrap
@import "~bootstrap/scss/root";
@import "~bootstrap/scss/reboot";
@import "~bootstrap/scss/type";
@import "~bootstrap/scss/images";
@import "~bootstrap/scss/code";
@import "~bootstrap/scss/grid";
@import "~bootstrap/scss/tables";
@import "~bootstrap/scss/forms";
@import "~bootstrap/scss/buttons";
@import "~bootstrap/scss/transitions";
@import "~bootstrap/scss/dropdown";
@import "~bootstrap/scss/button-group";
@import "~bootstrap/scss/input-group";
@import "~bootstrap/scss/custom-forms";
@import "~bootstrap/scss/nav";
@import "~bootstrap/scss/navbar";
@import "~bootstrap/scss/card";
@import "~bootstrap/scss/breadcrumb";
@import "~bootstrap/scss/pagination";
@import "~bootstrap/scss/badge";
@import "~bootstrap/scss/jumbotron";
@import "~bootstrap/scss/alert";
@import "~bootstrap/scss/progress";
@import "~bootstrap/scss/media";
@import "~bootstrap/scss/list-group";
@import "~bootstrap/scss/close";
@import "~bootstrap/scss/toasts";
@import "~bootstrap/scss/modal";
@import "~bootstrap/scss/tooltip";
@import "~bootstrap/scss/popover";
@import "~bootstrap/scss/carousel";

@import "~bootstrap/scss/spinners";
@import "~bootstrap/scss/utilities";
@import "~bootstrap/scss/print";
```

**Run**  
```
$yarn encore dev
```

**Copy example Bootstrap to** typo3conf/ext/site/Resources/Private/Page/Layouts/Default.html 	

```
<div class="container">
    <header class="blog-header py-3 my-3  border-bottom border-info">
        <div class="row flex-nowrap justify-content-between align-items-center">
            <div class="col-4 pt-1">
                <a class="text-muted" href="#">Subscribe</a>
            </div>
            <div class="col-4 text-center">
                <a class="blog-header-logo text-dark" href="#">LOGO</a>
            </div>
            <div class="col-4 d-flex justify-content-end align-items-center">
                <a class="text-muted" href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mx-3"><circle cx="10.5" cy="10.5" r="7.5"></circle><line x1="21" y1="21" x2="15.8" y2="15.8"></line></svg>
                </a>
                <a class="btn btn-sm btn-outline-secondary" href="#">Sign up</a>
            </div>
        </div>
    </header>

    <div class="nav-scroller pb-3 mb-2">
        <!-- Partial for MainNav -->
        <f:render partial="Navi/Main" arguments="{_all}" />
    </div>

    <div class="jumbotron p-3 p-md-5 text-white rounded bg-info">
        <div class="col-md-6 px-0">
            <h1>{data.title}</h1>
        </div>
    </div>
</div>

<main role="main" class="container">
    <div class="row">
        <div class="col-md-12 blog-main">
            <f:render section="Main" />
        </div>
    </div>
</main>
```


**Create a first partial** "packages/site/Resources/Private/Page/Partials/Navi/Main.html"
```
    <ul class="nav d-flex justify-content-between">
        <f:for each="{mainnavigation}" as="mainnavigationItem">
            <li class="{f:if(condition: mainnavigationItem.active, then:'active')}">
                <a href="{mainnavigationItem.link}" class="p-2" target="{mainnavigationItem.target}" title="{mainnavigationItem.title}">
                    {mainnavigationItem.title}
                </a>
                <f:if condition="{mainnavigationItem.children}">
                    <ul>
                        <f:for each="{mainnavigationItem.children}" as="child">
                            <li class="{f:if(condition: child.active, then:'active')}">
                                <a href="{child.link}" target="{child.target}" title="{child.title}">
                                    {child.title}
                                </a>
                            </li>
                        </f:for>
                    </ul>
                </f:if>
            </li>
        </f:for>
    </ul>

```


**Clear cache** 